<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:dp="http://www.datapower.com/extensions" 
    xmlns:dpfunc="http://www.datapower.com/extensions/functions"
    xmlns:dpconfig="http://www.datapower.com/param/config"
    xmlns:func="http://exslt.org/functions" 
    extension-element-prefixes="dp func" 
    exclude-result-prefixes="dp dpfunc dpconfig func">  
    
    <xsl:template match="/">  
      <xsl:variable name="transactionID" select="dp:variable('var://service/transaction-id')"/>
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope" xmlns:typ="http://rmx.rmetro.com/types">
   <soap:Header/>
   <soap:Body>
      <typ:NEMSIS2Response>
         <typ:Status>Success</typ:Status>
         <!--You have a CHOICE of the next 2 items at this level-->
         <typ:TransactionID><xsl:value-of select="$transactionID"/></typ:TransactionID>
      </typ:NEMSIS2Response>
   </soap:Body>
</soap:Envelope>
    </xsl:template> 
</xsl:stylesheet>
