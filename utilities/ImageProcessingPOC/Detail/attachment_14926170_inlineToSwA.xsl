<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" 
xmlns:dp="http://www.datapower.com/extensions" extensions-Element-prefixes="dp">
<xsl:template match="/">
    <xsl:variable name="uuid" select="dp:generate-uuid()"/>
    <xsl:variable name="requestDoc" select="dp:variable('var://context/requestDoc')"/>
	<xsl:variable name="vendorInfo" select="dp:variable('var://context/requestDoc')//vendorInfo//text()"/>
	<xsl:variable name="document" select="dp:variable('var://context/requestDoc')//document//text()"/>
	<xsl:variable name="requestContext" select="dp:variable('var://context/requestDoc')//requestContext//text()"/>
	<xsl:variable name="vendorCode" select="dp:variable('var://context/requestDoc')//vendorInfo//vendorCode//text()"/>
 	<xsl:message>vendorCode  in response is <xsl:value-of select="$vendorCode"/></xsl:message>
	<xsl:variable name="vendorTrackingNumber" select="dp:variable('var://context/requestDoc')//vendorInfo//vendorTrackingNumber//text()"/>
	<xsl:message>vendorTrackingNumber  in response is <xsl:value-of select="$vendorTrackingNumber"/></xsl:message>
	<xsl:variable name="vendorSubmissionDate" select="dp:variable('var://context/requestDoc')//vendorInfo//vendorSubmissionDate//text()"/>
	<xsl:message>vendorSubmissionDate  in response is <xsl:value-of select="$vendorSubmissionDate"/></xsl:message>
	<xsl:variable name="documentName" select="dp:variable('var://context/requestDoc')//document//documentName//text()"/>
	<xsl:message>documentName  in response is <xsl:value-of select="$documentName"/></xsl:message>
	<xsl:variable name="documentContent" select="dp:variable('var://context/requestDoc')//document//documentContent//text()"/>
	<xsl:message>documentContent  in response is <xsl:value-of select="$documentContent"/></xsl:message>
 	<xsl:variable name="key" select="dp:variable('var://context/requestDoc')//requestContext//key//text()"/>
 	<xsl:message>key  in response is <xsl:value-of select="$key"/></xsl:message>
	<xsl:variable name="value" select="dp:variable('var://context/requestDoc')//requestContext//value//text()"/>
	<xsl:message>value  in response is <xsl:value-of select="$value"/></xsl:message>
    <xsl:variable name="tempReqType" select="dp:variable('var://context/requestDoc')//requestMode//text()"/>
 	<xsl:message>tempReqType in response  is <xsl:value-of select="$tempReqType"/></xsl:message>
    <xsl:variable name="tempAgencyId" select="dp:variable('var://context/requestDoc')//agencyId//text()"/>
 	<xsl:message>tempAgencyId  in response is <xsl:value-of select="$tempAgencyId"/></xsl:message>
	<xsl:message>requestDoc in response is <xsl:value-of select="dp:variable('var://context/requestDoc')"/></xsl:message>
  	<xsl:variable name="clientAddr" select="dp:variable('var://service/client-service-address')"/>
 	<xsl:message>clientAddr  in response is <xsl:value-of select="$clientAddr"/></xsl:message>
	<xsl:variable name="soap">
	<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
	<soapenv:Body>
      <tns:scanAttachmentRequest xmlns:msg="http://ea.hhs.state.tx.us/malwarescan/0.2/messages" xmlns:type="http://ea.hhs.state.tx.us/malwarescan/0.2/types" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:tns="http://ea.hhs.state.tx.us/EaMalwareScanner/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/">
         <messageScanAttachmentRequest>
            <vendorInfo>
               <vendorCode><xsl:value-of select="$vendorCode"/></vendorCode>
               <vendorTrackingNumber><xsl:value-of select="$vendorTrackingNumber"/></vendorTrackingNumber>
               <vendorSubmissionDate><xsl:value-of select="$vendorSubmissionDate"/></vendorSubmissionDate>
            </vendorInfo>
            <requestContext>
               <key><xsl:value-of select="$key"/></key>
               <value><xsl:value-of select="$value"/></value>
            </requestContext>
         </messageScanAttachmentRequest>
      </tns:scanAttachmentRequest>
	</soapenv:Body>
	</soapenv:Envelope>
	</xsl:variable>
	<dp:set-variable name="'var://context/swa'" value="$soap" />
	<dp:set-http-request-header name="'Content-Type'" value="'application/soap+xml'" />	
	<xsl:variable name="v" select="'attachment://swa/cid:Attach'" />
	<dp:url-open target="{$v}" data-type="base64" response="ignore">
	<xsl:copy-of select="$documentContent" />
	</dp:url-open>
</xsl:template>
</xsl:stylesheet>


    mplate>
</xsl:stylesh