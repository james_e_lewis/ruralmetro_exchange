import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;

import com.ibm.broker.plugin.*; 
import com.ibm.broker.config.proxy.*; 
import com.ibm.mq.*;


import java.util.ArrayList; 
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.ListIterator; 
import java.util.Properties;
import java.util.jar.*; 
import java.util.Enumeration; 
import java.util.Iterator; 
public class RuntimeStatusFlow_RuntimeStatuJava extends MbJavaComputeNode {

	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal alt = getOutputTerminal("alternate");

		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		try {
			// create new message as a copy of the input
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);
			// ----------------------------------------------------------
			// Add user code below

			// End of user code
			// ----------------------------------------------------------
		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}
		 String broker = "RMDEV"; 
	       String eg = "EG4"; 
	       String mflow1 = "AdvanceClaim_TripPreprocessor_MF"; 
	       String mflow2 = "AdvanceClaim_TripSender_MF";
	    //   AdvanceClaim_TripUpload_App
	        
	          MQConfigManagerConnectionParameters   conparams   = new MQConfigManagerConnectionParameters("10.2.1.66",1414,"RMDEV"); 
	           
	      
	          // ---------------------------------------------------------- 
	         // Add user code below 
	          try {
	        	  
	        	  
				ConfigManagerProxy   cmp   =   ConfigManagerProxy.getInstance(conparams); 
				
				
				  ConfigManagerProxy.setRetryCharacteristics(10000); 
				  
				  // Topology details
				  TopologyProxy      tp   =   cmp.getTopology(); 
				  int NumberOfConnections=	  tp.getNumberOfConnections();
				  
			
				
				 // Broker Details :-
				  
				  BrokerProxy         bp   =   tp.getBrokerByName(broker); 
				  Boolean Brokerstatus =  bp.isRunning();
				  String QueueManagerName=	bp.getQueueManagerName();
				  
				  String BrokerName =	bp.getName();
				  String BrokerOSName =		bp.getBrokerOSName();
				  String BrokerOSVersion =	bp.getBrokerOSVersion();
				  int BrokerVersion =	bp.getBrokerVersion();
				  
				  MbMessage localEnv= outAssembly.getLocalEnvironment(); 

				 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "Brokerstatus", Brokerstatus); 
				 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "QueueManagerName", QueueManagerName); 
				 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "BrokerName", BrokerName); 
				 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "BrokerOSName", BrokerOSName); 
				 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "BrokerOSVersion", BrokerOSVersion); 
				 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "BrokerVersion", BrokerVersion); 
				  
				  int i =0;
				  int j =0;
				  int k =0;
				  int l =0;
				  // EG Details
				  
				  ExecutionGroupProxy ep   =   bp.getExecutionGroupByName(eg);
				  String Eg= ep.getName();
				  Boolean EGPortstatus = ep.isDebugPortActive();
				  Boolean EGstatus=  ep.isRunning();
				  localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "EGPortstatus", EGPortstatus); 
					 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "EGstatus", EGstatus); 
				  MessageFlowProxy   mpflow =  ep.getApplicationByName("AdvanceClaim_TripUpload_App").getMessageFlowByName(mflow1);
				
				  Enumeration<DeployedObject>  DeployedObjects= ep.getDeployedObjects();
				  
				 // ArrayList DeployedObject = new ArrayList();
				  String[] DeployedObject = new String[15];
				  String[] mpflowObjectReal = new String[15];
				  Boolean[] flowtatus = new Boolean[15];
				  Date[] DeployTime = new Date[15];
				  String[] FullName = new String[15];
				  Date[] ModifyTime = new Date[15];
				  String[] flowqueue = new String[15];
				  int[] AdditionalInstances = new int[15];
				  String[] Version = new String[15];
				  String[] RuntimeProperties = new String[15];
				  String[] getadvance = new String[15];
				  while (DeployedObjects.hasMoreElements()){
				         
				         
						DeployedObject[i] =DeployedObjects.nextElement().toString();
						
						
						  // Message Flow Details :-
						//  MessageFlowProxy   mp   =   ep.getMessageFlowByName(mflow);
						  MessageFlowProxy   mp =  ep.getApplicationByName(DeployedObject[i]).getMessageFlowByName(mflow1);
						  
						 flowtatus[i]=	  mp.isRunning();
						//  String BARFileName=  mp.getBARFileName();
						   DeployTime[i]=  mp.getDeployTime();
					//	  ExecutionGroupProxy ExecutionGroup=  mp.getExecutionGroup();
						   FullName[i]=  mp.getFullName();
						   ModifyTime[i]=  mp.getModifyTime();
						//  String Flowname=  mp.getName();
						  flowqueue=  mp.getQueues();
						  int totalqueue = flowqueue.length;
						  int QueueType=0;
						  int MaximumDepth=0;
						  int CurrentDepth=0;
						  int OpenInputCount=0;
						  int InhibitGet=0;
					//	  for(l=1; l<totalqueue; l++){
							  
							  String Queuecheck =flowqueue[l].replace(".QA", ".QL");
							  try {
							  MQQueueManager qMgr = new MQQueueManager(QueueManagerName);

					          int openOptions = MQC.MQOO_OUTPUT         |    // for put access 
					                            MQC.MQOO_INQUIRE        |    // check q depth 
					                            MQC.MQOO_INPUT_AS_Q_DEF |    // for get access 
					                            MQC.MQOO_FAIL_IF_QUIESCING;  // break on endmqm 
					          MQQueue outQ =  qMgr.accessQueue(Queuecheck, openOptions);
							   
					    QueueType=   outQ.getQueueType();
					       MaximumDepth=   outQ.getMaximumDepth();
					        
						           CurrentDepth = outQ.getCurrentDepth();// no alternate user id 
						         OpenInputCount=  outQ.getOpenInputCount();
						         InhibitGet= outQ.getInhibitGet();
							} catch (MQException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}  
					
					i=i+1;
				//}
						  
						  RuntimeProperties=	  mp.getRuntimePropertyNames();
						
						  
						
						   AdditionalInstances[i]=  mp.getAdditionalInstances();
						//  String Eg=  mp.getActivityLog();
						 Version[i]=	  mp.getVersion();
						//  String Eg=	  mp.getUserDefinedPropertyNames();
						 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "DeployedObject", DeployedObject[i]); 
						 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "mflow1", mflow1); 
						 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "flowtatus", flowtatus[i]); 
					//	 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "DeployTime", DeployTime[i]); 
						 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "FullName", FullName[i]); 
						 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "QueueName", Queuecheck); 
						 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "MaximumDepth", MaximumDepth); 
						 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "CurrentDepth", CurrentDepth); 
						 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "OpenInputCount", OpenInputCount); 
						 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "InhibitGet", InhibitGet); 
					//	 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "ModifyTime", ModifyTime[i]); 
						 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "AdditionalInstances", AdditionalInstances[i]); 
					//	 localEnv.getRootElement().createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "flowqueue", flowqueue);
						  i =i+1;	  
				  }   
				//  Properties BasicProperties =  ep.getBasicProperties();
				  int DebugPort =  ep.getDebugPort();
				//  String LastUpdateUser =	  ep.getLastUpdateUser();
				  Enumeration<DeployedObject> MessageFlowDependencies =	  ep.getMessageFlowDependencies();
				  String[] MessageFlowDependency = new String[15];
				  while (MessageFlowDependencies.hasMoreElements()){
					  
				         MessageFlowDependency[j] =MessageFlowDependencies.nextElement().toString();
				         j =j+1;
				  } 
				  Enumeration<DeployedObject> MessageSets =	  ep.getMessageSets();
				  String[] MessageSet = new String[15];
				  while (MessageSets.hasMoreElements()){
					 
				         MessageSet[k] =MessageSets.nextElement().toString();
				        k =k+1;
				  } 
				//  int NumberOfSubcomponents =	  ep.getNumberOfSubcomponents();
				 // String[] EGQueues =  ep.getQueues();
				//  GregorianCalendar TimeOfLastUpdate =  ep.getTimeOfLastUpdate();
				
				  
				
				  
				  
				  
				 
				  
				  
				//  String ConfigManagerFullVersion =	  cmp.getConfigManagerFullVersion();
			//	  int ConfigManagerVersion =  cmp.getConfigManagerVersion();
				//  String ConfigManagerOSName =  cmp.getConfigManagerOSName();
				  
				/*  
					try { 
					TopologyProxy topology = cmp.getTopology(); 
					if (topology != null) { 
					BrokerProxy b = topology.getBrokerByName(broker); 
					if (b != null) { 
					boolean brkRunning = b.isRunning();
					if (brkRunning) { 
					String Bstatus= "broker " + broker + " is running";
					} else { 
						String Bstatus="broker " + broker + " is stopped"; 
					} 
					ExecutionGroupProxy egName = b.getExecutionGroupByName(eg); 
					if (eg != null) {


					boolean egRunning = egName.isRunning();
					if (egRunning) { 
						String estatus="execution grp " + eg + " is running";
					} else { 
						String estatus="execution grp " + eg + " is stopped"; 
					} 
					MessageFlowProxy mf = egName.getMessageFlowByName(mflow1); 
					if (mf != null) { 
					boolean isRunning = mf.isRunning(); 
					String mstatus="Flow "+mflow1+" on " + egName+" on "+broker+" is "; 
					if (isRunning) { 
						String mstatus1=("running"); 
					} else { 
						String mstatus1="stopped"; 
					} 
					} else { 
						String mstatus3="No such flow "+mflow1; 
					} 
					} else { 
						String estatus1="No such exegrp "+eg+"!"; 
					} 
					} else { 
						String bstatus1="No such broker "+broker; 
					} 
					} else { 
						String tstatus1="Topology not available!"; 
					} 
					} catch(ConfigManagerProxyPropertyNotInitializedException ex) { 
						String cstatus1="Comms problem! "+ex; 
					} 
					
				  */
				 
				  
			}
	          catch (ConfigManagerProxyLoggedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (ConfigManagerProxyPropertyNotInitializedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}   
		
		// The following should only be changed
		// if not propagating message to the 'out' terminal
		out.propagate(outAssembly);

	}

}
