import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;


public class ReProcessingSQLExecution_MF_ExtractTransactionAndSend extends
		MbJavaComputeNode {

	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal alt = getOutputTerminal("alternate");

		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		try {
			// create new message as a copy of the input
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);
			// ----------------------------------------------------------
			// Add user code below

int noPayloadToProcess =1;
MbMessage Env= outAssembly.getGlobalEnvironment();
MbElement TotalTransactionRef =Env.getRootElement().getFirstElementByPath("JOB/TotalTransactionsForJob");
String totalNumberofTransactionsasString = TotalTransactionRef.getValueAsString();

int totalNumberofTransactions=  Integer.parseInt(totalNumberofTransactionsasString);

while (noPayloadToProcess <= totalNumberofTransactions)
{
	
	/*MbElement domain=Env.getRootElement().getFirstElementByPath("JOB/ParsingAsXML[noPayloadToProcess]/XMLNSC/Root/XMLNSC");
	String DomainName = domain.getName();*/


	MbElement payload=Env.getRootElement().getFirstElementByPath("JOB");
	MbElement payloadRoot = payload.getLastChild();
	String PayloadReal = payloadRoot.getName();
	MbElement payloadRoot2 = payloadRoot.getLastChild();
	String PayloadReal2 = payloadRoot2.getName();
	
	
	String PayloadReal1 = payloadRoot.getValueAsString();
	
	MbElement Queue=Env.getRootElement().getFirstElementByPath("JOB/TransactionList[1]");
	MbElement queueRoot = Queue.getLastChild();
	String queueReal = queueRoot.getValueAsString();
	


noPayloadToProcess =noPayloadToProcess+1;

}

			// End of user code
			// ----------------------------------------------------------
		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}
		// The following should only be changed
		// if not propagating message to the 'out' terminal
		out.propagate(outAssembly);

	}

}
