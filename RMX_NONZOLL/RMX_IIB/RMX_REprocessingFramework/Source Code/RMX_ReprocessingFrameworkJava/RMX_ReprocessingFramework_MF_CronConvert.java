import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;
import com.ibm.broker.plugin.MbXMLNSC;

import org.quartz.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import com.ibm.broker.plugin.MbElement;
public class RMX_ReprocessingFramework_MF_CronConvert extends MbJavaComputeNode {
	static Date date1 = null;
	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		
		// Code to get the current system time.
		Date date = new Date();
		String systemDate=date.toString();
	    System.out.println(date);
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal alt = getOutputTerminal("alternate");
		

		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		try {
			// create new message as a copy of the input
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);
			// ----------------------------------------------------------
			// Add user code below

			// Code to check when is the next schedule by using the value got above from table. 
		     // below code will convert the cron expression into system date format and return the next execution time.
			
			MbElement CronEpress = outMessage.getRootElement().getFirstElementByPath("XMLNSC/JobDetails/Schedule");
			String DateConvert=CronEpress.getValueAsString();
			
			CronExpression cronExpression;
			//CronExpression.validateExpression(DateConvert);
			try {
				cronExpression = new CronExpression(DateConvert);
				 MbElement creatingDate= outMessage.getRootElement().getFirstElementByPath("XMLNSC/JobDetails");
				// String covertedDate = "ConvertedDate";
				// String covertedDate1 = creatingDate.getValueAsString();
				 MbElement LastRunTimeForThisJob= outMessage.getRootElement().getFirstElementByPath("XMLNSC/JobDetails/LastRunTimeForThisJob");
				 String lastExecutedDate = LastRunTimeForThisJob.getValueAsString();
				 
				 SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				 Date dating = format.parse(lastExecutedDate);
				 String FormatedLastRunDate = dating.toString();
			
				 Date NextScheduledExecutionBasedonLastRun1 = cronExpression.getTimeAfter(dating);
				 // Next line is for cron validation
				 if(NextScheduledExecutionBasedonLastRun1 != null) {
					 String NextScheduledExecutionBasedonLastRun = NextScheduledExecutionBasedonLastRun1.toString();

				
				 if(NextScheduledExecutionBasedonLastRun1.before(date))
					{
					 
					 creatingDate.createElementAsFirstChild(MbXMLNSC.FIELD, "NEEDTOEXECUTE", "YES"); 
					 MbMessage localEnv= outAssembly.getLocalEnvironment(); 

					 localEnv.getRootElement(). 
					 createElementAsLastChild (MbElement.TYPE_NAME, "Destination", null). 
					 createElementAsLastChild(MbElement.TYPE_NAME, "RouterList", null). 
					 createElementAsLastChild(MbElement.TYPE_NAME, "DestinationData", null). 
					createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "labelname", "Execute"); 
					
					 out.propagate(outAssembly);
					}
				 else if(NextScheduledExecutionBasedonLastRun1.after(date))
				 {
					 creatingDate.createElementAsFirstChild(MbXMLNSC.FIELD, "NEEDTOEXECUTE", "No"); 
					 MbMessage localEnv= outAssembly.getLocalEnvironment(); 

					 localEnv.getRootElement(). 
					 createElementAsLastChild (MbElement.TYPE_NAME, "Destination", null). 
					 createElementAsLastChild(MbElement.TYPE_NAME, "RouterList", null). 
					 createElementAsLastChild(MbElement.TYPE_NAME, "DestinationData", null). 
					createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "labelname", "NotExecute"); 
				
					 out.propagate(outAssembly);
				 }
				 
				 } else {
					 alt.propagate(outAssembly);
					}
				
				 
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				alt.propagate(outAssembly);
				e.printStackTrace();
			}
			
			// End of user code
			// ----------------------------------------------------------
		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}
		// The following should only be changed
		// if not propagating message to the 'out' terminal

		

	}

}
