package utility;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ConvertBadChars {
	public static byte[] FixBadChars(String replaceChar, byte[] payload) {
	       // String xml10pattern = "[\u007F-\uFFFF]"; 	        
//	        String xml10pattern = "[^"
//                    + "\u0009\r\n"
//                    + "\u0020-\uD7FF"
//                    + "\uE000-\uFFFD"
//                    + "\ud800\udc00-\udbff\udfff"
//                    + "]";
			
		//String xml10pattern = "é" + "";
		String xml10pattern ="";
		
		Map<String, String> tokens = new HashMap<String, String>();
		tokens.put("é", "e");
        tokens.put("ý", "y");
        tokens.put("û", "u");
	    tokens.put("", "");
	    tokens.put("&#8211;", "-");
	    tokens.put("--", "-");
	    tokens.put("”", "''");
	    tokens.put("“", "''");
	    tokens.put("’", "''");
	    tokens.put("°", "degree");
        tokens.put("é", "e");
	    tokens.put("˜ ", "~");
        tokens.put("è", "e");
	    tokens.put("&amp;", "and");
	    tokens.put("& ", "and ");
	    tokens.put("ç", "c");
	    tokens.put("", "");
	    tokens.put("&#8212;", "-");
	    tokens.put("ñ", "n ");
	    tokens.put("½", "half");
	    tokens.put("&#253;", "-");
	    tokens.put("²", "2");
	    tokens.put("&#215;", "X");
	    tokens.put("&#233;", "e");
	    tokens.put("&#194;", "a");
	   
	    tokens.put("╫", "X");
	    tokens.put("Θ", "e");
	    tokens.put("┬", "a");
	    tokens.put("û", "u");
	    tokens.put("ù", "u");
	    tokens.put("²", "2");
//	   
	    
	   
//	   for(int i=0; i<10; i++){
//i=i+1;
//
//
//	    
//	  }
	    String legal = new String(payload); 
	    byte[] legalBytes = null;
	    
	    Iterator it= tokens.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	      String Key=  pair.getKey().toString();
	      String Value=  pair.getValue().toString();
	        
 
            
             legal = legal.replaceAll(Key, Value); 
      
            
            
            
	     //   it.remove(); // avoids a ConcurrentModificationException
	       

	    }
	    legalBytes = legal.getBytes();
	    return legalBytes;
//	    while (it.hasNext()) {
//	        Map.Entry entry = (Map.Entry)it.next();
//	        entry.getKey();
//	        entry.getValue();
//	    }

		
           // below this String legal = new String(payload).replaceAll(xml10pattern, replaceChar); 
//		
//		 String legal = new String(payload); 
//            
//            String legal1 = legal.replaceAll(xml10pattern, replaceChar); 
//      
//            byte[] legalBytes = legal1.getBytes();
//            
//            return legalBytes;
	}
}
	
