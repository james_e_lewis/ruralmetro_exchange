<?xml version="1.0" encoding="UTF-8"?><xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="http://www.nemsis.org" xmlns="http://www.nemsis.org" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:include schemaLocation="commonTypes_v3.xsd"/>
	<xs:include schemaLocation="dAgency_v3.xsd"/>
	<xs:include schemaLocation="eCustom_v3.xsd"/>
	<xs:include schemaLocation="eState_v3.xsd"/>
	<xs:include schemaLocation="eRecord_v3.xsd"/>
	<xs:include schemaLocation="eResponse_v3.xsd"/>
	<xs:include schemaLocation="eDispatch_v3.xsd"/>
	<xs:include schemaLocation="eCrew_v3.xsd"/>
	<xs:include schemaLocation="eTimes_v3.xsd"/>
	<xs:include schemaLocation="ePatient_v3.xsd"/>
	<xs:include schemaLocation="ePayment_v3.xsd"/>
	<xs:include schemaLocation="eScene_v3.xsd"/>
	<xs:include schemaLocation="eSituation_v3.xsd"/>
	<xs:include schemaLocation="eInjury_v3.xsd"/>
	<xs:include schemaLocation="eArrest_v3.xsd"/>
	<xs:include schemaLocation="eHistory_v3.xsd"/>
	<xs:include schemaLocation="eNarrative_v3.xsd"/>
	<xs:include schemaLocation="eVitals_v3.xsd"/>
	<xs:include schemaLocation="eLabs_v3.xsd"/>
	<xs:include schemaLocation="eExam_v3.xsd"/>
	<xs:include schemaLocation="eProtocols_v3.xsd"/>
	<xs:include schemaLocation="eMedications_v3.xsd"/>
	<xs:include schemaLocation="eProcedures_v3.xsd"/>
	<xs:include schemaLocation="eDisposition_v3.xsd"/>
	<xs:include schemaLocation="eDevice_v3.xsd"/>
	<xs:include schemaLocation="eOutcome_v3.xsd"/>
	<xs:include schemaLocation="eOther_v3.xsd"/>
	<xs:include schemaLocation="eAirway_v3.xsd"/>
	<xs:element name="EMSDataSet">
		<xs:annotation>
			<xs:documentation>Root Tag For Dataset</xs:documentation>
		</xs:annotation>
		<xs:complexType>
			<xs:sequence>
				<xs:element id="eStateSection" minOccurs="0" name="eState" type="eState">
					<xs:annotation>
						<xs:documentation>State Required Elements</xs:documentation>
					</xs:annotation>
				</xs:element>
				<xs:element id="HeaderGroup" maxOccurs="unbounded" name="Header">
					<xs:annotation>
						<xs:documentation>Header for file</xs:documentation>
					</xs:annotation>
					<xs:complexType>
						<xs:sequence>
							<xs:element id="DemographicGroup" name="DemographicGroup">
								<xs:annotation>
									<xs:documentation>Demographic information related to this agency</xs:documentation>
								</xs:annotation>
								<xs:complexType>
									<xs:sequence>
										<xs:element id="EMSDataset.EMSAgencyUniqueStateID" name="dAgency.01" type="EMSAgencyStateID">			
											<xs:annotation>
												<xs:documentation>
													<nemsisTacDoc>
														<number>dAgency.01</number>
														<name>EMS Agency Unique State ID</name>
														<national>Yes</national>
														<state>Yes</state>
														<definition>The unique ID assigned to the EMS Agency which is associated with all state licensure numbers and information.</definition>
														
														<usage>Mandatory</usage>
														<performanceMeasuresGroup>
															<performanceMeasure>Airway</performanceMeasure>
															<performanceMeasure>Cardiac Arrest</performanceMeasure>
															<performanceMeasure>Pediatric</performanceMeasure>
															<performanceMeasure>Response</performanceMeasure>
															<performanceMeasure>STEMI</performanceMeasure>
															<performanceMeasure>Stroke</performanceMeasure>
															<performanceMeasure>Trauma</performanceMeasure>
														</performanceMeasuresGroup>
														<comment>This may be the EMS Agency Name or a unique number assigned by the state EMS office. This is required to document multiple license types and numbers associated with the same EMS Agency.</comment>
														
													</nemsisTacDoc>
												</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="EMSDataset.EMSAgencyNumber" name="dAgency.02">
											<xs:annotation>
												<xs:documentation>
													<nemsisTacDoc>
														<number>dAgency.02</number>
														<name>EMS Agency Number</name>
														<national>Yes</national>
														<state>Yes</state>
														<definition>The state-assigned provider number of the responding agency</definition>
														<v2Number>D01_01</v2Number>
														<usage>Mandatory</usage>
														<performanceMeasuresGroup>
															<performanceMeasure>Airway</performanceMeasure>
															<performanceMeasure>Cardiac Arrest</performanceMeasure>
															<performanceMeasure>Pediatric</performanceMeasure>
															<performanceMeasure>Response</performanceMeasure>
															<performanceMeasure>STEMI</performanceMeasure>
															<performanceMeasure>Stroke</performanceMeasure>
															<performanceMeasure>Trauma</performanceMeasure>
														</performanceMeasuresGroup>
														<comment>This is the primary identifier for the entire Demographic Section.  Each of the Demographic sections must be associated with an EMS Agency Number. An EMS Agency can have more than one Agency Number within a state. This reflects the ability for an EMS Agency to have a different number for each service type or location (based on state implementation).</comment>
														
													</nemsisTacDoc>
												</xs:documentation>
											</xs:annotation>
											<xs:complexType>
												<xs:simpleContent>
													<xs:extension base="EMSAgencyNumber"/>
												</xs:simpleContent>
											</xs:complexType>
										</xs:element>
										<xs:element id="EMSDataset.EMSAgencyState" name="dAgency.04">
											<xs:annotation>
												<xs:documentation>
													<nemsisTacDoc>
														<number>dAgency.04</number>
														<name>EMS Agency State</name>
														<national>Yes</national>
														<state>Yes</state>
														<definition>The state/territory which assigned the EMS agency number.</definition>
														<v2Number>D01_03</v2Number>
														<usage>Mandatory</usage>
														<performanceMeasuresGroup>
															<performanceMeasure>Airway</performanceMeasure>
															<performanceMeasure>Cardiac Arrest</performanceMeasure>
															<performanceMeasure>Pediatric</performanceMeasure>
															<performanceMeasure>Response</performanceMeasure>
															<performanceMeasure>STEMI</performanceMeasure>
															<performanceMeasure>Stroke</performanceMeasure>
															<performanceMeasure>Trauma</performanceMeasure>
														</performanceMeasuresGroup>
														<comment>This has been clarified to reflect that it is the state in which the EMS Agency resides and the state associated with the EMS Agency number.
																	&lt;br/&gt;&lt;br/&gt;GNIS Codes Website: &lt;a&gt;http://geonames.usgs.gov/domestic/download_data.htm&lt;/a&gt;</comment>
														
													</nemsisTacDoc>
												</xs:documentation>
											</xs:annotation>
											<xs:complexType>
												<xs:simpleContent>
													<xs:extension base="ANSIStateCode"/>
												</xs:simpleContent>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element id="eCustomConfigurationSection" minOccurs="0" name="eCustomConfiguration" type="eCustomConfiguration">
							</xs:element>
							<xs:element id="PatientCareReportGroup" maxOccurs="unbounded" name="PatientCareReport">
								<xs:annotation>
									<xs:documentation>Container Tag to hold each instance of a patient care report (PCR)</xs:documentation>
								</xs:annotation>
								<xs:complexType>
									<xs:sequence>
										<xs:element id="eRecordSection" name="eRecord" type="eRecord">
											<xs:annotation>
												<xs:documentation>Patient Record Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eResponseSection" name="eResponse" type="eResponse">
											<xs:annotation>
												<xs:documentation>Unit Agency Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eDispatchSection" name="eDispatch" type="eDispatch">
											<xs:annotation>
												<xs:documentation>Dispatch Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eCrewSection" minOccurs="0" name="eCrew" type="eCrews">
											<xs:annotation>
												<xs:documentation>Crew Member Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eTimesSection" name="eTimes" type="eTimes">
											<xs:annotation>
												<xs:documentation>Call Event Times Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="ePatientSection" name="ePatient" type="ePatient">
											<xs:annotation>
												<xs:documentation>Patient Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="ePaymentSection" name="ePayment" type="ePayment">
											<xs:annotation>
												<xs:documentation>Insurance/Payment Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eSceneSection" name="eScene" type="eScene">
											<xs:annotation>
												<xs:documentation>Incident Scene Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eSituationSection" name="eSituation" type="eSituation">
											<xs:annotation>
												<xs:documentation>Situation Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eInjurySection" name="eInjury" type="eInjury">
											<xs:annotation>
												<xs:documentation>Injury Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eArrestSection" name="eArrest" type="eArrest">
											<xs:annotation>
												<xs:documentation>Cardiac Arrest Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eHistorySection" name="eHistory" type="eHistory">
											<xs:annotation>
												<xs:documentation>Patient History Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eNarrativeSection" minOccurs="0" name="eNarrative" type="eNarrative">
											<xs:annotation>
												<xs:documentation>Call Event Narrative</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eVitalsSection" name="eVitals" type="eVitals">
											<xs:annotation>
												<xs:documentation>Patient Vital Sign Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eLabsSection" minOccurs="0" name="eLabs" type="eLabs">
											<xs:annotation>
												<xs:documentation>Lab Results Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eExamSection" minOccurs="0" name="eExam" type="eExam">
											<xs:annotation>
												<xs:documentation>Patient Exam Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eProtocolsSection" name="eProtocols" type="eProtocols">
											<xs:annotation>
												<xs:documentation>Medical Protocols Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eMedicationsSection" name="eMedications" type="eMedications">
											<xs:annotation>
												<xs:documentation>Intervention Medications Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eProceduresSection" name="eProcedures" type="eProcedures">
											<xs:annotation>
												<xs:documentation>Intervention Procedures Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eAirwaysSection" minOccurs="0" name="eAirway" type="eAirways">
											<xs:annotation>
												<xs:documentation>Airway Device Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eDevicesSection" minOccurs="0" name="eDevice" type="eDevices">
											<xs:annotation>
												<xs:documentation>Medical Device Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eDispositionSection" name="eDisposition" type="eDisposition">
											<xs:annotation>
												<xs:documentation>Patient Disposition Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eOutcomeSection" name="eOutcome" type="eOutcome">
											<xs:annotation>
												<xs:documentation>Patient Outcome Information</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eCustomResultsSection" minOccurs="0" name="eCustomResults" type="eCustomResults">
											<xs:annotation>
												<xs:documentation>Custom Data Elements Results</xs:documentation>
											</xs:annotation>
										</xs:element>
										<xs:element id="eOtherSection" name="eOther" type="eOther">
											<xs:annotation>
												<xs:documentation>Other Miscellaneous Information</xs:documentation>
											</xs:annotation>
										</xs:element>
									</xs:sequence>
									<!--<xs:attribute name="RecordStatus" type="RecordStatus" use="optional"/>
									<xs:attribute name="CorrelationID" type="CorrelationID" use="optional"/>-->
								</xs:complexType>
							</xs:element>
						</xs:sequence>
					</xs:complexType>
				</xs:element>
			</xs:sequence>
		</xs:complexType>
	</xs:element>
</xs:schema>