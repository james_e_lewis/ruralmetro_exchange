package rmxcache;
//package Utilities;

import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbGlobalMap;
import com.ibm.broker.plugin.MbGlobalMapSessionPolicy;

public class CacheUtil {
	/**
	 * Method to get a value from Global Cache using map name and key
	 * @throws MbException 
	 */
	public static String getProfitCenterCodeFromCache(String Site, String Unit)  {
		
		String profitCenterCode = null;
		
		try
		{
		
		MbGlobalMap ProfiCenterMap = MbGlobalMap.getGlobalMap("ProfiCenterMap");
			
			String key = Site+"_"+Unit;
			profitCenterCode =	(String) ProfiCenterMap.get(key);
			
	
		}
		catch(MbException mbe)
		{
			System.out.println(mbe.getMessage());
			mbe.printStackTrace();
		}
		
		if(profitCenterCode == null) {
		return "null";
		}
		else
		{
			return profitCenterCode;
		}
	}
	
	/**
	 * Method to add all the key-value pairs for a map in Global Cache
	 */	
	
	
	/**
	 * Method to get a add a key-value pair to a map in Global Cache
	 * @throws MbException 
	 */	
	
	public static Boolean addPCCodetoCache(String Site, String Unit, String ProfitCenter) {
			
		try
		{
			
			
			MbGlobalMap ProfiCenterMap = MbGlobalMap.getGlobalMap("ProfiCenterMap");
			
			if(ProfiCenterMap.get("initKey") == null) {
				ProfiCenterMap.put("initKey", "true");
			}
			String SiteandUnit_Key = Site+"_"+Unit;
	
			if(ProfiCenterMap.containsKey(SiteandUnit_Key)) {
				ProfiCenterMap.update(SiteandUnit_Key,ProfitCenter);
			} else {
				ProfiCenterMap.put(SiteandUnit_Key, ProfitCenter);
			}
			
		}
		catch(MbException mbe) {
			System.out.println(mbe.getMessage());
			mbe.printStackTrace();
			return Boolean.FALSE;
		}		
		
		return Boolean.TRUE;
	}	

}
