<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:NS1="http://www.nemsis.org">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" omit-xml-declaration="yes" indent="yes" />
  <xsl:strip-space  elements="*"/>

  <xsl:template match="/">
       <xsl:apply-templates />
  </xsl:template>

  <xsl:template match="NS1:E05">
    <NS1:E05>
      <NS1:E05_01><xsl:value-of select="./NS1:E05_01"/></NS1:E05_01>
      <NS1:E05_02><xsl:value-of select="./NS1:E05_02"/></NS1:E05_02>
      <NS1:E05_03><xsl:value-of select="./NS1:E05_03"/></NS1:E05_03>
      <NS1:E05_04><xsl:value-of select="./NS1:E05_04"/></NS1:E05_04>
      <NS1:E05_05><xsl:value-of select="./NS1:E05_05"/></NS1:E05_05>
      <NS1:E05_06><xsl:value-of select="./NS1:E05_06"/></NS1:E05_06>
      <NS1:E05_07><xsl:value-of select="./NS1:E05_07"/></NS1:E05_07>
      <NS1:E05_08><xsl:value-of select="./NS1:E05_08"/></NS1:E05_08>
      <NS1:E05_09><xsl:value-of select="./NS1:E05_09"/></NS1:E05_09>
      <NS1:E05_10><xsl:value-of select="./NS1:E05_10"/></NS1:E05_10>
      
      <xsl:choose>
        <xsl:when test="string-length(./NS1:E05_11) &gt; 0">
          <NS1:E05_11><xsl:value-of select="./NS1:E05_11"/></NS1:E05_11>
        </xsl:when>
        <xsl:otherwise>
          <NS1:E05_11>1990-01-01T12:00:00Z</NS1:E05_11>
        </xsl:otherwise>
      </xsl:choose>
      
      <NS1:E05_12><xsl:value-of select="./NS1:E05_12"/></NS1:E05_12>
      <NS1:E05_13><xsl:value-of select="./NS1:E05_13"/></NS1:E05_13>
    </NS1:E05>
  </xsl:template>
  
  <xsl:template match="*">
       <xsl:if test="string-length(.//text())>0">
            <xsl:element name="{name(.)}" >
                 <xsl:apply-templates />
            </xsl:element>
       </xsl:if>
  </xsl:template>

</xsl:stylesheet>
