package com.rmx;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ListResourceBundle;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbElement;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbGlobalMap;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;
import com.ibm.broker.plugin.MbXMLNSC;

public class NEMISIS2To3_Converter extends MbJavaComputeNode {


                public void evaluate(MbMessageAssembly inAssembly) throws MbException {
                                MbOutputTerminal out = getOutputTerminal("out");
                //            MbOutputTerminal alt = getOutputTerminal("alternate");

                                MbMessage inMessage = inAssembly.getMessage();
                                // create an empty output message
                                MbMessage outMessage = new MbMessage();
                                MbMessageAssembly outAssembly = new MbMessageAssembly(inAssembly,
                                                                outMessage);
                                FileInputStream in = null;
                                try {

                                                // copy headers from the input message
                                                copyMessageHeaders(inMessage, outMessage);

                                                // ----------------------------------------------------------
                                                // Add user code below

                                                // Extract NEMSIS2 data when source is XMLNSC Parser
                                                MbMessage message = inAssembly.getMessage();
                                                MbElement root = message.getRootElement();
                                                MbElement body = root.getLastChild();
                                                byte[] InputStream = null;
                                                InputStream = body.toBitstream("", "", "", 0, 0, 0);

                                                String MessageBodyString = new String(InputStream);
                                                
                                                // Retrieve the MQMD from the global cache.

                                                String XSLTFileName = "v2v3EMSDataSet.xsl";
                                                byte[] retrieveXSLTFile = (byte[]) MbGlobalMap.getGlobalMap().get(XSLTFileName);
                                     if (retrieveXSLTFile == null)
                                     {
                                                               // save the MsgID for use as the key to the MQMD stored in the global cache 
                                     //        in = new FileInputStream("C:/Users/kmandila/Desktop/v2v3Translation_002/v2v3Translation/v2v3EMSDataSet.xsl");
                                                String Filename = "C:/Users/kmandila/Desktop/v2v3Translation_002/v2v3Translation/v2v3EMSDataSet.xsl";
                                                byte[] retrieveMQMD =               loadBinaryFile(Filename);
                                                               // Put key and data into map
                                                               MbGlobalMap.getGlobalMap().put(XSLTFileName, retrieveMQMD);
                                                               retrieveXSLTFile = (byte[]) MbGlobalMap.getGlobalMap().get(XSLTFileName);
                                                 }
                                
                                     if (retrieveXSLTFile == null)
                                     {
                                                throw new MbUserException(this,
                                      "evaluate",
                                      GlobalCacheMessages.MESSAGE_SOURCE,
                                                  GlobalCacheMessages.DATA_NOT_FOUND, 
                                                          "Tried to get XSL File from global cache, but could not retrieve the data",
                                                          new String[] {XSLTFileName});
                                                        
                                                 }
                                     
                                    String retrieveXSLTFileString =   openFileToString(retrieveXSLTFile);
                                                String Outpayload = myTransformer(MessageBodyString, retrieveXSLTFileString);
                                                byte[] MessageBodyByteArray1 = (byte[]) (Outpayload.getBytes());
                                                // localEnv.getRootElement().createElementAsFirstChild(MbElement.TYPE_NAME,"Payload",Outpayload);
                                                MbElement outMsgRootEl = outMessage.getRootElement();
                                                // create the 'Body' XMLNSC element
                                                String parserName = MbXMLNSC.PARSER_NAME;
                                                String messageType = "";
                                                String messageSet = "";
                                                String messageFormat = "";
                                                int encoding = 0;
                                                int ccsid = 0;
                                                int options = 0;
                                                outMsgRootEl.createElementAsLastChildFromBitstream(
                                                                                MessageBodyByteArray1, parserName, messageType, messageSet,
                                                                                messageFormat, encoding, ccsid, options);

                                                // End of user code
                                                // ----------------------------------------------------------
                                } catch (MbException e) {
                                                // Re-throw to allow Broker handling of MbException
                                                throw e;
                                } catch (RuntimeException e) {
                                                // Re-throw to allow Broker handling of RuntimeException
                                                throw e;
                                } catch (Exception e) {
                                                // Consider replacing Exception with type(s) thrown by user code
                                                // Example handling ensures all exceptions are re-thrown to be
                                                // handled in the flow
                                                throw new MbUserException(this, "evaluate()", "", "", e.toString(),
                                                                                null);
                                }
                                finally {
                         if (in != null) {
                            try {
                                                                                in.close();
                                                                } catch (IOException e) {
                                                                                // TODO Auto-generated catch block
                                                                                e.printStackTrace();
                                                                }
                         }
                                }
                                
                                // The following should only be changed
                                // if not propagating message to the 'out' terminal
                                out.propagate(outAssembly);

                }
                
                public String openFileToString(byte[] mybytes)
                {
                    String mystring="";

                    for(int i=0; i<mybytes.length; i++)
                    {
                        mystring=new String(Byte.toString(mybytes[i]));
                        mystring+=(" "+Byte.toString(mybytes[i]));
                    }
                    return mystring;
                }
                public byte[] loadBinaryFile (String name) {
                    try {

                        DataInputStream dis = new DataInputStream(new FileInputStream(name));
                        byte[] theBytes = new byte[dis.available()];
                        dis.read(theBytes, 0, dis.available());
                        dis.close();
                        return theBytes;
                    } catch (IOException ex) {
                    }
                    return null;
                } // ()

                public void copyMessageHeaders(MbMessage inMessage, MbMessage outMessage)
                                                throws MbException {
                                MbElement outRoot = outMessage.getRootElement();
                                MbElement header = inMessage.getRootElement().getFirstChild();

                                while (header != null && header.getNextSibling() != null) {
                                                outRoot.addAsLastChild(header.copy());
                                                header = header.getNextSibling();
                                }
                }
                
                public String myTransformer(String payloadXMLString, String xsltFileString)
                                                throws TransformerException, TransformerConfigurationException {
                                StringWriter writer = new StringWriter();

                                // Create a transform factory instance.
                                TransformerFactory transformObj = TransformerFactory.newInstance(
                                                                "net.sf.saxon.TransformerFactoryImpl", this.getClass()
                                                                                                .getClassLoader());

                                Transformer transformer = transformObj.newTransformer(new StreamSource(xsltFileString));
                                                //            new File(xsltFileString)));

                                transformer.transform(new StreamSource(new StringReader(
                                                                payloadXMLString)), new StreamResult(writer));

                                return writer.toString();

                }
                
                  /**
                   * The class is the ResourceBundle containing all the messages for this example.
                   */
                  public static class GlobalCacheMessages extends ListResourceBundle
                  {
                    public static final String MESSAGE_SOURCE = GlobalCacheMessages.class.getName();                                                
                    public static final String DATA_NOT_FOUND = "DATA NOT FOUND";

                
                    
                    private Object[][] messages  = {{DATA_NOT_FOUND, "Data not found in global cache for key {0}" },                                        
                                                   };

                    /* (non-Javadoc)
                     * @see java.util.ListResourceBundle#getContents()
                     */
                    public Object[][] getContents()
                    {
                      return messages;
                    }
                  }

}
