package com.rmetro.reconcile;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;
import com.ibm.mq.MQC;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.rmetro.jaxb.model.JAXBJavaToXML;
import com.rmetro.jaxb.model.Reconciliation;
import com.rmetro.util.BrokerProxyManager;
import com.rmetro.util.BrokerProxyManagerHolder;

public class ACReconciliationAPI_Reconcile extends MbJavaComputeNode {

	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal alt = getOutputTerminal("alternate");

		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		try {
			// create new message as a copy of the input
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);
			// ----------------------------------------------------------
			// Add user code below
			BrokerProxyManager bpm = BrokerProxyManagerHolder.getInstance();
			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn = null;
			Statement statement = null;
			String queryString = null;
			ResultSet rs = null;
			String odsJdbcProvider = bpm.getRmxUdcsProperties().getProperty("rmx_ods_jdbc_provider");
			String zollJdbcProvider = bpm.getRmxUdcsProperties().getProperty("rmx_zoll_jdbc_provider");
			String RMxQueueManager = bpm.getRmxUdcsProperties().getProperty("RMxQueueManager");
			String reconcileQueue = bpm.getRmxUdcsProperties().getProperty("ReconcileQueue");
			String zoll = "ZOLL";
			String tritech = "TRITECH";
			String xmlPayload;
			MQQueueManager qMgr = new MQQueueManager(RMxQueueManager);
			int openOptions = MQC.MQOO_INPUT_AS_Q_DEF | MQC.MQOO_OUTPUT;			
			MQQueue queue =  qMgr.accessQueue(reconcileQueue, openOptions);
			MQMessage msg;
			MQPutMessageOptions pmo = new MQPutMessageOptions();
			Reconciliation rc = new Reconciliation();
			
			//get posted zoll events, loop through result, post to queue
			conn = getJDBCType4Connection(zollJdbcProvider, null);
			statement = conn.createStatement();
			queryString = "SELECT UniqueTripId, TDATE, SOURCEDB, JOB, RUNNUMBER, TRIPSTATUS, FILETYPE FROM RMX_DBINPUT_ZOLL_EVENTS WHERE EVENT_STATUS = 'P' AND EVENT_TIME_ADDED < DATEADD(HH,-2,GETDATE())";
			rs = statement.executeQuery(queryString);
			
			while (rs.next()) {
				msg = new MQMessage();
				rc.setUniqueTripID(rs.getString("UniqueTripId"));
				rc.setTdate(rs.getString("TDATE"));
				rc.setJob(rs.getString("JOB"));
				rc.setSourceDB(rs.getString("SOURCEDB"));
				rc.setRunNumber(rs.getString("RUNNUMBER"));
				rc.setRecordStatus(rs.getString("TRIPSTATUS"));
				rc.setRecordType(rs.getString("FILETYPE"));
				rc.setSourceFlow(zoll);
				msg.writeString(JAXBJavaToXML.reconcileJavaToXml(rc));
				queue.put(msg, pmo);
			}
			
			//get posted tritech events, loop through result set, post to queue
			conn = getJDBCType4Connection(odsJdbcProvider, null);
			statement = conn.createStatement();
			queryString = "SELECT UniqueTripId, TDATE, SOURCEDB, MasterIncidentNumber, TRIPSTATUS, FILETYPE FROM RMX_DBINPUT_TRITECH_EVENTS WHERE EVENT_STATUS = 'P' and 0 = 1";
			rs = statement.executeQuery(queryString);
			
			while (rs.next()) {
				msg = new MQMessage();
				rc.setUniqueTripID(rs.getString("UniqueTripId"));
				rc.setTdate(rs.getString("TDATE"));
				rc.setJob("");
				rc.setSourceDB(rs.getString("SOURCEDB"));
				rc.setRunNumber(rs.getString("MasterIncidentNumber"));
				rc.setRecordStatus(rs.getString("TRIPSTATUS"));
				rc.setRecordType(rs.getString("FILETYPE"));
				rc.setSourceFlow(tritech);
				msg.writeString(JAXBJavaToXML.reconcileJavaToXml(rc));
				queue.put(msg, pmo);
			}

			// End of user code
			// ----------------------------------------------------------
		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}
		// The following should only be changed
		// if not propagating message to the 'out' terminal
		out.propagate(outAssembly);

	}

}
