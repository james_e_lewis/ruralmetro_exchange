package com.rmetro.jaxb.model;

import javax.xml.bind.annotation.*;

@XmlRootElement(name = "reconciliation")
@XmlType(propOrder = { "uniqueTripID", "tdate", "job", "sourceDB", "runNumber", "recordStatus", "recordType", "sourceFlow" })
public class Reconciliation {

	private String uniqueTripID;
	private String tdate;
	private String job;
	private String sourceDB;
	private String runNumber;
	private String recordStatus;
	private String recordType;
	private String sourceFlow;
	
	public String getUniqueTripID() {
		return uniqueTripID;
	}
	
	@XmlElement(name = "UniqueTripID")
	public void setUniqueTripID(String uniqueTripID) {
		this.uniqueTripID = uniqueTripID;
	}
	
	public String getTdate() {
		return tdate;
	}
	
	@XmlElement(name = "Tdate")
	public void setTdate(String tdate) {
		this.tdate = tdate;
	}
	
	public String getJob() {
		return job;
	}
	
	@XmlElement(name = "Job")
	public void setJob(String job) {
		this.job = job;
	}
	
	public String getSourceDB() {
		return sourceDB;
	}
	
	@XmlElement(name = "SourceDB")
	public void setSourceDB(String sourceDB) {
		this.sourceDB = sourceDB;
	}
	
	public String getRunNumber() {
		return runNumber;
	}
	
	@XmlElement(name = "RunNumber")
	public void setRunNumber(String runNumber) {
		this.runNumber = runNumber;
	}
	
	public String getRecordStatus() {
		return recordStatus;
	}
	
	@XmlElement(name = "RecordStatus")
	public void setRecordStatus(String recordStatus) {
		this.recordStatus = recordStatus;
	}
	
	public String getRecordType() {
		return recordType;
	}
	
	@XmlElement(name = "RecordType")
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	
	public String getSourceFlow() {
		return sourceFlow;
	}
	
	@XmlElement(name = "SourceFlow")
	public void setSourceFlow(String sourceFlow) {
		this.sourceFlow = sourceFlow;
	}
}