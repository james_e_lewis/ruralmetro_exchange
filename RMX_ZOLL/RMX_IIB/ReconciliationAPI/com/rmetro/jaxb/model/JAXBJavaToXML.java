package com.rmetro.jaxb.model;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class JAXBJavaToXML {

	/*
	public static void main(String[] args) {
		
		// creating Reconciliation object
		Reconciliation rc = new Reconciliation();
		rc.setUniqueTripID("TUCS.2014-03-03.0005-A");
		rc.setTdate("2014-03-03");
		rc.setJob("0005-A");
		rc.setSourceDB("TUCS");
		rc.setRunNumber("14019589");
		rc.setRecordStatus("completed");
		rc.setRecordType("epcr");
		rc.setSourceFlow("ZOLL");
		
		try {
			//create JAXB context and initializing Marshaller
			JAXBContext jaxbContext = JAXBContext.newInstance(rc.getClass());
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			
			// for getting nice formatted output
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			
			// Writing to console
			jaxbMarshaller.marshal(rc, System.out);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}*/
	
	public static String reconcileJavaToXml(Reconciliation rc) {
		String xmlOutput = "";
		StringWriter sw = new StringWriter();
		
		try {
			//create JAXB context and initializing Marshaller
			JAXBContext jaxbContext = JAXBContext.newInstance(rc.getClass());
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			
			// for getting nice formatted output
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			
			// Write to stringwriter
			jaxbMarshaller.marshal(rc, sw);
				
			xmlOutput = sw.toString();
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		
		return xmlOutput;
	}
}
