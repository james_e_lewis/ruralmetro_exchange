package com.rmetro.validation;

import java.io.Serializable;

public class Nemsis2Validation implements Serializable {
	private static final long serialVersionUID = 1L;
	private String fieldName;
	private String fieldType;
	private boolean required;
	private boolean nullable;
	private boolean blankNullFlag;
	private int minLength;
	private int maxLength;
	private double minValue;
	private double maxValue;
	private String fieldValues;
	private String defaultValue;
	private String minLengthString;
	private String maxLengthString;
	private String minValueString;
	private String maxValueString;

	public Nemsis2Validation() {
		fieldName = "";
		fieldType = "";
		required = false;
		nullable = false;
		blankNullFlag = false;
		minLength = -999;
		maxLength = -999;
		minValue = -999;
		maxValue = -999;
		fieldValues = "";
		defaultValue = "";
		minLengthString = "";
		maxLengthString = "";
		minValueString = "";
		maxValueString = "";
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getFieldType() {
		return fieldType;
	}

	public boolean isRequired() {
		return required;
	}

	public boolean isNullable() {
		return nullable;
	}

	public boolean isBlankNullFlag() {
		return blankNullFlag;
	}

	public String getFieldValues() {
		return fieldValues;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public String getMinLengthString() {
		return minLengthString;
	}

	public String getMaxLengthString() {
		return maxLengthString;
	}

	public String getMinValueString() {
		return minValueString;
	}

	public String getMaxValueString() {
		return maxValueString;
	}

	public int getMinLength() {
		return minLength;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public double getMinValue() {
		return minValue;
	}

	public double getMaxValue() {
		return maxValue;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}

	public void setBlankNullFlag(boolean blankNullFlag) {
		this.blankNullFlag = blankNullFlag;
	}

	public void setFieldValues(String fieldValues) {
		if(fieldValues != null) {
			this.fieldValues = " " + fieldValues + " ";
		} else {
			this.fieldValues = fieldValues;
		}
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public void setMinLengthString(String minLengthString) {
		this.minLengthString = minLengthString;
		if(minLengthString != null) {
			try {
				this.minLength = Integer.valueOf(minLengthString);
			} catch (Exception e) {
				this.minLength = -999;
			}
		}
	}

	public void setMaxLengthString(String maxLengthString) {
		this.maxLengthString = maxLengthString;
		if(maxLengthString != null) {
			try {
				this.maxLength = Integer.valueOf(maxLengthString);
			} catch (Exception e) {
				this.maxLength = -999;
			}
		}
	}

	public void setMinValueString(String minValueString) {
		this.minValueString = minValueString;
		if(minValueString != null) {
			try {
				this.minValue = Double.valueOf(minValueString);
			} catch (Exception e) {
				this.minValue = -999;
			}
		}
	}

	public void setMaxValueString(String maxValueString) {
		this.maxValueString = maxValueString;
		if(maxValueString != null) {
			try {
				this.maxValue = Double.valueOf(maxValueString);
			} catch (Exception e) {
				this.maxValue = -999;
			}
		}
	}
}
