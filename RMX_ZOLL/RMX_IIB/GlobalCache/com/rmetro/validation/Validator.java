package com.rmetro.validation;

import java.util.Calendar;
import java.util.GregorianCalendar;

import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbGlobalMap;
import com.ibm.broker.plugin.MbUserException;
import com.rmetro.logging.OdsLogger;
import com.rmetro.logging.OdsLoggerHolder;

public class Validator {
	private static final String stringType = "xs:string";
	private static final String integerType = "xs:integer";
	private static final String decimalType = "xs:decimal";
	private static final String dateTimeType = "xs:dateTime";
	private static final String dateType = "xs:date";
	private static final String MESSAGETYPE = "Validation";
	private static final String MESSAGECODE = "1000";
	
	public static Object fieldLevelValidation(Object nemsis2Field, Object nemsis2Value, Object messageId, Object sourceQueue) throws MbException {
		Object returnObject = "";
		
		if(nemsis2Field != null) {
			String key = nemsis2Field.toString();
			Nemsis2Validation n2v;
			key = key.toLowerCase();
			
			try {
				MbGlobalMap nemsis22ValidationMap = MbGlobalMap.getGlobalMap("nemsis22ValidationMap");
				
				if(nemsis22ValidationMap.get(key) != null) {
					n2v = (Nemsis2Validation) nemsis22ValidationMap.get(key);
					String fieldType = n2v.getFieldType().trim();
					
					switch (fieldType) {
						case stringType: 
							returnObject = stringValidation(n2v, nemsis2Value, messageId, sourceQueue);
							break;
						case integerType:
							returnObject = integerValidation(n2v, nemsis2Value, messageId, sourceQueue);
							break;
						case decimalType:
							returnObject = decimalValidation(n2v, nemsis2Value, messageId, sourceQueue);
							break;
						case dateTimeType:
							returnObject = dateTimeValidation(n2v, nemsis2Value, messageId, sourceQueue);
							break;
						case dateType:
							returnObject = dateValidation(n2v, nemsis2Value, messageId, sourceQueue);
							break;
						default:
							returnObject = "";
							break;
					}
				}
			} catch (MbException e) {
				// Re-throw to allow Broker handling of MbException
				throw e;
			} catch (RuntimeException e) {
				// Re-throw to allow Broker handling of RuntimeException
				throw e;
			} catch (Exception e) {
				// Consider replacing Exception with type(s) thrown by user code
				// Example handling ensures all exceptions are re-thrown to be handled in the flow
				throw new MbUserException(com.rmetro.validation.Validator.class, "fieldLevelValidation()", "", "", e.toString(), null);
			}
		}
		
		return returnObject;
	}
	
	private static Object stringValidation(Nemsis2Validation n2v, Object inputToValidate, Object messageId, Object sourceQueue) {
		String output = null;
		StringBuilder sb = new StringBuilder();
		String itv = null;
		
		if(inputToValidate == null) {
			if(n2v.isRequired()) {
				sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
				output = n2v.getDefaultValue();
			}
		} else {
			itv = inputToValidate.toString().trim();
			output = inputToValidate.toString().trim();
			if(output.equals("")) {
				if(n2v.isRequired()) {
					sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
					output = n2v.getDefaultValue();
				}
			} else {
				//validate against min length
				if(n2v.getMinLengthString() != null) {
					if(output.length() < n2v.getMinLength()) {
						sb.append(" Min length of " + n2v.getMinLength() + ", field value right padded. ");
						output = rightPadString(output, n2v.getMinLength());
					}
				}
				
				//validate against max length
				if(n2v.getMaxLengthString() != null) {
					if(output.length() > n2v.getMaxLength()) {
						sb.append(" Max length of " + n2v.getMaxLength() + ", field value shortened. ");
						output = output.substring(0, n2v.getMaxLength() - 1);
					}
				}
				
				//validate against required values, if applicable
				if(n2v.getMaxLengthString() == null && n2v.getMinLengthString() == null) {
					if(n2v.getFieldValues() != null) {
						if(!n2v.getFieldValues().contains(" " + output + " ")) {
							if(n2v.isRequired()) {
								sb.append("Value not in required value list:  " + n2v.getFieldValues() + ", using default: " + n2v.getDefaultValue() + ".");
								output = n2v.getDefaultValue();
							}
						}
					}
				}
				
				if(sb.length() != 0) {
					sb.insert(0, "The NEMSIS 2.2 field " + n2v.getFieldName() + " with a value of " + inputToValidate.toString() + " fails the following constraints: ");
				}
			}
		}
		
		//log field validation errors
		if(sb.length() != 0) {
			OdsLogger logger = OdsLoggerHolder.getInstance();
			logger.writeMessageDetail(messageId, MESSAGETYPE, MESSAGECODE, sb.toString(), itv, output, n2v.getFieldName(), sourceQueue);
		}
		
		return output;
	}
	
	private static Object integerValidation(Nemsis2Validation n2v, Object inputToValidate, Object messageId, Object sourceQueue) {
		String output = null;
		StringBuilder sb = new StringBuilder();
		String itv = null;
		
		if(inputToValidate == null) {
			if(n2v.isRequired()) {
				sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
				output = n2v.getDefaultValue();
			}
		} else {
			try {
				itv = inputToValidate.toString().trim();
				output = inputToValidate.toString().trim();
				if(output.equals("")) {
					if(n2v.isRequired()) {
						sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
						output = n2v.getDefaultValue();
					}
				} else {
					//validate against min value
					if(n2v.getMinValue() != -999) {
						if(Integer.valueOf(output) < n2v.getMinValue()) {
							sb.append(" Min value of " + n2v.getMinValue());
						}
					}
					
					//validate against max value
					if(n2v.getMaxValue() != -999) {
						if(Integer.valueOf(output) > n2v.getMaxValue()) {
							sb.append(" Max value of " + n2v.getMaxValue());
						}
					}
					
					//validate against required values, if applicable
					if(n2v.getFieldValues() != null) {
						if(!n2v.getFieldValues().contains(" " + output + " ")) {
							sb.append("Value not in required value list:  " + n2v.getFieldValues() + ".");
						}
					}
					
					if(sb.length() != 0) {
						sb.insert(0, "The NEMSIS 2.2 field " + n2v.getFieldName() + " with a value of " + inputToValidate.toString() + " fails the following constraints: ");
						if(n2v.isRequired()) {
							sb.append(" Using default value: " + n2v.getDefaultValue());
							output = n2v.getDefaultValue();
						} else {
							output = null;
						}
					}
				}
			} catch (Exception e) {
				//log message, return default if applicable
				if(n2v.isRequired()) {
					sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
					output = n2v.getDefaultValue();
				} else {
					output = null;
				}
			}
		}
		
		//log field validation errors
		if(sb.length() != 0) {
			OdsLogger logger = OdsLoggerHolder.getInstance();
			logger.writeMessageDetail(messageId, MESSAGETYPE, MESSAGECODE, sb.toString(), itv, output, n2v.getFieldName(), sourceQueue);
		}
		
		return output;
	}
	
	private static Object decimalValidation(Nemsis2Validation n2v, Object inputToValidate, Object messageId, Object sourceQueue) {
		String output = null;
		StringBuilder sb = new StringBuilder();
		String itv = null;
		
		if(inputToValidate == null) {
			if(n2v.isRequired()) {
				sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
				output = n2v.getDefaultValue();
			}
		} else {
			try {
				itv = inputToValidate.toString().trim();
				output = inputToValidate.toString().trim();
				if(output.equals("")) {
					if(n2v.isRequired()) {
						sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
						output = n2v.getDefaultValue();
					}
				} else {
					//validate against min value
					if(n2v.getMinValue() != -999) {
						if(Double.valueOf(output) < n2v.getMinValue()) {
							sb.append(" Min value of " + n2v.getMinValue());
						}
					}
					
					//validate against max value
					if(n2v.getMaxValue() != -999) {
						if(Double.valueOf(output) > n2v.getMaxValue()) {
							sb.append(" Max value of " + n2v.getMaxValue());
						}
					}
					
					//validate against required values, if applicable
					if(n2v.getFieldValues() != null) {
						if(!n2v.getFieldValues().contains(" " + output + " ")) {
							sb.append("Value not in required value list:  " + n2v.getFieldValues() + ".");
						}
					}
					
					if(sb.length() != 0) {
						sb.insert(0, "The NEMSIS 2.2 field " + n2v.getFieldName() + " with a value of " + inputToValidate.toString() + " fails the following constraints: ");
						if(n2v.isRequired()) {
							sb.append(" Using default value: " + n2v.getDefaultValue());
							output = n2v.getDefaultValue();
						} else {
							output = null;
						}
					}
				}
			} catch (Exception e) {
				//log message, return default if applicable
				if(n2v.isRequired()) {
					sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
					output = n2v.getDefaultValue();
				} else {
					output = null;
				}
			}
		}
		
		//log field validation errors
		if(sb.length() != 0) {
			OdsLogger logger = OdsLoggerHolder.getInstance();
			logger.writeMessageDetail(messageId, MESSAGETYPE, MESSAGECODE, sb.toString(), itv, output, n2v.getFieldName(), sourceQueue);
		}
		
		return output;
	}
	
	private static Object dateTimeValidation(Nemsis2Validation n2v, Object inputToValidate, Object messageId, Object sourceQueue) {
		String output = null;
		StringBuilder sb = new StringBuilder();
		Calendar c;
		Calendar d = new GregorianCalendar(1990,0,1);
		String itv = null;
		
		if(inputToValidate == null) {
			if(n2v.isRequired()) {
				sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
				output = n2v.getDefaultValue();
			}
		} else {
			try {
				itv = inputToValidate.toString().trim();
				output = inputToValidate.toString().trim();
				c = javax.xml.bind.DatatypeConverter.parseDateTime(output);
				
				//validate against min value
				if(n2v.getMinValue() != -999) {
					d.set(Calendar.YEAR, Integer.valueOf(n2v.getMinValueString()));
					if(c.get(Calendar.YEAR) < d.get(Calendar.YEAR)) {
						sb.append(" Min year value of " + n2v.getMinValue());
					}
				}
				
				//validate against max value
				if(n2v.getMaxValue() != -999) {
					d.set(Calendar.YEAR, Integer.valueOf(n2v.getMaxValueString()));
					if(c.get(Calendar.YEAR) > d.get(Calendar.YEAR)) {
						sb.append(" Max year value of " + n2v.getMaxValue());
					}
				}
				
				if(sb.length() != 0) {
					if(n2v.isRequired()) {
						sb.append(" Using default value: " + n2v.getDefaultValue());
						output = n2v.getDefaultValue();
					} else {
						output = null;
					}
				}
			} catch (Exception e) {
				//log message, return default if applicable
				if(n2v.isRequired()) {
					sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
					output = n2v.getDefaultValue();
				} else {
					output = null;
				}
			}
		}
		
		//log field validation errors
		if(sb.length() != 0) {
			OdsLogger logger = OdsLoggerHolder.getInstance();
			logger.writeMessageDetail(messageId, MESSAGETYPE, MESSAGECODE, sb.toString(), itv, output, n2v.getFieldName(), sourceQueue);
		}
		
		return output;
	}
	
	private static Object dateValidation(Nemsis2Validation n2v, Object inputToValidate, Object messageId, Object sourceQueue) {
		String output = null;
		StringBuilder sb = new StringBuilder();
		Calendar c;
		Calendar d = new GregorianCalendar(1990,0,1);
		String itv = null;
		
		if(inputToValidate == null) {
			if(n2v.isRequired()) {
				sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
				output = n2v.getDefaultValue();
			}
		} else {
			try {
				itv = inputToValidate.toString().trim();
				output = inputToValidate.toString().trim();
				c = javax.xml.bind.DatatypeConverter.parseDateTime(output);
				
				//validate against min value
				if(n2v.getMinValue() != -999) {
					d.set(Calendar.YEAR, Integer.valueOf(n2v.getMinValueString()));
					if(c.get(Calendar.YEAR) < d.get(Calendar.YEAR)) {
						sb.append(" Min year value of " + n2v.getMinValue());
					}
				}
				
				//validate against max value
				if(n2v.getMaxValue() != -999) {
					d.set(Calendar.YEAR, Integer.valueOf(n2v.getMaxValueString()));
					if(c.get(Calendar.YEAR) > d.get(Calendar.YEAR)) {
						sb.append(" Max year value of " + n2v.getMaxValue());
					}
				}
				
				if(sb.length() != 0) {
					if(n2v.isRequired()) {
						sb.append(" Using default value: " + n2v.getDefaultValue());
						output = n2v.getDefaultValue();
					} else {
						output = null;
					}
				}
			} catch (Exception e) {
				//log message, return default if applicable
				if(n2v.isRequired()) {
					sb.append("Using default value " + n2v.getDefaultValue() + " for required field " + n2v.getFieldName());
					output = n2v.getDefaultValue();
				} else {
					output = null;
				}
			}
		}
		
		//log field validation errors
		if(sb.length() != 0) {
			OdsLogger logger = OdsLoggerHolder.getInstance();
			logger.writeMessageDetail(messageId, MESSAGETYPE, MESSAGECODE, sb.toString(), itv, output, n2v.getFieldName(), sourceQueue);
		}
		
		return output;
	}
	
	private static String rightPadString(String input, int minLength) {
		String output = null;
		
		if(input.length() < minLength) {
			output = String.format("%1$-" + minLength + "s", input);
		}
		
		return output;
	}
}
