package com.rmetro.cache;

import java.io.Serializable;

public class ZollToNemsisMap implements Serializable {
	private static final long serialVersionUID = 1L;
	private String nemsis22Value;
	private String nemsis33Value;
	
	public ZollToNemsisMap(String nemsis22Value, String nemsis33Value) {
		this.nemsis22Value = nemsis22Value;
		this.nemsis33Value = nemsis33Value;
	}

	public String getNemsis22Value() {
		return nemsis22Value;
	}

	public void setNemsis22Value(String nemsis22Value) {
		this.nemsis22Value = nemsis22Value;
	}

	public String getNemsis33Value() {
		return nemsis33Value;
	}

	public void setNemsis33Value(String nemsis33Value) {
		this.nemsis33Value = nemsis33Value;
	}

}
