package com.rmetro.cache;

import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbGlobalMap;
import com.ibm.broker.plugin.MbUserException;
import com.rmetro.logging.OdsLogger;
import com.rmetro.logging.OdsLoggerHolder;

public class Nemsis3To2Xref  {
	private static final String MESSAGETYPE = "Xref";
	private static final String MESSAGECODE = "1004";

	/**
	 * Method that can be called from a Mapping Custom Java transform.
	 * The content of this method provides the implementation for the Custom Java transform.
	 */
	public static java.lang.Object getNemsis2ValueFromNemsis3(java.lang.Object NEMSIS3_3Field, java.lang.Object NEMSIS3_3Value, Object messageId, Object sourceQueue, String fieldName, String sourceDB) throws MbException {
		String nemsis2Value = "";
		
		if(NEMSIS3_3Field != null && NEMSIS3_3Value != null && fieldName != null && sourceDB != null) {
			String key = NEMSIS3_3Field.toString() + NEMSIS3_3Value.toString() + fieldName.toString() + sourceDB.toString();
			key = key.toLowerCase();
			
			try {
				MbGlobalMap nemsis33To22Map = MbGlobalMap.getGlobalMap("nemsis33To22Map");
				
				if(nemsis33To22Map.get(key) != null) {
					nemsis2Value = String.valueOf(nemsis33To22Map.get(key));
				} else {
					OdsLogger logger = OdsLoggerHolder.getInstance();
					logger.writeMessageDetail(messageId, MESSAGETYPE, MESSAGECODE, "NEMSIS3 to NEMSIS2 Xref does not exist.", NEMSIS3_3Value.toString().trim(), nemsis2Value, fieldName, sourceQueue);
				}
			} catch (MbException e) {
				// Re-throw to allow Broker handling of MbException
				throw e;
			} catch (RuntimeException e) {
				// Re-throw to allow Broker handling of RuntimeException
				throw e;
			} catch (Exception e) {
				// Consider replacing Exception with type(s) thrown by user code
				// Example handling ensures all exceptions are re-thrown to be handled in the flow
				throw new MbUserException(com.rmetro.cache.Nemsis3To2Xref.class, "getNemsis2ValueFromNemsis3()", "", "", e.toString(), 	null);
			}
		}
		
		return nemsis2Value;
	}

}
