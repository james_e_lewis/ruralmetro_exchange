package com.rmetro.cache;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbGlobalMap;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;
import com.rmetro.util.BrokerProxyManager;
import com.rmetro.util.BrokerProxyManagerHolder;
import com.rmetro.validation.Nemsis2Validation;

public class XrefGlobalCacheInit extends MbJavaComputeNode {

	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal alt = getOutputTerminal("alternate");

		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		Nemsis2Validation n2v = new Nemsis2Validation();
		try {
			// create new message as a copy of the input
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);
			// ----------------------------------------------------------
			// Add user code below
			BrokerProxyManager bpm = BrokerProxyManagerHolder.getInstance();
			
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection conn = null;
			Statement statement = null;
			String queryString = null;
			ResultSet rs = null;
			String key = null;
			String jdbcProvider = bpm.getRmxUdcsProperties().getProperty("rmx_ods_jdbc_provider");
			
			MbGlobalMap zollToNemsisMap = MbGlobalMap.getGlobalMap("zollToNemsisMap");
			MbGlobalMap nemsis33To22Map = MbGlobalMap.getGlobalMap("nemsis33To22Map");
			MbGlobalMap nemsis22ValidationMap = MbGlobalMap.getGlobalMap("nemsis22ValidationMap");
			
			if(zollToNemsisMap.get("initKey") == null || nemsis33To22Map.get("initKey") == null || nemsis22ValidationMap.get("initKey") == null) {
				//conn = DriverManager.getConnection(url, userName, password);
				conn = getJDBCType4Connection(jdbcProvider, null);
			}
			
			if(zollToNemsisMap.get("initKey") == null) {
				zollToNemsisMap.put("initKey", "true");
				
				statement = conn.createStatement();
				queryString = "SELECT source_field, source_value, source_DB, NEMSIS_2_Value, NEMSIS_3_Value FROM dbo.RMX_XREF_ZollToNEMSIS";
				rs = statement.executeQuery(queryString);
				
				while (rs.next()) {
					key = rs.getString("source_field") + rs.getString("source_DB") + rs.getString("source_value");
					if(zollToNemsisMap.get(key.toLowerCase()) == null) {
						zollToNemsisMap.put(key.toLowerCase(), new ZollToNemsisMap(rs.getString("NEMSIS_2_Value"), rs.getString("NEMSIS_3_Value")));
					} else {
						zollToNemsisMap.update(key.toLowerCase(), new ZollToNemsisMap(rs.getString("NEMSIS_2_Value"), rs.getString("NEMSIS_3_Value")));
					}
				}
			}
			
			if(nemsis33To22Map.get("initKey") == null) {
				nemsis33To22Map.put("initKey", "true");
				
				statement = conn.createStatement();
				queryString = "SELECT NEMSIS_3_Element, NEMSIS_3_Value, NEMSIS_2_Element, NEMSIS_2_Value, SourceDB FROM dbo.RMX_XREF_NEMSIS3ToNEMSIS2";
				rs = statement.executeQuery(queryString);
				
				while (rs.next()) {
					key = rs.getString("NEMSIS_3_Element") + rs.getInt("NEMSIS_3_Value") + rs.getString("NEMSIS_2_Element") + rs.getString("SourceDB");
					if(nemsis33To22Map.get(key.toLowerCase()) == null) {
						nemsis33To22Map.put(key.toLowerCase(), rs.getInt("NEMSIS_2_Value"));
					} else {
						nemsis33To22Map.update(key.toLowerCase(), rs.getInt("NEMSIS_2_Value"));
					}
				}
			}
			
			if(nemsis22ValidationMap.get("initKey") == null) {
				nemsis22ValidationMap.put("initKey", "true");
				
				statement = conn.createStatement();
				queryString = "SELECT NEMSIS_2_Element, element_type, is_required, is_nullable, is_blank_nullable, min_length, max_length, min_value, " +
				              "max_value, field_values, default_value FROM dbo.RMX_NEMSIS_2_VALIDATION";
				rs = statement.executeQuery(queryString);
				
				while (rs.next()) {
					key = rs.getString("NEMSIS_2_Element");
					n2v = new Nemsis2Validation();
					
					n2v.setFieldName(rs.getString("NEMSIS_2_Element"));
					n2v.setFieldType(rs.getString("element_type"));
					n2v.setRequired(rs.getBoolean("is_required"));
					n2v.setNullable(rs.getBoolean("is_nullable"));
					n2v.setBlankNullFlag(rs.getBoolean("is_blank_nullable"));
					n2v.setMinLengthString(rs.getString("min_length"));
					n2v.setMaxLengthString(rs.getString("max_length"));
					n2v.setMinValueString(rs.getString("min_value"));
					n2v.setMaxValueString(rs.getString("max_value"));
					n2v.setFieldValues(rs.getString("field_values"));
					n2v.setDefaultValue(rs.getString("default_value"));
					
					if(nemsis22ValidationMap.get(key.toLowerCase()) == null) {
						nemsis22ValidationMap.put(key.toLowerCase(), n2v);
					} else {
						nemsis22ValidationMap.update(key.toLowerCase(), n2v);
					}
				}
			}

			// End of user code
			// ----------------------------------------------------------
		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}
		// The following should only be changed
		// if not propagating message to the 'out' terminal
		out.propagate(outAssembly);

	}

}
