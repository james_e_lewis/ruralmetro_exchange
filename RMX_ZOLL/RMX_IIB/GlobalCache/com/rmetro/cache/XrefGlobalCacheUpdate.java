package com.rmetro.cache;

import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbGlobalMap;
import com.ibm.broker.plugin.MbMessage;
import com.ibm.broker.plugin.MbMessageAssembly;
import com.ibm.broker.plugin.MbOutputTerminal;
import com.ibm.broker.plugin.MbUserException;
import com.rmetro.logging.OdsLogger;
import com.rmetro.logging.OdsLoggerHolder;
import com.rmetro.util.BrokerProxyManagerHolder;

public class XrefGlobalCacheUpdate extends MbJavaComputeNode {

	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal alt = getOutputTerminal("alternate");

		MbMessage inMessage = inAssembly.getMessage();
		MbMessageAssembly outAssembly = null;
		try {
			// create new message as a copy of the input
			MbMessage outMessage = new MbMessage(inMessage);
			outAssembly = new MbMessageAssembly(inAssembly, outMessage);
			// ----------------------------------------------------------
			// Add user code below
						
			MbGlobalMap zollToNemsisMap = MbGlobalMap.getGlobalMap("zollToNemsisMap");
			MbGlobalMap nemsis33To22Map = MbGlobalMap.getGlobalMap("nemsis33To22Map");
			MbGlobalMap nemsis22ValidationMap = MbGlobalMap.getGlobalMap("nemsis22ValidationMap");
			
			String initKey = "initKey";
			if(zollToNemsisMap.get(initKey) != null) {
				zollToNemsisMap.remove(initKey);
			}
			
			if(nemsis33To22Map.get(initKey) != null) {
				nemsis33To22Map.remove(initKey);
			}
			
			if(nemsis22ValidationMap.get(initKey) != null) {
				nemsis22ValidationMap.remove(initKey);
			}

			// End of user code
			// ----------------------------------------------------------
		} catch (MbException e) {
			// Re-throw to allow Broker handling of MbException
			throw e;
		} catch (RuntimeException e) {
			// Re-throw to allow Broker handling of RuntimeException
			throw e;
		} catch (Exception e) {
			// Consider replacing Exception with type(s) thrown by user code
			// Example handling ensures all exceptions are re-thrown to be handled in the flow
			throw new MbUserException(this, "evaluate()", "", "", e.toString(),
					null);
		}
		// The following should only be changed
		// if not propagating message to the 'out' terminal
		out.propagate(outAssembly);

	}

}
