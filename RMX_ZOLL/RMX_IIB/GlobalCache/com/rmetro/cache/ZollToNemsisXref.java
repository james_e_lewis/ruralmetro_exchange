package com.rmetro.cache;

import com.ibm.broker.plugin.MbException;
import com.ibm.broker.plugin.MbGlobalMap;
import com.ibm.broker.plugin.MbUserException;
import com.rmetro.logging.OdsLogger;
import com.rmetro.logging.OdsLoggerHolder;

public class ZollToNemsisXref  {
	private static final String MESSAGETYPE = "Xref";
	private static final String MESSAGECODE = "1005";

	/**
	 * Method that can be called from a Mapping Custom Java transform.
	 * The content of this method provides the implementation for the Custom Java transform.
	 */
	public static java.lang.Object getNemsisValueFromZoll(Object sourceField, Object sourceValue, Object sourceDb, long nemsis, Object messageId, Object sourceQueue, String fieldName) throws MbException {
		String nemsisValue = "";
		
		if(sourceField != null && sourceValue != null && sourceDb != null) {
			ZollToNemsisMap nm = new ZollToNemsisMap("", "");
			String key = sourceField.toString() + sourceDb.toString() + sourceValue.toString();
			key = key.toLowerCase();
			
			//default is to return nemsis 2 value
			if(nemsis < 2 || nemsis > 3) {
				nemsis = 2;
			}
			
			try {
				MbGlobalMap zollToNemsisMap = MbGlobalMap.getGlobalMap("zollToNemsisMap");
				
				if(zollToNemsisMap.get(key) != null) {
					nm = (ZollToNemsisMap) zollToNemsisMap.get(key);
					
					if(nemsis == 3) {
						nemsisValue = nm.getNemsis33Value();
					} else {
						nemsisValue = nm.getNemsis22Value();
					}
				} else {
					OdsLogger logger = OdsLoggerHolder.getInstance();
					logger.writeMessageDetail(messageId, MESSAGETYPE, MESSAGECODE, "Zoll to NEMSIS" + nemsis + " Xref does not exist.", sourceValue.toString(), nemsisValue, fieldName, sourceQueue);
				}
			} catch (MbException e) {
				// Re-throw to allow Broker handling of MbException
				throw e;
			} catch (RuntimeException e) {
				// Re-throw to allow Broker handling of RuntimeException
				throw e;
			} catch (Exception e) {
				// Consider replacing Exception with type(s) thrown by user code
				// Example handling ensures all exceptions are re-thrown to be handled in the flow
				throw new MbUserException(com.rmetro.cache.ZollToNemsisXref.class, "getNemsisValueFromZoll()", "", "", e.toString(), 	null);
			}
		} else {
			OdsLogger logger = OdsLoggerHolder.getInstance();
			String sv = null;
			
			if(sourceValue != null) {
				sv = sourceValue.toString().trim();
			}
			
			logger.writeMessageDetail(messageId, MESSAGETYPE, MESSAGECODE, "Unable to look up Zoll to NEMSIS" + nemsis + " Xref.", sv, nemsisValue, fieldName, sourceQueue);
		}
		
		return nemsisValue;
	}

}
