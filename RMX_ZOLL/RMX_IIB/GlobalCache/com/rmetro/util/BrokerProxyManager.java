package com.rmetro.util;

import java.util.Properties;

import com.ibm.broker.config.proxy.BrokerProxy;
import com.ibm.broker.config.proxy.ConfigurableService;

public class BrokerProxyManager {
	private BrokerProxy bp;
	private ConfigurableService rmxUdcs;
	private Properties rmxUdcsProperties;
	private static final String SERVICETYPE = "UserDefined";
	private static final String UDCSNAME = "rmx_udcs";
	
	public BrokerProxyManager() {
		initialize();
	}
	
	private void initialize() {
		try {
			this.bp = BrokerProxy.getLocalInstance();
			while(!bp.hasBeenPopulatedByBroker()) { Thread.sleep(100); }
			this.rmxUdcs = this.bp.getConfigurableService(SERVICETYPE, UDCSNAME);
			this.rmxUdcsProperties = this.rmxUdcs.getProperties();
		} catch (Exception e) {
			// Re-throw to allow Broker handling of MbException
			e.printStackTrace();
		}
	}

	public BrokerProxy getBp() {
		return bp;
	}

	public ConfigurableService getRmxUdcs() {
		return rmxUdcs;
	}

	public Properties getRmxUdcsProperties() {
		return rmxUdcsProperties;
	}
}
