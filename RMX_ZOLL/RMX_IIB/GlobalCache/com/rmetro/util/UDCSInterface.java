package com.rmetro.util;

public class UDCSInterface {
	
	public static String getProperty(String p) {
		BrokerProxyManager bpm = BrokerProxyManagerHolder.getInstance();
		return bpm.getRmxUdcsProperties().getProperty(p);
	}
}
