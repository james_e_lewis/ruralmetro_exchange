package com.rmetro.util;

public class BrokerProxyManagerHolder {
	private static class Holder {
		static BrokerProxyManager INSTANCE = new BrokerProxyManager();
	}
	
	private BrokerProxyManagerHolder() {
		
	}
	
	public static BrokerProxyManager getInstance() {
		return Holder.INSTANCE;
	}
}
