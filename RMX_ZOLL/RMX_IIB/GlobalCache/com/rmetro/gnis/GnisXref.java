package com.rmetro.gnis;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.rmetro.logging.OdsLogger;
import com.rmetro.logging.OdsLoggerHolder;
import com.rmetro.util.BrokerProxyManager;
import com.rmetro.util.BrokerProxyManagerHolder;

public class GnisXref {
	private static final String MESSAGETYPE = "Xref";
	private static final String MESSAGECODEGNIS = "1001";
	private static final String MESSAGECODEANSI = "1002";
	private static final String MESSAGECODEFIPS = "1003";
	public static java.lang.Object getGnisCityCode(String city, String state, Object messageId, String fieldName, Object sourceQueue) {
		String gnisCityCode = "";
		StringBuilder sb = new StringBuilder();
		
		if(city != null && state != null) {
			try {
				BrokerProxyManager bpm = BrokerProxyManagerHolder.getInstance();
				
				String userName = bpm.getRmxUdcsProperties().getProperty("userName");
				String password = bpm.getRmxUdcsProperties().getProperty("password");
				String url = bpm.getRmxUdcsProperties().getProperty("ods_url");
				
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				Connection conn = null;
				PreparedStatement statement = null;
				String queryString = null;
				ResultSet rs = null;
				
				conn = DriverManager.getConnection(url, userName, password);
				
				queryString = "SELECT TOP 1 FEATURE_ID AS [GNISCityCode], CENSUS_PREFIX AS [ANSIStateCode], CENSUS_CODE, FEATURE_CLASS " +
							  "FROM RMX_XREF_GNISCityCodes " +
							  "WHERE FEATURE_NAME = ? AND STATE_ALPHA = ? AND FEATURE_CLASS IN (?, ?) " +
							  "ORDER BY FEATURE_CLASS ASC";
				statement = conn.prepareStatement(queryString);
				statement.setString(1, city);
				statement.setString(2, state);
				statement.setString(3, "Civil");
				statement.setString(4, "Populated Place");
				rs = statement.executeQuery();
				
				while (rs.next()) {
					gnisCityCode = rs.getString("GNISCityCode");
				}
				
				if(gnisCityCode.equals("")) {
					sb.append("Unable to look up GNIS city code.");
				}
				
				if(conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				sb.append(e.getMessage().trim());
				e.printStackTrace();
			}
		}
		
		if(sb.length() != 0) {
			OdsLogger logger = OdsLoggerHolder.getInstance();
			logger.writeMessageDetail(messageId, MESSAGETYPE, MESSAGECODEGNIS, sb.toString(), city, gnisCityCode, fieldName, sourceQueue);
		}
		
		return gnisCityCode;
	}
	
	public static java.lang.Object getFipsCityCode(String gnisFeatureId, Object messageId, String fieldName, Object sourceQueue) {
		String fipsCityCode = "";
		StringBuilder sb = new StringBuilder();
		
		if(gnisFeatureId != null) {
			try {
				BrokerProxyManager bpm = BrokerProxyManagerHolder.getInstance();
				
				String userName = bpm.getRmxUdcsProperties().getProperty("userName");
				String password = bpm.getRmxUdcsProperties().getProperty("password");
				String url = bpm.getRmxUdcsProperties().getProperty("ods_url");
				
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				Connection conn = null;
				PreparedStatement statement = null;
				String queryString = null;
				ResultSet rs = null;
				
				conn = DriverManager.getConnection(url, userName, password);
				
				queryString = "SELECT TOP 1 CENSUS_CODE AS [FIPSCode] " +
							  "FROM RMX_XREF_GNISCityCodes " +
							  "WHERE FEATURE_ID = ?";
				statement = conn.prepareStatement(queryString);
				statement.setString(1, gnisFeatureId);
				rs = statement.executeQuery();
				
				while (rs.next()) {
					fipsCityCode = rs.getString("FIPSCode");
				}
				
				if(fipsCityCode.equals("")) {
					sb.append("Unable to look up FIPS city code.");
				}
				
				if(conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				sb.append(e.getMessage().trim());
				e.printStackTrace();
			}
		}
		
		if(sb.length() != 0) {
			OdsLogger logger = OdsLoggerHolder.getInstance();
			logger.writeMessageDetail(messageId, MESSAGETYPE, MESSAGECODEFIPS, sb.toString(), gnisFeatureId, fipsCityCode, fieldName, sourceQueue);
		}
		
		return fipsCityCode;
	}
	
	public static java.lang.Object getAnsiStateCode(String state, Object messageId, String fieldName, Object sourceQueue) {
		String ansiStateCode = "";
		StringBuilder sb = new StringBuilder();
		
		if(state != null) {
			try {
				BrokerProxyManager bpm = BrokerProxyManagerHolder.getInstance();
				
				String userName = bpm.getRmxUdcsProperties().getProperty("userName");
				String password = bpm.getRmxUdcsProperties().getProperty("password");
				String url = bpm.getRmxUdcsProperties().getProperty("ods_url");
				
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				Connection conn = null;
				PreparedStatement statement = null;
				String queryString = null;
				ResultSet rs = null;
				
				conn = DriverManager.getConnection(url, userName, password);
				
				queryString = "SELECT TOP 1 CENSUS_PREFIX AS [ANSIStateCode] " +
							  "FROM RMX_XREF_GNISCityCodes " +
							  "WHERE STATE_ALPHA = ?";
				statement = conn.prepareStatement(queryString);
				statement.setString(1, state);
				rs = statement.executeQuery();
				
				while (rs.next()) {
					ansiStateCode = rs.getString("ANSIStateCode");
				}
				
				if(ansiStateCode.equals("")) {
					sb.append("Unable to look up ANSI state code.");
				}
				
				if(conn != null) {
					conn.close();
				}
			} catch (Exception e) {
				sb.append(e.getMessage().trim());
				e.printStackTrace();
			}
		}
		
		if(sb.length() != 0) {
			OdsLogger logger = OdsLoggerHolder.getInstance();
			logger.writeMessageDetail(messageId, MESSAGETYPE, MESSAGECODEANSI, sb.toString(), state, ansiStateCode, fieldName, sourceQueue);
		}
		
		return ansiStateCode;
	}
}
