package com.rmetro.logging;

public class OdsLoggerHolder {
	private static class Holder {
		static OdsLogger INSTANCE = new OdsLogger();
	}
	
	private OdsLoggerHolder() {
		//Exists only to defeat instantiation
	}
	
	public static OdsLogger getInstance() {
		return Holder.INSTANCE;
	}
	
	public static void refreshInstance() {
		Holder.INSTANCE.refreshOdsLogger();
	}
}
