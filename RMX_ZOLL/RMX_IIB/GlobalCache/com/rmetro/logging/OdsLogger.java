package com.rmetro.logging;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import com.rmetro.util.BrokerProxyManager;
import com.rmetro.util.BrokerProxyManagerHolder;

public class OdsLogger {
	private String userName;
	private String password;
	private String url;
	private boolean detail;
	private Connection conn;
	private PreparedStatement statement;
	private String queryString;
	
	public OdsLogger() {
		
	}
	
	public void refreshOdsLogger() {
		initialize();
	}
	
	private void initialize() {
		BrokerProxyManager bpm = BrokerProxyManagerHolder.getInstance();
		
		this.userName = bpm.getRmxUdcsProperties().getProperty("userName");
		this.password = bpm.getRmxUdcsProperties().getProperty("password");
		this.url = bpm.getRmxUdcsProperties().getProperty("ods_url");
		this.detail = Boolean.valueOf(bpm.getRmxUdcsProperties().getProperty("logDetailToggle"));
	}
	
	public void writeMessageDetail(Object messageId, String messageType, String messageCode, String messageDescription, String sourceValue, String mappedValue, String fieldName, Object sourceQueue) {
		try {
			byte[] blobBytes;
			Blob msgId;
			String sq = null;
			
			if(this.detail) {
				if(messageId != null) {
					blobBytes = (byte[]) messageId;
					msgId = new javax.sql.rowset.serial.SerialBlob(blobBytes);
				} else {
					msgId = null;
				}
				
				if(sourceQueue != null) {
					sq = sourceQueue.toString().trim();
				}
				
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
				
				this.conn = DriverManager.getConnection(this.url, this.userName, this.password);
				
				this.queryString = "INSERT INTO Message_Log_Detail " +
						      "(MessageId, MessageType, MessageCode, MessageDescription, SourceValue, MappedValue, FieldName, SourceQueue) " +
						      "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
				this.statement = conn.prepareStatement(queryString);
				this.statement.setBlob(1, msgId);
				this.statement.setString(2, messageType);
				this.statement.setString(3, messageCode);
				this.statement.setString(4, messageDescription);
				this.statement.setString(5, sourceValue);
				this.statement.setString(6, mappedValue);
				this.statement.setString(7, fieldName);
				this.statement.setString(8, sq);
				this.statement.executeUpdate();
				
				if(this.conn != null) {
					this.conn.close();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
