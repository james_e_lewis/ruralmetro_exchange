package com.rmetro.formatting;

public class SocialSecurityNumberPad {

	/**
	 * Sample method that can be called from a Mapping Custom Java transform.
	 * The content of this method provides the implementation for the Custom Java transform.
	 */
	public static java.lang.Object ssnFix(java.lang.Object ssnInput) {
		String ssnOutput = "";
		int ssn = 0;
		
		if(ssnInput != null) {
			if(!ssnInput.toString().isEmpty() & ssnInput.toString().length() > 0) {
				try {
					ssn = Integer.valueOf(ssnInput.toString());
					String format = "%09d";
					ssnOutput = String.format(format, ssn);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
		return ssnOutput;
	}
}
