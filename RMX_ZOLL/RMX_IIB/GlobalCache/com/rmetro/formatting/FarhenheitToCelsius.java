package com.rmetro.formatting;

public class FarhenheitToCelsius {

	/**
	 * Sample method that can be called from a Mapping Custom Java transform.
	 * The content of this method provides the implementation for the Custom Java transform.
	 */
	public static java.lang.Object convertFarhenheitToCelsius(String decimalInput) {
		double temperatureCelsius = 0.0;
		double temperatureFarhenheit = (double) DecimalFix.fixDecimal(decimalInput);
		
		temperatureCelsius = (temperatureFarhenheit - 32) * 5.0 / 9.0;
		
		temperatureCelsius = (double) Math.round(temperatureCelsius * 10) / 10;
		
		return temperatureCelsius;
	}
}
