package com.rmetro.formatting;

public class DecimalFix {

	/**
	 * Sample method that can be called from a Mapping Custom Java transform.
	 * The content of this method provides the implementation for the Custom Java transform.
	 */
	public static java.lang.Object fixDecimal(String decimalInput) {
		double decimalReading = 0.0;
		
		if(decimalInput != null) {
			if(isDouble(decimalInput)) {
				String x = "";
				if(decimalInput.length() >= 2) {
					x = decimalInput.substring(0, decimalInput.length() - 1) + "." + decimalInput.substring(decimalInput.length() - 1, decimalInput.length());
					
					decimalReading = Double.valueOf(x);
				} else if(decimalInput.length() == 1) {
					x = "0." + decimalInput;
					decimalReading = Double.valueOf(x);
				}
			}
		}
		
		return decimalReading;
	}
	
	private static boolean isDouble(String value)
	{        
	    boolean seenDot = false;
	    boolean seenExp = false;
	    boolean justSeenExp = false;
	    boolean seenDigit = false;
	    for (int i=0; i < value.length(); i++)
	    {
	        char c = value.charAt(i);
	        if (c >= '0' && c <= '9')
	        {
	            seenDigit = true;
	            continue;
	        }
	        if ((c == '-' || c=='+') && (i == 0 || justSeenExp))
	        {
	            continue;
	        }
	        if (c == '.' && !seenDot)
	        {
	            seenDot = true;
	            continue;
	        }
	        justSeenExp = false;
	        if ((c == 'e' || c == 'E') && !seenExp)
	        {
	            seenExp = true;
	            justSeenExp = true;
	            continue;
	        }
	        return false;
	    }
	    if (!seenDigit)
	    {
	        return false;
	    }
	    try
	    {
	        Double.parseDouble(value);
	        return true;
	    }
	    catch (NumberFormatException e)
	    {
	        return false;
	    }
	}
}
