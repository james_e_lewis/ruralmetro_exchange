package com.ibm.iib.errorhandler;

import java.util.List;
import com.ibm.mq.constants.MQConstants; // com.ibm.mq.jmqi.jar
import com.ibm.broker.javacompute.MbJavaComputeNode;
import com.ibm.broker.plugin.*;

public class ErrorToQueue_SetErrorMsg extends MbJavaComputeNode {

	public void evaluate(MbMessageAssembly inAssembly) throws MbException {
		MbOutputTerminal out = getOutputTerminal("out");
		MbOutputTerminal alt = getOutputTerminal("alternate");

		MbMessage inMessage = inAssembly.getMessage();

		// create new message
		MbMessage outMessage = new MbMessage(inMessage);
		MbMessageAssembly outAssembly = new MbMessageAssembly(inAssembly,
				outMessage);

		try {
			// ----------------------------------------------------------
			// Add user code below
			MbElement outRoot = outMessage.getRootElement() ;
			MbElement mqmd = outRoot.getFirstElementByPath("MQMD") ;
			MbElement rfh2c = outRoot.getFirstElementByPath("MQRFH2C") ;
		if (mqmd == null || (Integer)inMessage.getRootElement().getFirstElementByPath("MQMD/BackoutCount").getValue() == 0) {	
			//create MQMD and/or MQRFH2 if needed
			if (mqmd == null)
				mqmd = outRoot.getFirstElementByPath("Properties").createElementAfter(MbMQMD.PARSER_NAME);
			if (rfh2c == null)
				rfh2c = outRoot.getLastChild().createElementBefore(MbRFH2C.PARSER_NAME);
			
			
			//put the exception list in MQRFH2C
			MbXPath rfh2cXp = new MbXPath("/?usr/?ExceptionList",rfh2c) ;		
			MbElement exListInRfh2 = (MbElement)((List)outMessage.evaluateXPath(rfh2cXp)).get(0);
			exListInRfh2.copyElementTree(inAssembly.getExceptionList().getRootElement());
			
			
			//set MQMD format for rfh2
			String rfh2Format = MQConstants.MQFMT_RF_HEADER_2 ; // "MQHRF2  "
			MbElement format = mqmd.getFirstElementByPath("Format");
			if(format == null)
				mqmd.createElementAsLastChild(MbElement.TYPE_NAME_VALUE, "Format", rfh2Format);
			else
				format.setValue(rfh2Format) ;
			
	

			// End of user code
			// ----------------------------------------------------------

			// The following should only be changed
			// if not propagating message to the 'out' terminal
			out.propagate(outAssembly);
		} else
			alt.propagate(outAssembly) ;
		
		} finally {
			// clear the outMessage
			outMessage.clearMessage();
		}
	}

}
