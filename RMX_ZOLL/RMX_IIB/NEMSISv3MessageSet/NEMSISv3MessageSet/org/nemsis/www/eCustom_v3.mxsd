<?xml version="1.0" encoding="UTF-8"?><xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" targetNamespace="http://www.nemsis.org" xmlns="http://www.nemsis.org" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:include schemaLocation="commonTypes_v3.mxsd"/>
	<xs:complexType id="eCustomConfiguration.eCustomHeaderInformation" name="eCustomConfiguration">
		<xs:sequence>
			<xs:element id="eCustomConfiguration.eCustomGroup" maxOccurs="unbounded" minOccurs="0" name="eCustomConfiguration.CustomGroup">
				<xs:annotation>
					<xs:documentation>Group Tag to hold custom information</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element id="eCustomConfiguration.eCustomDataElementTitle" minOccurs="1" name="eCustomConfiguration.01">
							<xs:annotation>
								<xs:documentation>
									<nemsisTacDoc>
										<number>eCustomConfiguration.01</number>
										<name>Custom Data Element Title</name>
										<national>No</national>
										<state>No</state>
										<definition>This is the title of the custom data element created to collect information that is not defined formally in NEMSIS Version 3.</definition>
										
										<usage>Mandatory</usage>
										
										<comment>This is grouped with all data elements in this section and can have multiple instances.</comment>
										<v3Changes>Added to allow customized data elements to be inserted and collected from within the NEMSIS Version 3 standard.</v3Changes>
									</nemsisTacDoc>
								</xs:documentation>
							</xs:annotation>
							<xs:complexType>
								<xs:simpleContent>
									<xs:extension base="CustomTitle">
										<xs:attribute name="nemsisElement" type="xs:string" use="optional"/>
									</xs:extension>
								</xs:simpleContent>
							</xs:complexType>
						</xs:element>
						<xs:element id="eCustomConfiguration.eCustomDefinition" minOccurs="1" name="eCustomConfiguration.02" type="CustomDefinition">
							<xs:annotation>
								<xs:documentation>
									<nemsisTacDoc>
										<number>eCustomConfiguration.02</number>
										<name>Custom Definition</name>
										<national>No</national>
										<state>No</state>
										<definition>The definition of the custom element and how it should be used.</definition>
										
										<usage>Mandatory</usage>
										
										
										
									</nemsisTacDoc>
								</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element id="eCustomConfiguration.eCustomDataType" minOccurs="1" name="eCustomConfiguration.03" type="CustomDataType">
							<xs:annotation>
								<xs:documentation>
									<nemsisTacDoc>
										<number>eCustomConfiguration.03</number>
										<name>Custom Data Type</name>
										<national>No</national>
										<state>No</state>
										<definition>The data type of the custom element.</definition>
										
										<usage>Mandatory</usage>
										
										
										
									</nemsisTacDoc>
								</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element id="eCustomConfiguration.eCustomDataElementRecurrence" minOccurs="1" name="eCustomConfiguration.04" type="YesNoValues">
							<xs:annotation>
								<xs:documentation>
									<nemsisTacDoc>
										<number>eCustomConfiguration.04</number>
										<name>Custom Data Element Recurrence</name>
										<national>No</national>
										<state>No</state>
										<definition>Indication if the data element will accept multiple values.</definition>
										
										<usage>Mandatory</usage>
										
										
										<v3Changes>Added to allow customized data elements to be inserted and collected from within the NEMSIS Version 3 standard.</v3Changes>
									</nemsisTacDoc>
								</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element id="eCustomConfiguration.eCustomDataElementUsage" minOccurs="1" name="eCustomConfiguration.05" type="ElementUsage">
							<xs:annotation>
								<xs:documentation>
									<nemsisTacDoc>
										<number>eCustomConfiguration.05</number>
										<name>Custom Data Element Usage</name>
										<national>No</national>
										<state>No</state>
										<definition>The Usage (Mandatory, Required, Recommended or Optional) for the Custom Data Element.</definition>
										
										<usage>Mandatory</usage>
										
										<comment>Mandatory = Must be completed and will not accept null values
                    &lt;br/&gt;Required = Must be completed but will accept null values
                    &lt;br/&gt;Recommended = Not required but if collected will accept null values
                    &lt;br/&gt;Optional = Not required but if collected, it cannot be a null value.</comment>
										<v3Changes>Added to allow customized data elements to be inserted and collected from within the NEMSIS Version 3 standard.</v3Changes>
									</nemsisTacDoc>
								</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element id="eCustomConfiguration.eCustomDataElementPotentialValues" maxOccurs="unbounded" minOccurs="0" name="eCustomConfiguration.06">
							<xs:annotation>
								<xs:documentation>
									<nemsisTacDoc>
										<number>eCustomConfiguration.06</number>
										<name>Custom Data Element Potential Values</name>
										<national>No</national>
										<state>No</state>
										<definition>The values which are associated with the Custom Data Element. Values would be the choices provided to the user when they document the Custom Data Element</definition>
										
										<usage>Optional</usage>
										
										
										<v3Changes>Added to allow customized data elements to be inserted and collected from within the NEMSIS Version 3 standard.</v3Changes>
									</nemsisTacDoc>
								</xs:documentation>
							</xs:annotation>
							<xs:complexType>
								<xs:simpleContent>
									<xs:extension base="CustomValue">
										<xs:attribute name="nemsisCode" type="xs:string" use="optional"/>
										<xs:attribute name="customValueDescription" type="xs:string" use="optional"/>
									</xs:extension>
								</xs:simpleContent>
							</xs:complexType>
						</xs:element>
						<xs:element id="eCustomConfiguration.eCustomAcceptableNV" maxOccurs="unbounded" minOccurs="0" name="eCustomConfiguration.07" type="NV">
							<xs:annotation>
								<xs:documentation>
									<nemsisTacDoc>
										<number>eCustomConfiguration.07</number>
										<name>Custom Data Element Potential NOT Values (NV)</name>
										<national>No</national>
										<state>No</state>
										<definition>NOT Values (NV) associated with the custom element</definition>
										
										<usage>Optional</usage>
										
										
										
									</nemsisTacDoc>
								</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element id="eCustomConfiguration.eCustomAcceptablePN" maxOccurs="unbounded" minOccurs="0" name="eCustomConfiguration.08" type="PN">
							<xs:annotation>
								<xs:documentation>
									<nemsisTacDoc>
										<number>eCustomConfiguration.08</number>
										<name>Custom Data Element Potential Pertinent Negative Values (PN)</name>
										<national>No</national>
										<state>No</state>
										<definition>Pertinent Negative Values (PN) associated with the custom element</definition>
										
										<usage>Optional</usage>
										
										
										
									</nemsisTacDoc>
								</xs:documentation>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
					<xs:attribute name="CustomElementID" type="CorrelationID" use="required"/>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:complexType id="eCustomConfiguration.eCustomResults" name="eCustomResults">
		<xs:sequence>
			<xs:element id="eCustomConfiguration.eResultsGroup" maxOccurs="unbounded" minOccurs="0" name="eCustomResults.ResultsGroup">
				<xs:annotation>
					<xs:documentation>Group Tag to hold custom results</xs:documentation>
				</xs:annotation>
				<xs:complexType>
					<xs:sequence>
						<xs:element id="eCustomConfiguration.eCustomDataElementResult" maxOccurs="unbounded" minOccurs="1" name="eCustomResults.01" nillable="true">
							<xs:annotation>
								<xs:documentation>
									<nemsisTacDoc>
										<number>eCustomResults.01</number>
										<name>Custom Data Element Result</name>
										<national>No</national>
										<state>No</state>
										<definition>The actual value or values chosen (if values listed in dCustomConfiguration.06) or result (free text, Date/Time, or number) documented for the Custom Data Element</definition>
										
										<usage>Mandatory</usage>
										
										
										<v3Changes>Added to allow customized data elements to be inserted and collected from within the NEMSIS Version 3 standard.</v3Changes>
									</nemsisTacDoc>
								</xs:documentation>
							</xs:annotation>
							<xs:complexType>
								<xs:simpleContent>
									<xs:extension base="CustomResults">
										<xs:attribute name="NV" type="NV" use="optional"/>
										<xs:attribute name="PN" type="PN" use="optional"/>
									</xs:extension>
								</xs:simpleContent>
							</xs:complexType>
						</xs:element>
						<xs:element id="eCustomConfiguration.eCustomDataElementReference" minOccurs="1" name="eCustomResults.02" type="CorrelationID">
							<xs:annotation>
								<xs:documentation>
									<nemsisTacDoc>
										<number>eCustomResults.02</number>
										<name>Custom Element ID Referenced</name>
										<national>No</national>
										<state>No</state>
										<definition>References the CustomElementID attribute for eCustomConfiguration.CustomGroup</definition>
										
										<usage>Mandatory</usage>
										
										
										
									</nemsisTacDoc>
								</xs:documentation>
							</xs:annotation>
						</xs:element>
						<xs:element id="eCustomConfiguration.eCustomCorrelationID" minOccurs="0" name="eCustomResults.03" type="CorrelationID">
							<xs:annotation>
								<xs:documentation>
									<nemsisTacDoc>
										<number>eCustomResults.03</number>
										<name>CorrelationID of PatientCareReport Element or Group</name>
										<national>No</national>
										<state>No</state>
										<definition>References the CorrelationID attribute of an element or group in the PatientCareReport section</definition>
										
										<usage>Optional</usage>
										
										
										
									</nemsisTacDoc>
								</xs:documentation>
							</xs:annotation>
						</xs:element>
					</xs:sequence>
					<xs:attribute name="CorrelationID" type="CorrelationID" use="optional"/>
				</xs:complexType>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
    <xs:element name="eCustomConfiguration" type="eCustomConfiguration">
    <xs:annotation>
      <xs:appinfo source="WMQI_APPINFO">
        <MRMessage messageDefinition="/0/eCustomConfiguration;XSDElementDeclaration$MRObject"/>
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
    <xs:element name="eCustomResults" type="eCustomResults">
    <xs:annotation>
      <xs:appinfo source="WMQI_APPINFO">
        <MRMessage messageDefinition="/0/eCustomResults;XSDElementDeclaration=1$MRObject"/>
      </xs:appinfo>
    </xs:annotation>
  </xs:element>
</xs:schema>