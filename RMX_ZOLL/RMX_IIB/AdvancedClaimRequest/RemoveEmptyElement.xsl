<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:io="http://www.nemsis.org" xmlns:out="http://www.nemsis.org">
 
  <xsl:output method="xml" version="1.0" encoding="UTF-8" omit-xml-declaration="no" indent="yes" />
  <xsl:strip-space  elements="*"/>
 
  <xsl:template match="/">
       <xsl:apply-templates />
  </xsl:template>
 
  <xsl:template match="*">
       <xsl:if test="string-length(.//text())>0">
            <xsl:element name="{name(.)}" >
                 <xsl:apply-templates />
            </xsl:element>
       </xsl:if>
  </xsl:template>
 
</xsl:stylesheet>