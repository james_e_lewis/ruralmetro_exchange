USE [msdb]
GO
/****** Object:  Job [RMx -- Daily Sent to AC Stats]    Script Date: 10/13/2015 1:47:36 PM ******/
BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]]    Script Date: 10/13/2015 1:47:37 PM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'RMx -- Daily Sent to AC Stats', 
        @enabled=1, 
        @notify_level_eventlog=0, 
        @notify_level_email=0, 
        @notify_level_netsend=0, 
        @notify_level_page=0, 
        @delete_level=0, 
        @description=N'No description available.', 
        @category_name=N'[Uncategorized (Local)]', 
        @owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [RMx Sent To AC stats]    Script Date: 10/13/2015 1:47:38 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'RMx Sent To AC stats', 
        @step_id=1, 
        @cmdexec_success_code=0, 
        @on_success_action=3, 
        @on_success_step_id=0, 
        @on_fail_action=2, 
        @on_fail_step_id=0, 
        @retry_attempts=0, 
        @retry_interval=0, 
        @os_run_priority=0, @subsystem=N'TSQL', 
        @command=N'DECLARE @StartDate DATE,
    @EndDate DATE
SET @StartDate = ''1-8-2015'';
SET @EndDate = GETDATE();

TRUNCATE TABLE dbo.rpt_daily_rmx_counts;

WITH CTE_Trips_By_Day_Per_Site
AS
(
    SELECT  a.SourceDB,
    a.Trip_Date,
    DATENAME(Weekday,CAST(a.Trip_Date AS DATE)) AS DAY_Of_Week,
    ISNULL(b.Total_Trips,0) AS Total_Trips
    FROM ( /* Get the SourceDb''s and every day since @StartDate as initial Data set to make sure we get every day even if there are days without data*/
            SELECT DISTINCT SourceDB, Trip_Date
            FROM (SELECT DATEADD(day, number, @StartDate) AS Trip_Date
                                 FROM   ( SELECT DISTINCT number
                                          FROM master.dbo.spt_values
                                          WHERE name IS NULL
                                        ) n
                                 WHERE  DATEADD(day, number, @StartDate) <= @EndDate
            ) Trip_Date
            CROSS JOIN dbo.RMX_XREF_ProfitCenters   
            WHERE SourceDB NOT IN ( ''INDY'' )
     ) a
    LEFT OUTER JOIN (SELECT SUBSTRING(UniqueTripId, 0, CHARINDEX(''.'', UniqueTripId, 0)) AS SourceDB,
                CAST(event_create_timestamp AS DATE) Trip_Date,
                DATENAME(Weekday,CAST(event_create_timestamp AS DATE)) AS DAY_Of_Week,
                COUNT(*) AS Total_Trips
                FROM dbo.rmx_eventsummary summ ( NOLOCK )
                INNER JOIN ( SELECT localtransactionid,
                            event_sequence,
                            SourceSite,
                            SourceAgency,
                            SourceVendor,
                            TransactionType,
                            ePCRNumber,
                            UniqueTripId,
                            CADIncidentNumber,
                            PatientFirstName,
                            PatientLastName,
                            [Date Of Service],
                            ProfitCenter,
                            BillingRateProfitCenterID
                            FROM   (SELECT localtransactionid,
                                    event_sequence,
                                    event_contextdata_element_name,
                                    event_contextdata_element_value
                                    FROM      dbo.rmx_eventcontextdata (NOLOCK)
                                    ) ctx 
                                    PIVOT
                                        ( MAX(event_contextdata_element_value) FOR event_contextdata_element_name IN ( SourceSite,
                                                                                                                          SourceAgency,
                                                                                                                          SourceVendor,
                                                                                                                          TransactionType,
                                                                                                                          ePCRNumber,
                                                                                                                          UniqueTripId,
                                                                                                                          CADIncidentNumber,
                                                                                                                          PatientFirstName,
                                                                                                                          PatientLastName,
                                                                                                                          [Date Of Service],
                                                                                                                          ProfitCenter,
                                                                                                                          BillingRateProfitCenterID ) ) piv
                                       ) pivotctx ON summ.localtransactionid = pivotctx.localtransactionid
                                                 AND summ.event_sequence = pivotctx.event_sequence
                WHERE   ( (summ.subcomponent_name = ''AdvanceClaim_TripSender_MF''
                          AND summ.event_type IN ( ''TRANSACTION_DELIVERED'',
                                                   ''TRANSACTION_DELEIVERED'' ))
                        )
                        AND event_create_timestamp > @StartDate
                GROUP BY SUBSTRING(UniqueTripId, 0, CHARINDEX(''.'', UniqueTripId, 0)),
                        CAST(event_create_timestamp AS DATE),
                        event_type,
                        event_summary
        ) b
        ON a.sourcedb = b.sourcedb
        AND a.Trip_Date = b.Trip_Date
)
INSERT INTO dbo.rpt_daily_rmx_counts
        ( SourceSite,
          Trip_Date,
          Day_of_Week,
          Total_Trips,
          Average,
          Two_Week_Average,
          Two_Week_Min,
          Two_Week_Max
        )
SELECT SourceDB AS SourceSite,Trip_Date,
DAY_Of_Week,
Total_Trips,
AVG(Total_Trips) OVER (PARTITION BY SourceDB) AS Average,
AVG(Total_Trips) OVER ( PARTITION BY SourceDB
                       ORDER BY Trip_Date
                       ROWS 14 PRECEDING ) AS Two_Week_Average,
Min(Total_Trips) OVER ( PARTITION BY SourceDB
                       ORDER BY Trip_Date
                       ROWS 14 PRECEDING ) AS Two_Week_Min,
Max(Total_Trips) OVER ( PARTITION BY SourceDB
                       ORDER BY Trip_Date
                       ROWS 14 PRECEDING ) AS Two_Week_Max
FROM CTE_Trips_By_Day_Per_Site a
ORDER BY 1,2;', 
        @database_name=N'RMx_ODS', 
        @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Email to DG_RMX]    Script Date: 10/13/2015 1:47:38 PM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Email to DG_RMX', 
        @step_id=2, 
        @cmdexec_success_code=0, 
        @on_success_action=1, 
        @on_success_step_id=0, 
        @on_fail_action=2, 
        @on_fail_step_id=0, 
        @retry_attempts=0, 
        @retry_interval=0, 
        @os_run_priority=0, @subsystem=N'TSQL', 
        @command=N'DECLARE @Subject VARCHAR(250),  @Body VARCHAR(250), 
@recipients VARCHAR(40) = ''DG_RMx@rmetro.com'',
@TripDate VARCHAR(10) = CAST(GETDATE() -1 AS DATE), @QueryText VARCHAR(5000),
@FileName VARCHAR(50) 

SET @QueryText =  ''SELECT Trip_Date AS Date_RMx_Received,
    CASE WHEN SourceSite IN (''''PMT'''',''''Maricopa'''') THEN ''''Starwest''''
        WHEN SourceSite IN (''''SANDIEGO'''',''''SANTACLARA'''') THEN ''''ImageTrend''''
        WHEN SourceSite IN (''''SUMTER'''') THEN ''''Intermedix''''
        WHEN SourceSite IN (''''ERLA'''',''''LGB'''') THEN ''''CAD Shell -- Zoll''''
        ELSE ''''Zoll'''' END AS EPCR_Type,
    SourceSite,Total_Trips,Average,Two_Week_Average,Two_Week_Min, Two_Week_Max,
            CAST((CASE WHEN Average > 0 THEN Total_Trips/CAST(Average AS DECIMAL (12,4)) ELSE CAST(0.0000 AS DECIMAL(12,4)) END) AS DECIMAL(12,3)) * 100 AS Trips_to_Average,
            CAST((CASE WHEN Two_Week_Average > 0 THEN Total_Trips/CAST(Two_Week_Average AS DECIMAL (12,4)) ELSE 0.0000 END) AS DECIMAL(12,3)) * 100 AS Trips_to_Two_Week_Average
            FROM RMx_Ods.dbo.rpt_daily_rmx_counts
            WHERE Trip_Date = '''''' + @TripDate  + ''''''
            ORDER BY CASE WHEN Average > 0 THEN Total_Trips/CAST(Average AS DECIMAL (12,4)) ELSE 0.0000 END DESC'';

SET @FileName = ''Daily_RMX_Trend'' + REPLACE(@TripDate,''-'',''_'') + ''.csv'';
 
SET @Subject = ''RMx to AC by Day Per Site'' + @TripDate ;
SET @Body = @Subject;--''Test You''''ll see and email like this starting tomorrow'';

EXEC msdb.dbo.sp_send_dbmail @profile_name = ''SQL Alerts'',
    @recipients = @recipients, @subject = @Subject, @body = @Body,
    --@Body_format = ''HTML'',
    @Query = @QueryText,
    @attach_query_result_as_file = 1,
    @query_attachment_filename = @FileName,--''Daily_RMX_Trend.csv'',
    @query_result_separator = '','',
    --@attach_query_result_as_file = 1,
    @query_result_no_padding= 1,
    @exclude_query_output =1,
    --@append_query_error = 0,
    @query_result_header =1;
', 
        @database_name=N'msdb', 
        @flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'RMx_Daily_Stats_to_AC', 
        @enabled=1, 
        @freq_type=4, 
        @freq_interval=1, 
        @freq_subday_type=1, 
        @freq_subday_interval=0, 
        @freq_relative_interval=0, 
        @freq_recurrence_factor=0, 
        @active_start_date=20150310, 
        @active_end_date=99991231, 
        @active_start_time=4500, 
        @active_end_time=235959, 
        --@schedule_uid=N'9822d9ae-7ecc-4ac5-a15c-e1a60202856c'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:

GO


