use rmx_ods;
/*
select summ.*, ctx.Event_ContextData_Element_Value as UniqueTripID
from dbo.RMx_EventSummary summ (NOLOCK)
	INNER JOIN dbo.RMx_EventContextData ctx (NOLOCK)
	ON summ.LocalTransactionID=ctx.LocalTransactionID and summ.Event_Sequence=ctx.Event_Sequence 
	--inner join dbo.RMx_EventBitStreamData bs (NOLOCK) ON bs.LocalTransactionID=summ.LocalTransactionID and bs.Event_Sequence=1
WHERE
	summ.SourceSystem in ('TriTech','Medusa Medical Technologies')
	and ctx.Event_ContextData_Element_Name='UniqueTripID'
	and summ.event_sequence > 1
	and ctx.Event_ContextData_Element_Value='Maricopa.2015-10-28T09:04:53.R821'
order by summ.Event_Create_Timestamp desc
*/
BEGIN transaction MANTIS282
GO

with xmlnamespaces ('http://www.nemsis.org' as nem, 'http://www.w3.org/2001/XMLSchema-instance' as xsi)
update dbo.RMx_EventContextData
set Event_ContextData_Element_Value = 
	CASE
		WHEN CHARINDEX('-07:00',Event_ContextData_Element_Value)>1 THEN REPLACE(Event_ContextData_Element_Value,'-07:00','')
		WHEN CHARINDEX('-08:00',Event_ContextData_Element_Value)>1 THEN 

			REPLACE(Event_ContextData_Element_Value,
					SUBSTRING(Event_ContextData_Element_Value,
							  CHARINDEX('-08:00',Event_ContextData_Element_Value)-19,
							  25),
					CONVERT(Varchar,
							DATEADD(hour,
									1,
									CONVERT(DateTime, 
											SUBSTRING(Event_ContextData_Element_Value,
													CHARINDEX('-08:00',Event_ContextData_Element_Value)-19,
													19), 
											127)), 
					126))

		ELSE Event_ContextData_Element_Value
	END 
from dbo.RMx_EventSummary summ (NOLOCK)
	INNER JOIN dbo.RMx_EventContextData ctx (NOLOCK)
	ON summ.LocalTransactionID=ctx.LocalTransactionID and summ.Event_Sequence=ctx.Event_Sequence 
WHERE
	summ.SourceSystem='Medusa Medical Technologies'
	and ctx.Event_ContextData_Element_Name='UniqueTripID'
GO

select @@rowcount as RowsAffected
Go

COMMIT transaction MANTIS282
GO
