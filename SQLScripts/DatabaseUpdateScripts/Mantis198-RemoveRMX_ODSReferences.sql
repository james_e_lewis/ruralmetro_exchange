update [dbo].[RMX_REPROCESSING_JOB_LOOKUP]
set SQLQuery = 'SELECT 
  outSummary.LocalTransactionID,
  outSummary.Global_TransactionID,
  outSummary.Event_Type,
  outSummary.Event_Details,
  outSummary.Event_Sequence,
  contextdata.UniqueTripID,
  eventStream.Event_BitStreamData 
FROM
  dbo.RMx_EventSummary outSummary
INNER JOIN dbo.RMx_EventBitStreamData eventStream
ON
  outSummary.LocalTransactionID = eventStream.LocalTransactionID
  and eventStream.event_sequence = 1
INNER JOIN dbo.vw_Context_Values_Pivoted contextdata
ON
  contextdata.LocalTransactionID               = outSummary.localtransactionid
AND contextdata.event_sequence                 = outSummary.event_sequence
WHERE
  outSummary.Event_Type               =''BUSINESS_FAULT''
AND outSummary.Event_Summary          =''Trip has Invalid Profit Center value''
AND outSummary.event_create_timestamp >
  (  -- This clause catches all failures that have never been reprocessed yet
    SELECT
      isnull(MAX(datetimerun),''2014-12-20'')
    FROM
      dbo.rmx_reprocessing_history_lookup history
    INNER JOIN dbo.rmx_reprocessing_job_lookup job
    ON
      job.jobid = history.jobid
    WHERE
      job.name = ''Invalid Profit Center Business Errors''
      AND history.JobStatus = ''Success''
  )
AND
  ( --this clause ensures we filter those records by Unique Trip ID which
    -- have not yet exceeded the configured max retries for this job
    SELECT
      COUNT(*)
    FROM
      dbo.rmx_eventsummary insummary
    INNER JOIN dbo.vw_Context_Values_Pivoted innercontext
    ON
      insummary.localtransactionid                  = innercontext.localtransactionid
    AND insummary.event_sequence                    = innercontext.event_sequence
    WHERE
      outsummary.subcomponent_name                   = insummary.subcomponent_name
    AND innercontext.UniqueTripId = contextdata.UniqueTripId
  )
  <
  (
    SELECT
      isnull(MAX(maxretries), 5000)
    FROM
      dbo.rmx_reprocessing_job_lookup
    WHERE
      name = ''Invalid Profit Center Business Errors''
  );'
WHERE
    [JobID] = 1;
GO


update [dbo].[RMX_REPROCESSING_JOB_LOOKUP]
set SQLQuery = 'SELECT
      outSummary.LocalTransactionID,
      outSummary.Global_TransactionID,
      outSummary.Event_Type,
      outSummary.Event_Details,
      outSummary.Event_Sequence,
      eventStream.Event_BitStreamData
    FROM
      dbo.RMx_EventSummary outSummary
    INNER JOIN dbo.RMx_EventBitStreamData eventStream
    ON
      outSummary.LocalTransactionID = eventStream.LocalTransactionID
      and eventStream.event_sequence = 1
    INNER JOIN dbo.vw_Context_Values_Pivoted contextdata
    ON
      contextdata.LocalTransactionID               = outSummary.localtransactionid
    AND contextdata.event_sequence                 = outSummary.event_sequence
    WHERE
      outSummary.Event_Type               =''TECHNICAL_FAULT''
    AND outSummary.Event_Summary          =''Error occured during Advance Claim call''
    AND outSummary.event_create_timestamp >
      (  -- This clause catches all failures that have never been reprocessed yet
        SELECT
          isnull(MAX(datetimerun),''2014-12-01'')
        FROM
          dbo.rmx_reprocessing_history_lookup history
        INNER JOIN dbo.rmx_reprocessing_job_lookup job
        ON
          job.jobid = history.jobid
        WHERE
          job.name = ''Advance Claim Submit Technical Errors''
          AND history.JobStatus = ''Success''

      )
    AND
      ( --this clause ensures we filter those records by Unique Trip ID which
        -- have not yet exceeded the configured max retries for this job
        SELECT
          COUNT(*)
        FROM
          dbo.rmx_eventsummary insummary
        INNER JOIN dbo.vw_Context_Values_Pivoted innercontext
        ON
          insummary.localtransactionid                  = innercontext.localtransactionid
        AND insummary.event_sequence                    = innercontext.event_sequence
        WHERE
          outsummary.subcomponent_name                   = insummary.subcomponent_name
        AND innercontext.UniqueTripId                   = contextdata.UniqueTripId
      )
      <
      (
        SELECT
          isnull(MAX(maxretries), 5000)
        FROM
          dbo.rmx_reprocessing_job_lookup
        WHERE
          name = ''Advance Claim Submit Technical Errors''
      )'
WHERE
    [JobID] = 2;
GO