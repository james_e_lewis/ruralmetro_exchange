USE [RMx_ODS]
GO

UPDATE [dbo].[RMX_REPROCESSING_JOB_LOOKUP]
   SET [SQLQuery] = 'SELECT
  outSummary.LocalTransactionID,
  outSummary.Global_TransactionID,
  outSummary.Event_Type,
  outSummary.Event_Details,
  outSummary.Event_Sequence,
  eventStream.Event_BitStreamData
FROM
  RMx_ODS.dbo.RMx_EventSummary outSummary (NOLOCK)
INNER JOIN RMx_ODS.dbo.RMx_EventBitStreamData eventStream (NOLOCK)
ON
  outSummary.LocalTransactionID = eventStream.LocalTransactionID
  and eventStream.event_sequence = 1
INNER JOIN RMx_ODS.dbo.RMx_EventContextData contextdata (NOLOCK)
ON
  contextdata.LocalTransactionID               = outSummary.localtransactionid
AND contextdata.event_sequence                 = outSummary.event_sequence
AND contextdata.event_contextdata_element_name = ''UniqueTripId''
WHERE
  outSummary.Event_Type               =''TECHNICAL_FAULT''
AND outSummary.Event_Summary          =''Error occured during Advance Claim call''
AND outSummary.event_create_timestamp >
  (  -- This clause catches all failures that have never been reprocessed yet
    SELECT
      isnull(MAX(datetimerun),''2014-12-01'')
    FROM
      RMx_ODS.dbo.rmx_reprocessing_history_lookup history (NOLOCK)
    INNER JOIN RMx_ODS.dbo.rmx_reprocessing_job_lookup job (NOLOCK)
    ON
      job.jobid = history.jobid
    WHERE
      job.name = ''Advance Claim Submit Technical Errors'' and history.JobStatus = ''Success''
  )
AND
  ( --this clause ensures we filter those records by Unique Trip ID which
    -- have not yet exceeded the configured max retries for this job
    SELECT
      COUNT(*)
    FROM
      RMx_ODS.dbo.rmx_eventsummary insummary (NOLOCK)
    INNER JOIN RMx_ODS.dbo.rmx_eventcontextdata innercontext (NOLOCK)
    ON
      insummary.localtransactionid                  = innercontext.localtransactionid
    AND insummary.event_sequence                    = innercontext.event_sequence
    AND innercontext.event_contextdata_element_name =
      contextdata.event_contextdata_element_name --''UniqueTripId''
    WHERE
      outsummary.subcomponent_name                   = insummary.subcomponent_name
    AND innercontext.event_contextdata_element_value =
      contextdata.event_contextdata_element_value
  )
  <
  (
    SELECT
      isnull(MAX(maxretries), 5000)
    FROM
      rmx_ods.dbo.rmx_reprocessing_job_lookup (NOLOCK)
    WHERE
      name = ''Advance Claim Submit Technical Errors''
  );'
 WHERE [JobID] = 2
GO

UPDATE [dbo].[RMX_REPROCESSING_JOB_LOOKUP]
   SET [SQLQuery] = 'SELECT 
  outSummary.LocalTransactionID,
  outSummary.Global_TransactionID,
  outSummary.Event_Type,
  outSummary.Event_Details,
  outSummary.Event_Sequence,
  contextdata.event_contextdata_element_value AS UniqueTripID,
  eventStream.Event_BitStreamData 
FROM
  RMx_ODS.dbo.RMx_EventSummary outSummary (NOLOCK)
INNER JOIN RMx_ODS.dbo.RMx_EventBitStreamData eventStream (NOLOCK)
ON
  outSummary.LocalTransactionID = eventStream.LocalTransactionID
  and eventStream.event_sequence = 1
INNER JOIN RMx_ODS.dbo.RMx_EventContextData contextdata (NOLOCK)
ON
  contextdata.LocalTransactionID               = outSummary.localtransactionid
AND contextdata.event_sequence                 = outSummary.event_sequence
AND contextdata.event_contextdata_element_name = ''UniqueTripId''
WHERE
  outSummary.Event_Type               =''BUSINESS_FAULT''
AND outSummary.Event_Summary          =''Trip has Invalid Profit Center value''
AND outSummary.event_create_timestamp >
  (  -- This clause catches all failures that have never been reprocessed yet
    SELECT
      isnull(MAX(datetimerun),''2015-01-01'')
    FROM
      rmx_ods.dbo.rmx_reprocessing_history_lookup history (NOLOCK)
    INNER JOIN rmx_ods.dbo.rmx_reprocessing_job_lookup job (NOLOCK)
    ON
      job.jobid = history.jobid
    WHERE
      job.name = ''Invalid Profit Center Business Errors'' and history.JobStatus = ''Success''
  )
AND
  ( --this clause ensures we filter those records by Unique Trip ID which
    -- have not yet exceeded the configured max retries for this job
    SELECT
      COUNT(*)
    FROM
      rmx_ods.dbo.rmx_eventsummary insummary (NOLOCK)
    INNER JOIN rmx_ods.dbo.rmx_eventcontextdata innercontext (NOLOCK)
    ON
      insummary.localtransactionid                  = innercontext.localtransactionid
    AND insummary.event_sequence                    = innercontext.event_sequence
    AND innercontext.event_contextdata_element_name =
      contextdata.event_contextdata_element_name --UniqueTripId
    WHERE
      outsummary.subcomponent_name                   = insummary.subcomponent_name
    AND innercontext.event_contextdata_element_value =
      contextdata.event_contextdata_element_value
  )
  <
  (
    SELECT
      isnull(MAX(maxretries), 5000)
    FROM
      rmx_ods.dbo.rmx_reprocessing_job_lookup (NOLOCK)
    WHERE
      name = ''Invalid Profit Center Business Errors''
  );'
WHERE [JobID] = 1
GO