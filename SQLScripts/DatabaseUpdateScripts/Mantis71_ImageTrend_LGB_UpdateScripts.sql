 update [RMx_ODS_Dev02].[dbo].[RMX_BILLABLECODE_LOOKUP] SET SourceSite = 'SANDIEGO' where SourceAgency ='06';
 update [RMx_ODS_Dev02].[dbo].[RMX_BILLABLECODE_LOOKUP] SET SourceSite = 'SANTACLARA' where SourceAgency ='XSCEMS';

update [RMx_ODS_Dev02].[dbo].[RMX_SITE_DETERMINATION_LOOKUP] SET SITE   = 'SANDIEGO' where AgencyNumber ='06';
update [RMx_ODS_Dev02].[dbo].[RMX_SITE_DETERMINATION_LOOKUP] SET SITE = 'SANTACLARA' where AgencyNumber ='XSCEMS';

update [RMx_ODS_Dev02].[dbo].[RMX_XREF_ProfitCenters] SET SourceDB  = 'SANDIEGO' where Code ='06';
update [RMx_ODS_Dev02].[dbo].[RMX_XREF_ProfitCenters] SET SourceDB  = 'SANTACLARA' where Code ='XSCEMS';