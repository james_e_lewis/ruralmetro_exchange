use rmx_ods;

begin transaction mantis283
GO

with xmlnamespaces ('http://www.nemsis.org' as nem, 'http://www.w3.org/2001/XMLSchema-instance' as xsi)
update dbo.RMx_EventContextData 
set Event_ContextData_Element_Value = bs.Event_XMLPayloadData.value('(//nem:E02_03)[1]', 'nvarchar(max)')
from dbo.RMx_EventContextData ctx (NOLOCK)
	inner join dbo.RMx_EventBitStreamData bs (NOLOCK)
	on	bs.LocalTransactionID=ctx.LocalTransactionID and bs.Event_Sequence=1
	inner join dbo.RMx_EventSummary summ (NOLOCK)
	on summ.LocalTransactionID=ctx.LocalTransactionID and summ.Event_Sequence=ctx.Event_Sequence
where
	sourcesystem = 'Medusa Medical Technologies'
	and summ.SubComponent_Name in ('AdvanceClaim_TripPreprocessor_MF','AdvanceClaim_TripSender_MF')
	and ctx.Event_ContextData_Element_Name='CADIncidentNumber'
GO

select @@rowcount as numCADIncidentNumsUpdated
GO

/*
select summ.*, ctx.*
from dbo.RMx_EventSummary summ (NOLOCK)
	inner join dbo.vw_Context_Values_Pivoted ctx
	ON summ.LocalTransactionID=ctx.LocalTransactionID and summ.Event_Sequence=ctx.Event_Sequence 
WHERE
	summ.SourceSystem in ('TriTech','Medusa Medical Technologies')
	and summ.event_sequence > 1
	and ctx.UniqueTripID='Maricopa.2015-10-28T09:04:53.R821'
order by summ.Event_Create_Timestamp desc
*/
with xmlnamespaces ('http://www.nemsis.org' as nem, 'http://www.w3.org/2001/XMLSchema-instance' as xsi)
insert into dbo.RMx_EventContextData (LocalTransactionID, Event_Sequence, Event_ContextData_Sequence, Event_ContextData_Element_Name, Event_ContextData_Element_Value, Event_ContextData_Element_Type)
select summ.LocalTransactionID,
		summ.Event_Sequence,
		(select ISNULL(max(Event_ContextData_Sequence),0)+1 from dbo.RMx_EventContextData ctx2 where ctx2.LocalTransactionID=summ.LocalTransactionID and ctx2.Event_Sequence=summ.Event_Sequence),
		'CADMasterIncident',
		CASE
			WHEN bs.Event_XMLPayloadData.value('(//nem:E02_02)[1]', 'nvarchar(max)') is null then bs.Event_XMLPayloadData.value('(//nem:E02_03)[1]', 'nvarchar(max)')
			ELSE bs.Event_XMLPayloadData.value('(//nem:E02_02)[1]', 'nvarchar(max)')
		END,
		'String'
from dbo.RMx_EventSummary summ (NOLOCK) inner join dbo.RMx_EventBitStreamData bs (NOLOCK)
	on summ.LocalTransactionID=bs.LocalTransactionID and bs.Event_Sequence=1
where
	sourcesystem = 'Medusa Medical Technologies'
	and summ.SubComponent_Name in ('AdvanceClaim_TripPreprocessor_MF','AdvanceClaim_TripSender_MF')
	and summ.Event_Sequence=2
GO

select @@rowcount as numCADMasterIncidentsInserted
GO

commit transaction mantis283
GO