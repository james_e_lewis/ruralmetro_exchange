update rmx_ods.dbo.rmx_eventcontextdata
set event_contextdata_element_name = 'OLDPatientFirstName'
where event_contextdata_element_name = 'PatientFirstName';

update rmx_ods.dbo.rmx_eventcontextdata
set event_contextdata_element_name = 'PatientFirstName'
where event_contextdata_element_name = 'PatientLastName';

update rmx_ods.dbo.rmx_eventcontextdata
set event_contextdata_element_name = 'PatientLastName'
where event_contextdata_element_name = 'OLDPatientFirstName';