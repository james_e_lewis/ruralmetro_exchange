USE [System_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[ProfitCenterLookup]    Script Date: 7/9/2015 1:53:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[ProfitCenterLookup] (@Site VARCHAR(20) = NULL, 
                                                       @UnitCallSign VARCHAR(20),
                                                       @Incident VARCHAR(20) = NULL, 
                                                       @ProfitCenter VARCHAR(20) =  NULL OUT)
AS
SET NOCOUNT ON
BEGIN
/*  First query searches by Incident and UnitCall sign, the most accurate way */
SELECT DISTINCT @ProfitCenter = vi.SerialNumber
FROM response_master_incident RMI (NOLOCK)
INNER JOIN Response_Vehicles_Assigned (NOLOCK) RVA ON RVA.Master_Incident_ID = RMI.ID
inner join dbo.Vehicle v (NOLOCK) ON V.ID = RVA.Vehicle_ID
INNER JOIN dbo.VehicleInventory vi (NOLOCK) ON v.ID = vi.Vehicle_ID
INNER JOIN dbo.Inventory i (NOLOCK) ON i.ID = vi.InventoryID
WHERE (rva.radio_Name = @UnitCallSign OR rva.radio_Name LIKE '[a-Z]' + @UnitCallSign)
       AND v.IsActive = 'true'
       AND I.Code = 'Prof'
       AND vi.SerialNumber IS NOT NULL
       AND RMI.Master_Incident_Number =  @Incident
       AND vi.id =
       (SELECT MAX(vi2.id)
              FROM dbo.vehicleinventory vi2  (NOLOCK)
              INNER JOIN dbo.Inventory i2 (NOLOCK) ON i2.ID = vi2.InventoryID
              AND v.IsActive = 'true'
              AND i2.code = 'Prof'
              WHERE vi2.vehicle_id = vi.vehicle_id
              AND vi2.serialnumber != '1'
              AND vi2.serialnumber IS NOT NULL )
END

/* If the first query doesn't return a profit center then search by UnitCallSign alone */
IF @ProfitCenter IS NULL
BEGIN 
       SELECT DISTINCT @ProfitCenter = vi.SerialNumber
       FROM dbo.Vehicle v (NOLOCK)
       INNER JOIN dbo.VehicleInventory vi (NOLOCK) ON v.ID = vi.Vehicle_ID
       INNER JOIN dbo.Inventory i (NOLOCK) ON i.ID = vi.InventoryID
       WHERE (v.Name = @UnitCallSign
              OR v.Name LIKE '[a-Z]' + @UnitCallSign)
              AND v.IsActive = 'true'
              AND I.Code = 'Prof'
              AND vi.SerialNumber IS NOT NULL
              AND vi.id =
              (SELECT MAX(vi2.id)
                     FROM dbo.vehicleinventory vi2
                     INNER JOIN dbo.Inventory i2 ON i2.ID = vi2.InventoryID
                     AND v.IsActive = 'true'
                     AND i2.code = 'Prof'
                     WHERE vi2.vehicle_id = vi.vehicle_id
                     AND vi2.serialnumber != '1'
                     AND vi2.serialnumber IS NOT NULL)
END

IF @ProfitCenter IS NULL SELECT @ProfitCenter = 'PC not Found'

/*  If by chance more than on profit center is returned set the profit center to "Multiple Records" */
IF @@Rowcount > 1 SELECT @ProfitCenter = 'Multiple Records'

GO
