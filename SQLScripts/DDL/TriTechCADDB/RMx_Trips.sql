USE [System_Reporting]
GO

/****** Object:  Table [dbo].[RMX_Trips]    Script Date: 6/16/2015 3:04:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RMX_Trips](
    [Master_Incident_ID] [int] NOT NULL,
    [Vehicle_Assigned_ID] [int] NOT NULL,
    [Event_Trigger_DTTM] [datetime] NOT NULL,
    [RMX_PickUp_DTTM] [datetime] NULL,
 CONSTRAINT [pk_RMX_Trips] PRIMARY KEY CLUSTERED 
(
    [Master_Incident_ID] ASC,
    [Vehicle_Assigned_ID] ASC,
    [Event_Trigger_DTTM] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

GRANT UPDATE,INSERT,DELETE ON dbo.RMx_Trips TO ro_rmx_prod
GO
