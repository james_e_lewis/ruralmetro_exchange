SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[BuildTriTechXML]
    (
      @Master_incident_id INT,
      @vehicle_Assigned_id INT,
      @Cad_data VARCHAR(8000) OUTPUT
    )
AS
    BEGIN 
        DECLARE @Cad_Data_max VARCHAR(MAX)
        SET @Master_incident_id = @Master_incident_id
        SET @vehicle_Assigned_id = @vehicle_Assigned_id

        SET @Cad_Data_max = CAST(( SELECT   id AS IncidentID,
                                        master_incident_number AS MasterIncidentNumber,
                                        response_date AS ResponseDate,
                                        Agency_type AS Agency,
                                        Jurisdiction,
                                        Division,
                                        Battalion,
                                        response_area AS ResponseArea,
                                        response_plan AS ResponsePlan,
                                        incident_type AS IncidentType,
                                        ( SELECT top 1    code
                                          FROM      dbo.problem
                                          WHERE     description = Problem
                                        ) AS ProblemCode,
                                        problem AS ProblemDescription,
                                        priority_number AS PriorityNumber,
                                        priority_description AS PriorityDescription,
                                        ( SELECT top 1   code
                                          FROM      dbo.Locations
                                          WHERE     name = location_name
                                        ) AS LocationCode,
                                        location_name AS LocationName,
                                        location_type AS LocationType,
                                        Address,
                                        house_number AS AddressHouseNumber,
                                        house_number_suffix AS AddressHouseNumberSuffix,
                                        prefix_directional AS AddressPrefixDirectional,
                                        name_component AS AddressStreetName,
                                        street_type AS AddressStreetType,
                                        post_directional AS AddressPostDirectional,
                                        Apartment,
                                        Building,
                                        City,
                                        State,
                                        postal_code AS PostalCode,
                                        County,
                                        CONVERT(decimal(10,6),CAST(isnull(Longitude,0) as Decimal)/-1000000) as Longitude,
                                        CONVERT(decimal(10,6),CAST(isnull(Latitude,0) as Decimal)/1000000) as Latitude,
                                        map_info AS MapInfo,
                                        cross_street AS CrossStreet,
                                        MethodOfCallRcvd,
                                        call_back_phone AS CallbackPhone,
                                        call_back_phone_ext AS CallbackPhoneExt,
                                        caller_type AS CallerType,
                                        caller_name AS CallerName,
                                        caller_address AS CallerAddress,
                                        caller_apartment AS CallerApartment,
                                        caller_building AS CallerBuilding,
                                        caller_city AS CallerCity,
                                        caller_state AS CallerState,
                                        caller_postal_code AS CallerPostalCode,
                                        caller_county AS CallerCounty,
                                        caller_location_phone AS CallerLocationPhone,
                                        caller_location_name AS CallerLocationName,
                                        time_phonepickup AS TimePhonePickup,
                                        time_firstcalltakingkeystroke AS TimeFirstCall,
                                        time_callenteredqueue AS TimeCallEnteredQueue,
                                        time_calltakingcomplete AS TimeCallTakingComplete,
                                        time_first_unit_assigned AS TimeFirstUnitAssigned,
                                        time_first_unit_enroute AS TimeFirstUnitEnroute,
                                        TimeFirstStaged,
                                        time_first_unit_arrived AS TimeFirstUnitArrived,
                                        TimeFirstPTContact AS TimeFirstContact,
                                        time_incident_under_control AS TimeIncidentUnderControl,
                                        TimeFirstDelayedAvail AS TimeFirstDelayedAvailable,
                                        TimeFirstCallCleared,
                                        time_callclosed AS TimeCallClosed,
                                        Elapsed_CallRcvd2InQueue,
                                        Elapsed_CallRcvd2CalTakDone,
                                        Elapsed_CallRcvd2FirstAssign,
                                        Elapsed_CallRcvd2CallClosed,
                                        Elapsed_InQueue_2_FirstAssign AS Elapsed_InQueue2FirstAssign,
                                        Elapsed_Assigned2FirstEnroute,
                                        Elapsed_Enroute2FirstAtScene,
                                        Fixed_Time_PhonePickUp AS FixedTimePhonePickup,
                                        Fixed_Time_CallEnteredQueue AS FixedTimeCallEnteredQueue,
                                        Fixed_Time_CallTakingComplete AS FixedTimeCallTakingComplete,
                                        Fixed_Time_CallClosed AS FixedTimeCallClosed,
                                        CallTaking_Performed_By AS CallTakingPerformedBy,
                                        CallClosing_Performed_By AS CallClosingPerformedBy,
                                        CallDisposition_Performed_By,
                                        StagedPerfBy AS StagedPerformedBy,
                                        PtContactPerfBy AS ContactPerformedBy,
                                        DelayedPerfBy AS DelayedAvailPerformedBy,
                                        CallClearedPerfBy AS CallClearedPerformedBy,
                                        Command_Channel AS CommandChannel,
                                        Primary_TAC_Channel AS PrimaryTACChannel,
                                        Alternate_TAC_Channel AS AlternateTACChannel,
                                        Call_Disposition AS CallDisposition,
                                        Cancel_Reason AS CancelReason,
                                        base_response_number AS BaseResponseNumber,
                                        confirmation_number AS ConfirmationNumber,
                                        certification_level AS CertificationLevel,
                                        Determinant,
                                        EMD_Used,
                                        Num_ANIALI_Calls,
                                        senttobilling_flag AS SentToBillingFlag,
                                        call_is_active AS CallIsActive,
                                        cautionnote AS CautionNoteFlag,
                                        hazmatnote AS HazMatNoteFlag,
                                        premisehistory AS PremiseHistoryFlag,
                                        call_source AS CallSource,
                                        MachineName,
                                        ( SELECT   TOP 1 ( CAST(date_time AS VARCHAR(20))
                                                      + CAST(comment_type AS VARCHAR(10))
                                                      + ' ['
                                                      + CAST(id AS VARCHAR(20))
                                                      + '] [' + performed_by
                                                      + '] ' + comment ) AS "data()"
                                          FROM      dbo.response_comments comm ( NOLOCK )
                                          WHERE     comm.master_incident_id = inc.id
                                        FOR
                                          XML PATH('Comment'),
                                              TYPE
                                        ) AS ResponseComments,
                                        ( SELECT    rva.id AS RVA_ID,
                                                    rva.response_number AS UnitResponseNumber,
                                                    rva.radio_name AS UnitName,
                                                    rva.Jurisdiction,
                                                    rva.time_assigned AS TimeAssigned,
                                                    rva.assign_performed_by AS DispatcherID,
                                                    rva.assign_performed_by AS AssignPerformedBy,
                                                    rva.time_enroute AS TimeEnroute,
                                                    rva.odometer_enroute AS OdometerEnroute,
                                                    rva.enroutetoscene_performed_by AS EnroutePerformedBy,
                                                    rva.time_staged AS TimeStaged,
                                                    rva.staging_performed_by AS StagedPerformedBy,
                                                    rva.time_arrivedatscene AS TimeAtScene,
                                                    CONVERT(decimal(10,6),CAST(isnull(rt.Latitude ,0) as Decimal)/1000000) AS lat_arrivedatscene,
                                                    CONVERT(decimal(10,6),CAST(isnull(rt.Longitude ,0) as Decimal)/-1000000) AS lon_arrivedatscene,
                                                    rva.Odometer_AtScene AS OdometerAtScene,
                                                    rva.ArrivedAtScene_Performed_By AS AtScenePerformedBy,
                                                    rva.time_contact AS TimeContact,
                                                    rva.Contact_Performed_By AS ContactPerformedBy,
                                                    rt.Time_Depart_Scene AS TimeDepartScene,
                                                    rt.Departure_Performed_By AS DepartScenePerformedBy,
                                                    rt.Time_Arrive_Destination AS TimeAtDestination,
                                                    rt.Odometer_At_Destination AS OdometerAtDestination,
                                                    rt.Transport_Mileage AS TransportMileage,
                                                    rt.ArrivedAtDest_Performed_By AS AtDestinationPerformedBy,
                                                    rva.Time_Delayed_Availability AS TimeDelayedAvailability,
                                                    rva.Delayed_Available_Performed_By AS DelayedAvailabilityPerformedBy,
                                                    rva.Time_Call_Cleared AS TimeClearedCall,
                                                    rva.Call_Clear_Performed_By AS ClearedCallPerformedBy,
                                                    rva.Time_ArrivedAtScene AS TimeAvailableAtScene,
                                                    rva.Avail_AtScene_Performed_By AS AvailAtScenePerformedBy,
                                                    rva.Number_Of_Victims_Seen AS NumberOfVictimsSeen,
                                                    rva.Call_Disposition AS UnitDisposition,
                                                    rva.Cancel_Reason AS CancelReason,
                                                    ( SELECT  Personnel_table_id AS EmpID,
                                                             Name
                                                      FROM    dbo.Response_Vehicles_Personnel
                                                      WHERE   master_incident_id = inc.ID
                                                              AND Veh_Assigned_ID = rva.ID
                                                    FOR
                                                      XML PATH('Employee'),
                                                          TYPE
                                                    ) AS Employees,
                                                    rt.Location_Name AS TransportLocationName,
                                                    rt.Location_Type AS TransportLocationType,
                                                    ( SELECT  vi.serialnumber
                                                      FROM    dbo.vehicle v ( NOLOCK )
                                                              INNER JOIN dbo.vehicleinventory vi ( NOLOCK ) ON v.ID = vi.Vehicle_ID
                                                              AND vi.ID = ( SELECT
                                                              MAX(vi2.id)
                                                              FROM
                                                              dbo.VehicleInventory vi2 ( NOLOCK )
                                                              INNER JOIN dbo.Inventory i ( NOLOCK ) ON vi2.InventoryID = i.ID
                                                              AND i.Code = 'Prof'
                                                              WHERE
                                                              vi2.Vehicle_ID = v.ID
                                                              )
                                                      WHERE   v.ID = rva.Vehicle_ID
                                                    ) AS VehicleProfitCenter,
                                                    rva.Agency_Type AS VehicleAgency,
                                                    rva.Division AS VehicleDivision,
                                                    rva.Battalion AS VehicleBattalion,
                                                    rt.Address AS DestinationAddress,
                                                    rt.City AS DestinationCity,
                                                    rt.State AS DestinationState,
                                                    rt.Postal_Code AS DestinationPostalCode,
                                                    rt.Transport_Protocol AS TransportProtocol,
                                                    rt.Name_First AS PatientFirstName,
                                                    rt.Name_Last AS PatientLastName,
                                                    rt.Name_MI AS PatientMiddleInitial,
                                                    ( SELECT top 1  Address
                                                      FROM    dbo.Patient_Information (NOLOCK)
                                                      WHERE   id = rt.Patient_Info_ID
                                                    ) AS PatientAddress,
                                                    ( SELECT top 1  City
                                                      FROM    dbo.Patient_Information (NOLOCK)
                                                      WHERE   id = rt.Patient_Info_ID
                                                    ) AS PatientCity,
                                                    ( SELECT top 1  County
                                                      FROM    dbo.Patient_Information (NOLOCK)
                                                      WHERE   id = rt.Patient_Info_ID
                                                    ) AS PatientCounty,
                                                    ( SELECT top 1  State
                                                      FROM    dbo.Patient_Information (NOLOCK)
                                                      WHERE   id = rt.Patient_Info_ID
                                                   ) AS PatientState,
                                                    ( SELECT top 1  Postal_code
                                                      FROM    dbo.Patient_Information (NOLOCK)
                                                      WHERE   id = rt.Patient_Info_ID
                                                    ) AS PatientPostalCode,
                                                    ( SELECT top 1  RTRIM(Social_Security)
                                                      FROM    dbo.Patient_Information (NOLOCK)
                                                      WHERE   id = rt.Patient_Info_ID
                                                    ) AS PatientSSN,
                                                    ( SELECT top 1 Sex
                                                      FROM    dbo.Patient_Information (NOLOCK)
                                                      WHERE   id = rt.Patient_Info_ID
                                                    ) AS PatientGender,
                                                    ( SELECT top 1  Date_of_Birth
                                                      FROM    dbo.Patient_Information (NOLOCK)
                                                      WHERE   id = rt.Patient_Info_ID
                                                    ) AS PatientDOB,
                                                    ( SELECT top 1  Phone
                                                      FROM    dbo.Patient_Information (NOLOCK)
                                                      WHERE   id = rt.Patient_Info_ID
                                                    ) AS PatientPhone,
                                                    ( SELECT top 1  insType.Name
                                                      FROM    dbo.Patient_Information pinfo ( NOLOCK )
                                                              INNER JOIN dbo.Patient_Insurance pins ( NOLOCK ) ON pinfo.id = pins.patient_info_id
                                                              INNER JOIN dbo.Insurance_Types insType ( NOLOCK ) ON pins.Type_ID = insType.ID
                                                      WHERE   rt.patient_info_id = pinfo.id
                                                    ) AS InsuranceType,
                                                    ( SELECT top 1  InsGroup
                                                      FROM    dbo.Patient_Information pinf ( NOLOCK )
                                                              INNER JOIN dbo.Patient_Insurance pins ( NOLOCK ) ON pinf.ID = pins.Patient_Info_ID
                                                      WHERE   pinf.id = rt.Patient_Info_ID
                                                    ) AS InsuranceGroup,
                                                    ( SELECT top 1 InsPlan
                                                      FROM    dbo.Patient_Information pinf ( NOLOCK )
                                                              INNER JOIN dbo.Patient_Insurance pins ( NOLOCK ) ON pinf.ID = pins.Patient_Info_ID
                                                      WHERE   pinf.id = rt.Patient_Info_ID
                                                    ) AS InsurancePlan,
                                                    ( SELECT top 1  Policy
                                                      FROM    dbo.Patient_Information pinf ( NOLOCK )
                                                              INNER JOIN dbo.Patient_Insurance pins ( NOLOCK ) ON pinf.ID = pins.Patient_Info_ID
                                                      WHERE   pinf.id = rt.Patient_Info_ID
                                                    ) AS InsurancePolicy
                                          FROM      dbo.response_vehicles_assigned rva ( NOLOCK )
                                                    LEFT OUTER JOIN dbo.response_transports rt ( NOLOCK ) ON rva.Master_Incident_ID = rt.Master_Incident_ID
                                                              AND rt.Vehicle_Assigned_ID = rva.ID
                                          WHERE     rva.master_incident_id = inc.ID
                                                    AND rva.id = @vehicle_Assigned_id
                                        FOR
                                          XML PATH('Unit'),
                                              TYPE
                                        ) AS UnitsAssigned
                               FROM     dbo.Response_master_incident inc ( NOLOCK )
                               WHERE    inc.id = @Master_incident_id
                             FOR
                               XML PATH('ROW'),
                                   ROOT('DOC')
                             ) AS VARCHAR(MAX))
    IF LEN(@Cad_data_Max) > 8000 
        SET @Cad_data_max = REPLACE(@Cad_Data,'  ',' ')
    IF LEN(@Cad_Data_Max) > 8000
        SET @Cad_data = 'Xml had more than 8000 characters'
    ELSE SET 
    @Cad_Data = @Cad_Data_Max

    END 
    
RETURN
GO
GRANT EXEC ON dbo.BuildTriTechXML TO PUBLIC
