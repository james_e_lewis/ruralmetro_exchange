--TRIGGER FOR UPDATE ON [dbo].[Response_Vehicles_Assigned]

CREATE TRIGGER RESPONSE_VEHICLE_ASSIGNED_UPDTRIGGER
ON [dbo].[Response_Vehicles_Assigned]
FOR UPDATE
AS 
DECLARE @CURRDATE DATETIME
SET @CURRDATE = GETDATE()

INSERT INTO RMX_TRIPS(Master_Incident_ID,Vehicle_Assigned_ID, Event_Trigger_DTTM)
SELECT I.Master_Incident_ID,I.ID,@CURRDATE
FROM INSERTED I INNER JOIN DELETED D ON  I.Master_Incident_ID = D.Master_Incident_ID
AND I.ID = D.ID
WHERE ((I.Time_Delayed_Availability IS NOT NULL AND D.Time_Delayed_Availability IS NULL)
OR
(I.Time_Enroute IS NOT NULL AND D.Time_Enroute IS NULL)
OR
(I.Time_Call_Cleared IS NOT NULL AND D.Time_Call_Cleared IS NULL))
AND NOT EXISTS
(SELECT 1 
FROM RMX_TRIPS
WHERE Master_Incident_ID = I.Master_Incident_ID
AND Vehicle_Assigned_ID = I.ID
AND RMX_PickUp_DTTM IS NULL)
GO
