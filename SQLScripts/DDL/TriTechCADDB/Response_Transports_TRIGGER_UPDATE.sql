--TRIGGER FOR UPDATE ON [dbo].[Response_Transports]

CREATE TRIGGER RESPONSE_TRANSPORT_UPDTRIGGER
ON [dbo].[Response_Transports]
FOR UPDATE
AS 
DECLARE @CURRDATE DATETIME
SET @CURRDATE = GETDATE()

INSERT INTO RMX_TRIPS(Master_Incident_ID,Vehicle_Assigned_ID, Event_Trigger_DTTM)
SELECT I.Master_Incident_ID,I.Vehicle_Assigned_ID,@CURRDATE
FROM INSERTED I INNER JOIN DELETED D ON  I.Master_Incident_ID = D.Master_Incident_ID
AND I.Vehicle_Assigned_ID = D.Vehicle_Assigned_ID
WHERE ((I.TIME_DEPART_SCENE IS NOT NULL AND D.TIME_DEPART_SCENE IS NULL)
OR
(I.Time_Arrive_Destination IS NOT NULL AND D.Time_Arrive_Destination IS NULL))
AND NOT EXISTS
(SELECT 1 
FROM RMX_TRIPS
WHERE Master_Incident_ID = I.Master_Incident_ID
AND Vehicle_Assigned_ID = I.Vehicle_Assigned_ID
AND RMX_PickUp_DTTM IS NULL)
GO
