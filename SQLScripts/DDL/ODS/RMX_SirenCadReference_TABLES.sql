DROP TABLE [dbo].[RMX_SirenCadReferenceData]
DROP TABLE [dbo].[RMX_SirenCadReference_Sheet]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RMX_SirenCadReferenceData](
    [RMX_SirenCadReferenceData_ID] [varchar](70) NOT NULL,
    [Organization] [varchar](10) NOT NULL,
    [Category] [varchar](50) NOT NULL,
    [SirenID] [int] NOT NULL,
    [Description] [varchar](200) NULL,
 CONSTRAINT [PK_RMX_SirenCadReferenceData] PRIMARY KEY CLUSTERED 
(
    [RMX_SirenCadReferenceData_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE NONCLUSTERED INDEX [IX_RMX_SirenCadReferenceData] ON [dbo].[RMX_SirenCadReferenceData] ([Organization], [Category])
GO

CREATE TABLE [dbo].[RMX_SirenCadReference_Sheet](
    [SHEET_NAME] [varchar](50) NOT NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



