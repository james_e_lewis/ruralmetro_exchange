/****** Object:  Table [dbo].[RMX_XREF_SirenOrgIDs]    Script Date: 5/12/2015 1:31:19 PM ******/
DROP TABLE [dbo].[RMX_XREF_SirenOrgIDs]
GO

/****** Object:  Table [dbo].[RMX_XREF_SirenOrgIDs]    Script Date: 5/12/2015 1:31:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RMX_XREF_SirenOrgIDs](
    [OrganizationID] [nvarchar](20) NOT NULL,
    [OrganizationDescription] [nvarchar](50) NULL,
    [NEMSISAgencyName] [nvarchar](50) NOT NULL
) ON [PRIMARY]

GO


