/****** Object:  View [dbo].[vw_Context_Values_Pivoted]    Script Date: 10/8/2015 3:55:24 PM ******/
DROP VIEW [dbo].[vw_Context_Values_Pivoted]
GO

/****** Object:  View [dbo].[vw_Context_Values_Pivoted]    Script Date: 10/8/2015 3:55:24 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_Context_Values_Pivoted]
 AS
 select localtransactionid, event_sequence,
    SourceSite, SourceAgency, SourceVendor, TransactionType, ePCRNumber,
    UniqueTripId, CADMasterIncident, CADIncidentNumber, PatientFirstName, PatientLastName, DateOfService,
    ProfitCenter,BillingRateProfitCenterID,UnitCallSign, Outcome, ACResponseFile, SceneZone, DispatchZone, CallType,
    SirenMsgID, SirenMsgSeqNum, SirenOutcomeCode, ProfitCenterCode, BillingRateCode
    from 
    (
      select localtransactionid, event_sequence, event_contextdata_element_name, event_contextdata_element_value
      from dbo.rmx_eventcontextdata (NOLOCK)
    ) ctx
    PIVOT
    (  
      max(event_contextdata_element_value)
      for event_contextdata_element_name in (SourceSite,SourceAgency,SourceVendor,TransactionType,ePCRNumber,UniqueTripId, CADIncidentNumber, CADMasterIncident, PatientFirstName, PatientLastName, ProfitCenterCode, ProfitCenter, BillingRateCode, BillingRateProfitCenterID,UnitCallSign,DateOfService, Outcome, ACResponseFile,SceneZone,DispatchZone,CallType, SirenMsgID, SirenMsgSeqNum,SirenOutcomeCode)
    ) piv

GO