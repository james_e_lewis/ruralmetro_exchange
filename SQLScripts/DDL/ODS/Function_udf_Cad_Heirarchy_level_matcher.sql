CREATE FUNCTION [dbo].[udf_Cad_Heirarchy_level_matcher] (@Site VARCHAR(25), @HierarchyType VARCHAR(50),@Level1 VARCHAR(50), @Level2 VARCHAR(50),
                                                                                         @Level3 VARCHAR(50), @Level4 VARCHAR(50), @Level5 VARCHAR(50))
RETURNS BIT
AS
BEGIN
       --SET @HierarchyType = 'Unit', @Site = 'Maricopa'
       DECLARE @MatchFound BIT
       SELECT @MatchFound = 
                                         CASE WHEN  Level1 = @Level1 AND Level1 IS NOT NULL
                                         AND Level2 = @Level2 AND Level2 IS NOT NULL
                                         AND Level3 = @Level3 AND Level3 IS NOT NULL
                                         AND Level4 = @Level4 AND Level4 IS NOT NULL
                                         AND Level5 = @Level5 AND Level5 IS NOT NULL 
                                         THEN 1 

                                         WHEN  Level1 = @Level1 AND Level1 IS NOT NULL
                                         AND Level2 = @Level2 AND Level2 IS NOT NULL
                                         AND Level3 = @Level3 AND Level3 IS NOT NULL
                                         AND Level4 = @Level4 AND Level4 IS NOT NULL
                                         THEN 1 

                                         WHEN  Level1 = @Level1 AND Level1 IS NOT NULL
                                         AND Level2 = @Level2 AND Level2 IS NOT NULL
                                         AND Level3 = @Level3 AND Level3 IS NOT NULL
                                         THEN 1 

                                         WHEN  Level1 = @Level1 AND Level1 IS NOT NULL
                                         AND Level2 = @Level2 AND Level2 IS NOT NULL
                                         THEN 1 

                                         WHEN  Level1 = @Level1 AND Level1 IS NOT NULL
                                         THEN 1 
                                         ELSE 0 END 
       FROM  dbo.RMx_CADHierarchyFiltering WHERE site = @Site AND HierarchyType = @HierarchyType 
       AND (Level1 = @Level1
                     OR Level2 = @Level2
                     OR Level3 = @Level3
                     OR Level4 = @Level4
                     OR Level5 = @Level5)
RETURN ISNULL(@MatchFound,0)
END
GO
