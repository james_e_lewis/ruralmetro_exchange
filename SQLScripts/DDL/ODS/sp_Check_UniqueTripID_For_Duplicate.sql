/****** Object:  StoredProcedure [dbo].[Check_UniqueTripID_For_Duplicate]    Script Date: 9/28/2015 3:19:03 PM ******/
DROP PROCEDURE [dbo].[Check_UniqueTripID_For_Duplicate]
GO

/****** Object:  StoredProcedure [dbo].[Check_UniqueTripID_For_Duplicate]    Script Date: 9/28/2015 3:19:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Check_UniqueTripID_For_Duplicate]
    @UniqueTripID VARCHAR(100),
    @FilterAsDuplicate INT OUTPUT
AS
    SELECT  @FilterAsDuplicate = CASE WHEN COUNT(*) = 0 THEN 0
                                      ELSE 1
                                 END
    FROM    dbo.RMx_EventSummary summ
            INNER JOIN dbo.RMx_EventContextData ctx ON summ.LocalTransactionID = ctx.LocalTransactionID
                                                       AND summ.Event_Sequence = ctx.Event_Sequence
    WHERE   summ.SubComponent_Name = 'AdvanceClaim_TripSender_MF'
            AND summ.Event_Type = 'TRANSACTION_DELIVERED'
            AND ctx.Event_ContextData_Element_Name = 'UniqueTripId'
            AND ctx.Event_ContextData_Element_Value = @UniqueTripID


GO