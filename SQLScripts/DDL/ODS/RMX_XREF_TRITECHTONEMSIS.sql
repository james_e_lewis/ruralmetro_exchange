/*************************************
**  Drop existing table so it can   **
** be redefined.                    **
**************************************/
DROP TABLE [dbo].[RMX_XREF_TriTechToNEMSIS]
GO

/**************************************
**  Create new cross-reference table **
***************************************/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RMX_XREF_TriTechToNEMSIS](
    [TritechAgency] [nvarchar](50) NOT NULL,
    [NEMSISVersion] [nvarchar](10) NOT NULL,
    [NEMSISElement] [nvarchar](30) NOT NULL,
    [NEMSISElementDesc] [nvarchar](512) NULL,
    [TritechElement] [nvarchar](50) NOT NULL,
    [TritechValue] [nvarchar](100) NOT NULL,
    [TritechValueDesc] [nvarchar](255) NULL,
    [NEMSISValue] [nvarchar] (30) NOT NULL,
    [NEMSISValueDesc] [nvarchar] (1024) NULL,
PRIMARY KEY CLUSTERED 
(
    [TritechAgency] ASC,
    [NEMSISVersion] ASC,
    [NEMSISElement] ASC,
    [TritechElement] ASC,
    [TritechValue] ASC,
    [NEMSISValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

