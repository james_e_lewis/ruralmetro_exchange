SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vw_Siren_CallType_XREF]
AS
select * from 
(
    SELECT siren.organization, siren.description, siren.category as SirenElement, sirenID, nemsisversion, nemsiselement, nemsisvalue
      FROM dbo.RMX_SirenCadReferenceData siren (NOLOCK) inner join [dbo].[RMX_XREF_NEMSISCADToSiren] nem (NOLOCK)
        ON siren.RMX_SirenCadReferenceData_ID = nem.SirenCADRefData_ID
    where
    --  organization = '001' --OrganizationID looked up already
        siren.Category = 'CallType' --Siren field name
    --  and nemsisversion = '3.4' --�hard� coded as NEMSIS version
) SirenXREF
PIVOT
(
    max(nemsisvalue)
    for nemsiselement in ([eCad.11],[eCad.30])
) piv
