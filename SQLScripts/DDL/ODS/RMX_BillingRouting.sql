/****** Object:  Table [dbo].[RMX_BillingRouting]    Script Date: 1/21/2016 8:53:27 AM ******/
DROP TABLE [dbo].[RMX_BillingRouting]
GO

/****** Object:  Table [dbo].[RMX_BillingRouting]    Script Date: 1/21/2016 8:53:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RMX_BillingRouting](
	[Site] [nvarchar](100) NOT NULL,
	[SendToAC] [bit] NOT NULL,
	[SendToAMR] [bit] NOT NULL,
 CONSTRAINT [PK_RMX_BillingRouting] PRIMARY KEY CLUSTERED 
(
	[Site] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


