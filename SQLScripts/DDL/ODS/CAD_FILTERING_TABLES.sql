USE [RMx_ODS_Dev02]
GO

/****** Object:  Table [dbo].[RMx_CADUnitFiltering]    Script Date: 4/30/2015 8:28:15 AM ******/
DROP TABLE [dbo].[RMx_CADUnitFiltering]
DROP TABLE [dbo].[RMx_CADHierarchyFiltering]
DROP TABLE [dbo].[RMx_CADFilteringControl]
GO

/****** Object:  Table [dbo].[RMx_CADUnitFiltering]    Script Date: 4/30/2015 8:28:15 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RMx_CADFilteringControl](
	[Site] [nvarchar](50) NOT NULL,
	[IncidentFilteringEnabled] [bit] NOT NULL,
	[IncidentFilteringType] [nvarchar](20) NULL,
	[UnitHierarchyFilteringEnabled] [bit] NOT NULL,
	[UnitHierarchyFilteringType] [nvarchar](20) NULL,
	[UnitFilteringEnabled] [bit] NOT NULL,
	[UnitFilteringType] [nvarchar](20) NULL,
 CONSTRAINT [PK_RMx_CADFilteringControl] PRIMARY KEY CLUSTERED 
(
	[Site] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[RMx_CADUnitFiltering](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Site] [nvarchar](50) NOT NULL,
	[UnitCallSign] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_RMx_CADUnitFiltering] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

CREATE TABLE [dbo].[RMx_CADHierarchyFiltering](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Site] [nvarchar](50) NOT NULL,
	[HierarchyType] [nvarchar](20) NOT NULL,
	[Level1] [nvarchar](30) NOT NULL,
	[Level2] [nvarchar](30) NULL,
	[Level3] [nvarchar](30) NULL,
	[Level4] [nvarchar](30) NULL,
	[Level5] [nvarchar](30) NULL,
 CONSTRAINT [PK_RMx_CADHierarchyFiltering] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
