/****** Object:  StoredProcedure [dbo].[CAD_Filter_By_Unit]    Script Date: 5/28/2015 4:06:47 PM ******/
DROP PROCEDURE [dbo].[CAD_Filter_By_Unit]
GO

/****** Object:  StoredProcedure [dbo].[CAD_Filter_By_Unit]    Script Date: 5/28/2015 4:06:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CAD_Filter_By_Unit] @Site VARCHAR(25), @Unit VARCHAR(25),
@Level1 VARCHAR(50) = NULL,
@Level2 VARCHAR(50) = NULL,
@Level3 VARCHAR(50) = NULL,
@Level4 VARCHAR(50) = NULL,
@Level5 VARCHAR(50) = NULL,
@IsFiltered BIT OUTPUT
AS
DECLARE @UnitFilteringEnabled BIT, @UnitFilteringType VARCHAR(10) 

/* Make sure the site and unit exist in tables */
IF @Site NOT IN (SELECT Site FROM dbo.RMx_CADFilteringControl) OR 
NOT EXISTS ( SELECT 1 FROM dbo.RMx_CADFilteringControl cfc
                    INNER JOIN dbo.RMx_CADUnitFiltering cuf
                    ON cfc.Site = cuf.site 
                    WHERE cfc.site = @Site AND cuf.UnitCallSign = @Unit
                    AND cfc.UnitFilteringEnabled = 1
        
                    )
BEGIN
    SET @IsFiltered = 1;
    RETURN (@IsFiltered);
END

/* Assign results from query to Variables */
SELECT @Site = cfc.Site,@UnitFilteringEnabled = UnitFilteringEnabled,
@UnitFilteringType = UnitFilteringType
FROM dbo.RMx_CADFilteringControl cfc 
LEFT JOIN dbo.RMx_CADHierarchyFiltering chf
ON cfc.Site = chf.Site
AND chf.HierarchyType = 'Unit'
WHERE cfc.Site = @Site;

SELECT @IsFiltered = 
    CASE 
        WHEN @UnitFilteringEnabled = 0 
            THEN 0
        WHEN @UnitFilteringEnabled = 1 AND @UnitFilteringType IS NOT NULL 
            THEN CASE 
                    WHEN @UnitFilteringType = 'Inclusive' THEN
                        CASE WHEN dbo.udf_Cad_Heirarchy_level_matcher(@Site,'Unit',@Level1,@Level2,@Level3,@Level4,@Level5) = 1 THEN 0
                            ELSE 1 END
                    WHEN @UnitFilteringType = 'Exclusive' THEN 
                        CASE WHEN dbo.udf_Cad_Heirarchy_level_matcher(@Site,'Unit',@Level1,@Level2,@Level3,@Level4,@Level5) = 1 THEN 1
                            ELSE  0 END
                 END
    ELSE NULL
    END
RETURN (@IsFiltered)

GO
