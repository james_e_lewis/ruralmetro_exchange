/****** Object:  Table [dbo].[RMX_XREF_NEMSISCADToSiren]    Script Date: 5/12/2015 1:30:35 PM ******/
DROP TABLE [dbo].[RMX_XREF_NEMSISCADToSiren]
GO

/****** Object:  Table [dbo].[RMX_XREF_NEMSISCADToSiren]    Script Date: 5/12/2015 1:30:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RMX_XREF_NEMSISCADToSiren](
    [SirenCADRefData_ID] [nvarchar](70) NOT NULL,
    [NEMSISVersion] [nvarchar](15) NOT NULL,
    [NEMSISElement] [nvarchar](50) NOT NULL,
    [NEMSISValue] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_RMX_XREF_NEMSISCADToSiren] PRIMARY KEY CLUSTERED 
(
    [SirenCADRefData_ID] ASC,
    [NEMSISVersion] ASC,
    [NEMSISElement] ASC,
    [NEMSISValue] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


