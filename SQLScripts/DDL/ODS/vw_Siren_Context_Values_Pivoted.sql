CREATE VIEW [dbo].[vw_Siren_Context_Values_Pivoted]
AS
  SELECT localtransactionid,
         event_sequence,
         sirenmsgid,
         unitcallsign,
         cadincidentnumber,
         sourceagency,
         sirenmsgseqnum
  FROM   (SELECT localtransactionid,
                 event_sequence,
                 event_contextdata_element_name,
                 event_contextdata_element_value
          FROM   dbo.rmx_eventcontextdata (nolock)) ctx
         PIVOT ( Max(event_contextdata_element_value)
               FOR event_contextdata_element_name IN (sirenmsgid,
                                                      unitcallsign,
                                                      cadincidentnumber,
                                                      sourceagency,
                                                      sirenmsgseqnum) ) piv 