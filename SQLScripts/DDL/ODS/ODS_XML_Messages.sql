/****** Object:  Table [dbo].[ODS_XML_Messages]    Script Date: 9/29/2015 10:17:39 AM ******/
DROP TABLE [dbo].[ODS_XML_Messages]
GO

/****** Object:  Table [dbo].[ODS_XML_Messages]    Script Date: 9/29/2015 10:17:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ODS_XML_Messages](
	[MessageID] [int] IDENTITY(1,1) NOT NULL,
	[UniqueID] [varchar](40) NULL,
	[Create_Timestamp] [datetime] NULL,
	[Message_Type] [varchar](20) NULL,
	[Message_Version] [varchar](10) NULL,
	[Message_XML] [xml] NULL,
 CONSTRAINT [PK_ODS_XML_Messages] PRIMARY KEY CLUSTERED 
(
	[MessageID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


