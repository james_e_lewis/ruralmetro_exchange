
/****** Object:  StoredProcedure [dbo].[CAD_Filter_By_Incident]    Script Date: 5/28/2015 4:05:55 PM ******/
DROP PROCEDURE [dbo].[CAD_Filter_By_Incident]
GO

/****** Object:  StoredProcedure [dbo].[CAD_Filter_By_Incident]    Script Date: 5/28/2015 4:05:55 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CAD_Filter_By_Incident] @Site VARCHAR(25),
@Level1 VARCHAR(50) = NULL,
@Level2 VARCHAR(50) = NULL,
@Level3 VARCHAR(50) = NULL,
@Level4 VARCHAR(50) = NULL,
@Level5 VARCHAR(50) = NULL,
@IsFiltered BIT OUTPUT
AS
DECLARE @IncidentFilteringEnabled BIT, @IncidentFilteringType VARCHAR(10)

/* Make sure the site is in the control table*/
IF @Site NOT IN (SELECT Site FROM dbo.RMx_CADFilteringControl)
BEGIN
    SET @IsFiltered = 1;
    RETURN (@IsFiltered);
END

SELECT @Site = cfc.Site,@IncidentFilteringEnabled = IncidentFilteringEnabled,
@IncidentFilteringType = IncidentFilteringType
FROM dbo.RMx_CADFilteringControl cfc 
LEFT JOIN dbo.RMx_CADHierarchyFiltering chf
ON cfc.Site = chf.Site
AND chf.HierarchyType = 'Incident'
WHERE cfc.Site = @Site;

/* Assign results from query to Variables */
SELECT @IsFiltered = 
    CASE 
        WHEN @IncidentFilteringEnabled = 0 
            THEN 0
        WHEN @IncidentFilteringEnabled = 1 AND @IncidentFilteringType IS NOT NULL 
            THEN CASE 
                    WHEN @IncidentFilteringType = 'Inclusive' THEN
                        CASE WHEN dbo.udf_Cad_Heirarchy_level_matcher(@Site,'Incident',@Level1,@Level2,@Level3,@Level4,@Level5) = 1 THEN 0
                            ELSE 1 END
                    WHEN @IncidentFilteringType = 'Exclusive' THEN 
                        CASE WHEN dbo.udf_Cad_Heirarchy_level_matcher(@Site,'Incident',@Level1,@Level2,@Level3,@Level4,@Level5) = 1 THEN 1
                            ELSE  0 END
                 END
    ELSE NULL
    END
RETURN (@IsFiltered)

GO
