USE [RMx_ODS_Dev02]
GO

/****** Object:  Table [dbo].[RMX_XREF_ProfitCenters]    Script Date: 4/30/2015 8:58:52 AM ******/
DROP TABLE [dbo].[RMX_XREF_ProfitCenters]
GO

/****** Object:  Table [dbo].[RMX_XREF_ProfitCenters]    Script Date: 4/30/2015 8:58:52 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RMX_XREF_ProfitCenters](
	[RMX_XREF_ProfitCenters_ID] [int] IDENTITY(1,1) NOT NULL,
	[SourceDB] [varchar](20) NOT NULL,
	[Code] [varchar](20) NULL,
	[Description] [varchar](30) NULL,
	[ProfitCenter] [varchar](10) NULL,
	[ATS_ProfitCenter] [varchar](10) NULL,
 CONSTRAINT [PK_RMX_XREF_ProfitCenters] PRIMARY KEY NONCLUSTERED 
(
	[RMX_XREF_ProfitCenters_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


