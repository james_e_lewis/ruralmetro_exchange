/****** Object:  Table [dbo].[RMX_REPROCESSING_HistoryError]    Script Date: 9/28/2015 9:50:25 AM ******/
DROP TABLE [dbo].[RMX_REPROCESSING_HistoryError]
GO

/****** Object:  Table [dbo].[RMX_REPROCESSING_HistoryError]    Script Date: 9/28/2015 9:50:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[RMX_REPROCESSING_HistoryError](
	[JobID] [int] NOT NULL,
	[DateTimeRun] [datetime] NOT NULL,
	[LocalTransactionID] [varchar](255) NOT NULL,
	[ErrorDetails] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[JobID] ASC,
	[DateTimeRun] ASC,
	[LocalTransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


