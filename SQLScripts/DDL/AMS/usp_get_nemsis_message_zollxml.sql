/****** Object:  StoredProcedure [dbo].[usp_get_nemsis_message_zollxml]    Script Date: 2/18/2016 9:28:50 AM ******/
DROP PROCEDURE [dbo].[usp_get_nemsis_message_zollxml]
GO

/****** Object:  StoredProcedure [dbo].[usp_get_nemsis_message_zollxml]    Script Date: 2/18/2016 9:28:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[usp_get_nemsis_message_zollxml]
    @SourceDB VARCHAR(20),
    @Tdate VARCHAR(10),
    @Job VARCHAR(6),
    @FileType VARCHAR(10) = 'ePCR',
    @XMLOutput VARCHAR(8000) OUTPUT,
    @Narrative VARCHAR(8000) OUTPUT,
    @E14 VARCHAR(8000) OUTPUT,
	@E16 VARCHAR(8000) OUTPUT,
    @E18 VARCHAR(8000) OUTPUT,
    @E19 VARCHAR(8000) OUTPUT
AS
    SET NOCOUNT ON 
    DECLARE @XML XML,
        @XML_data_Max VARCHAR(8000);

/***************************************************  ***************************
********* Set Up ProfitCenter for Tucson Yuma and Knoxville  ********************
***************************************************  ***************************/
    DECLARE @ProfitCenter VARCHAR(20)
    IF @SourceDB = 'TUCSON'
        BEGIN
            SELECT  @ProfitCenter = COALESCE(( SELECT DISTINCT TOP 1
                                                        vi.SerialNumber
                                               FROM     TUCSREPORT.Reporting_System.dbo.Vehicle v ( NOLOCK )
                                                        INNER JOIN TUCSREPORT.Reporting_System.dbo.VehicleInventory vi ( NOLOCK ) ON v.ID = vi.Vehicle_ID
                                                        INNER JOIN TUCSREPORT.Reporting_System.dbo.Response_Vehicles_Assigned RVA ( NOLOCK ) ON rva.Vehicle_ID = v.id
                                                        INNER JOIN TUCSREPORT.Reporting_System.dbo.Inventory i ( NOLOCK ) ON i.ID = vi.InventoryID
                                               WHERE    rva.radio_Name = ( SELECT TOP 1
                                                              v.veh AS Vehicle
                                                              FROM
                                                              FDC_Trips t
                                                              INNER JOIN FDC_Shifts s ON t.SourceDB = s.SourceDB
                                                              AND t.shiftno = s.shiftno
                                                              INNER JOIN dbo.Vehicles v ON s.SourceDB = v.SourceDB
                                                              AND s.veh = v.veh
                                                              WHERE
                                                              t.sourcedb = @SourceDB
                                                              AND t.tdate = @Tdate
                                                              AND t.job = @Job
                                                              )
                                                        AND v.IsActive = 'true'
                                                        AND I.Code = 'Prof'
                                                        AND vi.SerialNumber IS NOT NULL
                                                        AND vi.id = ( SELECT
                                                              MAX(vi2.id)
                                                              FROM
                                                              TUCSREPORT.Reporting_System.dbo.vehicleinventory vi2
                                                              INNER JOIN TUCSREPORT.Reporting_System.dbo.Inventory i2 ON i2.ID = vi2.InventoryID
                                                              AND i2.code = 'Prof'
                                                              WHERE
                                                              vi2.vehicle_id = vi.vehicle_id
                                                              AND vi2.serialnumber != '1'
                                                              AND vi2.serialnumber IS NOT NULL
                                                              )
                                             ),
                                             ( SELECT DISTINCT TOP 1
                                                        VI.SerialNumber AS ProfitCenter
                                               FROM     TUCSREPORT.Reporting_System.dbo.Response_Master_Incident RMI ( NOLOCK )
                                                        LEFT JOIN TUCSREPORT.Reporting_System.dbo.Response_Vehicles_Assigned RVA ( NOLOCK ) ON RVA.Master_Incident_ID = RMI.ID
                                                        LEFT JOIN TUCSREPORT.Reporting_System.dbo.VehicleInventory VI ( NOLOCK ) ON VI.Vehicle_ID = RVA.Vehicle_ID
                                                        LEFT JOIN TUCSREPORT.Reporting_System.dbo.Vehicle V ( NOLOCK ) ON V.ID = RVA.Vehicle_ID
                                                        LEFT JOIN TUCSREPORT.Reporting_System.dbo.Unit_Names U ( NOLOCK ) ON U.ID = V.UnitName_ID
                                                        LEFT JOIN TUCSREPORT.Reporting_System.dbo.Inventory I ( NOLOCK ) ON I.ID = VI.InventoryID
                                                        LEFT JOIN TUCSREPORT.Reporting_System.dbo.ResponseArea RA ( NOLOCK ) ON RA.name = RMI.Response_Area
                                               WHERE    ISNULL(rmi.Master_Incident_Number,
                                                              '') <> ''
                                                        AND I.description = 'Profit Center'
                                                        AND VI.SerialNumber IS  NOT NULL
                                                        AND RMI.Master_Incident_Number = ( SELECT
                                                              runnumber
                                                              FROM
                                                              fdc_trips
                                                              WHERE
                                                              SourceDB = @SourceDB
                                                              AND tdate = @Tdate
                                                              AND Job = @Job
                                                              )
                                             ))
        END
/*Profit Center for Yuma*/
    IF @SourceDB = 'YUMAAZ'
        BEGIN
            SELECT  @ProfitCenter = '256'
        END
/* Profit Center for KNOX */
    IF @SourceDB = 'KNOX'
        BEGIN
            SELECT  @ProfitCenter = cmpy
            FROM    dbo.FDC_Trips
            WHERE   SourceDB = @SourceDB
                    AND tdate = @Tdate
                    AND Job = @Job
        END

    IF @FileType = 'ePCR'
        BEGIN
            WITH XMLNAMESPACES (DEFAULT 'http://www.nemsis.org')
SELECT @XML = (
/***************************************************
********* NEMSIS 2 ePCR  ***************************
***************************************************/
SELECT TOP 1
	--CONCAT(cmpy.sourcedb,'-',cmpy.name) as 'comment()'
	--ISNULL(dem.EMS_Agency_Number,'99') as 'D01_01'
	trips.SourceDB AS 'D01_01'
	,(SELECT TOP 1 census_prefix FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=cmpy.state) AS 'D01_03' 
	,ISNULL(dem.EMS_Agency_County,(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='D01_04')) AS 'D01_04'	--TODO: Translate GNIS code
	,ISNULL(dem.Level_Of_Service,'6090') AS 'D01_07'	--TODO: Translate NEMSIS code
	,'5840' AS 'D01_08'	--TODO: Translate NEMSIS code--ISNULL(dem.Organizational_Type,'5840') AS 'D01_08'	--TODO: Translate NEMSIS code
	,'5870' AS 'D01_09'--ISNULL(dem.Organizational_Status,'5870') AS 'D01_09'	--TODO: Translate NEMSIS code
	,ISNULL(cmpy.NPI,'-10') AS 'D01_21'
	,cmpy.zip AS 'D02_07'
	,(SELECT 	
		(SELECT 
			CONCAT(trips.sourcedb, '.', trips.tdate, '.', trips.job) AS 'E01_01'
			,'Zoll' AS 'E01_02'
			,'RescueNet' AS 'E01_03' --TODO: Where to get software name?
			,'ePCR' AS 'E01_04' --TODO: Where to get software version?
		FOR XML PATH('E01'), TYPE)
	
		,(SELECT
			ISNULL(dem.EMS_Agency_Number,'000') AS 'E02_01'
			,trips.RunNumber AS 'E02_02'
			,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_04' AND sourcevalue=trips.Type_Of_Service) AS 'E02_04' 
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_05' AND sourcevalue=trips.primary_role),
					(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E02_05')) AS 'E02_05' 
			,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_06' AND sourcevalue=cad_trips.LateReason) AS 'E02_06'	
			,(SELECT TOP 1 nemsis_2_value AS 'E02_07' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_07' AND sourcevalue=trips.respdelayreason FOR XML PATH(''),TYPE) 
			,(SELECT TOP 1 nemsis_2_value AS 'E02_07' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_07' AND sourcevalue=trips.respdelayreason_2 FOR XML PATH(''),TYPE) 
			,(SELECT TOP 1 nemsis_2_value AS 'E02_07' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_07' AND sourcevalue=trips.respdelayreason_3 FOR XML PATH(''),TYPE)
			,(SELECT TOP 1 nemsis_2_value AS 'E02_08' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_08' AND sourcevalue=trips.atscenedelayreason FOR XML PATH(''),TYPE) 
			,(SELECT TOP 1 nemsis_2_value AS 'E02_08' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_08' AND sourcevalue=trips.atscenedelayreason_2 FOR XML PATH(''),TYPE) 
			,(SELECT TOP 1 nemsis_2_value AS 'E02_08' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_08' AND sourcevalue=trips.atscenedelayreason_3 FOR XML PATH(''),TYPE) 
			,(SELECT TOP 1 nemsis_2_value AS 'E02_09' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_09' AND sourcevalue=trips.transdelayreason FOR XML PATH(''),TYPE) 
			,(SELECT TOP 1 nemsis_2_value AS 'E02_09' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_09' AND sourcevalue=trips.transdelayreason_2 FOR XML PATH(''),TYPE) 
			,(SELECT TOP 1 nemsis_2_value AS 'E02_09' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_09' AND sourcevalue=trips.transdelayreason_3 FOR XML PATH(''),TYPE) 
			,(SELECT TOP 1 nemsis_2_value AS 'E02_10' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_10' AND sourcevalue=trips.turnarounddelayreason FOR XML PATH(''),TYPE) 
			,(SELECT TOP 1 nemsis_2_value AS 'E02_10' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_10' AND sourcevalue=trips.turnarounddelayreason_2 FOR XML PATH(''),TYPE) 
			,(SELECT TOP 1 nemsis_2_value AS 'E02_10' FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_10' AND sourcevalue=trips.turnarounddelayreason_3 FOR XML PATH(''),TYPE) 
			,ISNULL(trips.veh,'-20') AS 'E02_11'
			,ISNULL(shifts.ems_call_sign,(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E02_12')) AS 'E02_12'
			,ISNULL(shifts.station,(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E02_13')) AS 'E02_13'
			,CASE WHEN @SourceDB IN ('TUCSON','YUMAAZ','KNOX') THEN @ProfitCenter ELSE (CASE WHEN dispatchCallType.descr LIKE 'ATS%' THEN CAST(cad_trips.dispzone AS VARCHAR(6)) + 'A' ELSE CAST(cad_trips.dispzone AS VARCHAR(6)) END) END AS 'E02_14'
			,(SELECT
				ISNULL(CAST(trips.dispatch_latitude AS DECIMAL),'0') AS '@Lat'
				,ISNULL(CAST(trips.dispatch_longitude AS DECIMAL),'0') AS '@Long'
			 FOR XML PATH('E02_15'),TYPE)
			,NULLIF((CASE WHEN trips.odomenr IS NULL OR LEN(trips.odomenr)=0 OR trips.odomenr=0 THEN '0' ELSE CONCAT(LEFT(trips.odomenr,LEN(trips.odomenr)-1),'.',RIGHT(trips.odomenr,1)) END),'0') AS 'E02_16'
			,NULLIF((CASE WHEN trips.odomats IS NULL OR LEN(trips.odomats)=0 OR trips.odomats=0 THEN '0' ELSE CONCAT(LEFT(trips.odomats,LEN(trips.odomats)-1),'.',RIGHT(trips.odomats,1)) END),'0') AS 'E02_17' 
			,NULLIF((CASE WHEN trips.odomatd IS NULL OR LEN(trips.odomatd)=0 OR trips.odomatd=0 THEN '0' ELSE CONCAT(LEFT(trips.odomatd,LEN(trips.odomatd)-1),'.',RIGHT(trips.odomatd,1)) END),'0') AS 'E02_18'
			,NULLIF((CASE WHEN trips.odomatbase IS NULL OR LEN(trips.odomatbase)=0 OR trips.odomatbase=0 THEN '0' ELSE CONCAT(LEFT(trips.odomatbase,LEN(trips.odomatbase)-1),'.',RIGHT(trips.odomatbase,1)) END),'0') AS 'E02_19' 
			,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E02_20' AND sourcevalue=trips.priority) AS 'E02_20'
		FOR XML PATH('E02'), TYPE)

		,(SELECT
			ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E03_01' AND sourcevalue=trips.natureofcall),(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E03_01')) AS 'E03_01'
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E03_02' AND sourcevalue=trips.EMD_Performed),(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E03_02')) AS 'E03_02'
			,trips.emd_card_number AS 'E03_03'
		FOR XML PATH('E03'), TYPE)
--Introducing multiple E04 sections since Zoll puts the crew into the same rows
--Crew member 1
		,case when shifts.crew1code is not null then (SELECT
			shifts.crew1employeenumber as 'E04_01'
			,shifts.crew1name as 'comment()'
			,ISNULL((select top 1 NEMSIS_2_Value from dbo.RMX_XREF_ZollToNEMSIS (NOLOCK) where nemsis_2_element='E04_02' and sourcevalue=crew.crew1role and crew.SourceDB=SourceDB),(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E04_02')) AS 'E04_02'
			,ISNULL((select top 1 NEMSIS_2_Value from dbo.RMX_XREF_ZollToNEMSIS (NOLOCK) where nemsis_2_element='E04_03' and sourcevalue=crew.crew1level and crew.SourceDB=SourceDB),(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E04_03')) AS 'E04_03'
			FROM dbo.FDC_Trip_Crew crew (NOLOCK)
			WHERE trips.sourcedb=crew.sourcedb and trips.tdate=crew.tdate and trips.job=crew.job
		FOR XML PATH('E04'), TYPE) end
--Crew member 2
		,case when shifts.crew2code is not null then (SELECT
			shifts.crew2employeenumber as 'E04_01'
			,shifts.crew2name as 'comment()'
			,ISNULL((select top 1 NEMSIS_2_Value from dbo.RMX_XREF_ZollToNEMSIS (NOLOCK) where nemsis_2_element='E04_02' and sourcevalue=crew.crew2role and crew.SourceDB=SourceDB),(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E04_02')) AS 'E04_02'
			,ISNULL((select top 1 NEMSIS_2_Value from dbo.RMX_XREF_ZollToNEMSIS (NOLOCK) where nemsis_2_element='E04_03' and sourcevalue=crew.crew2level and crew.SourceDB=SourceDB),(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E04_03')) AS 'E04_03'
			FROM dbo.FDC_Trip_Crew crew (NOLOCK)
			WHERE trips.sourcedb=crew.sourcedb and trips.tdate=crew.tdate and trips.job=crew.job
		FOR XML PATH('E04'), TYPE) end
--Crew member 3
		,case when shifts.crew3code is not null then (SELECT
			shifts.crew3employeenumber as 'E04_01'
			,shifts.crew3name as 'comment()'
			,ISNULL((select top 1 NEMSIS_2_Value from dbo.RMX_XREF_ZollToNEMSIS (NOLOCK) where nemsis_2_element='E04_02' and sourcevalue=crew.crew3role and crew.SourceDB=SourceDB),(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E04_02')) AS 'E04_02'
			,ISNULL((select top 1 NEMSIS_2_Value from dbo.RMX_XREF_ZollToNEMSIS (NOLOCK) where nemsis_2_element='E04_03' and sourcevalue=crew.crew3level and crew.SourceDB=SourceDB),(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E04_03')) AS 'E04_03'
			FROM dbo.FDC_Trip_Crew crew (NOLOCK)
			WHERE trips.sourcedb=crew.sourcedb and trips.tdate=crew.tdate and trips.job=crew.job
		FOR XML PATH('E04'), type) end		,(SELECT 
			CASE WHEN trips.injurydate='1900-01-01' THEN '1990-01-01T00:00:00' ELSE CONCAT(trips.injurydate, 'T', trips.injurytime) END AS 'E05_01'
			,ISNULL((SELECT NULLIF(CONCAT(PSAP_date,'T',PSAP_time),'T') FROM dbo.FDC_Trip_Times times (NOLOCK) WHERE times.tdate=trips.tdate AND times.job=trips.job AND times.sourcedb=trips.sourcedb),'1990-01-01T00:00:00') AS 'E05_02'
			,CASE WHEN trips.calldate='1990-01-01' THEN '1990-01-01T00:00:00' ELSE CONCAT(trips.calldate, 'T', trips.calltime) END AS 'E05_03'
			,CONCAT(trips.dispdate, 'T', trips.disptime) AS 'E05_04'
			,CASE WHEN trips.enrdate='1990-01-01' THEN '1990-01-01T00:00:00' ELSE CONCAT(trips.enrdate,'T',trips.enrtime) END AS 'E05_05'
			,CASE WHEN trips.atsdate='1990-01-01' THEN '1990-01-01T00:00:00' ELSE CONCAT(trips.atsdate,'T',trips.atstime) END AS 'E05_06'
			,CASE WHEN trips.atpsdate='1990-01-01' THEN '1990-01-01T00:00:00' ELSE CONCAT(trips.atpsdate,'T',trips.atpstime) END AS 'E05_07'
			,ISNULL(
				(SELECT CASE WHEN txcare_date='1990-01-01' 
							THEN '1990-01-01T00:00:00' 
						ELSE NULLIF(CONCAT(txcare_date,'T',txcare_time),'T')
						END 
				FROM dbo.FDC_Trip_Times times (NOLOCK) WHERE times.tdate=trips.tdate AND times.job=trips.job AND times.sourcedb=trips.sourcedb),'1990-01-01T00:00:00') AS 'E05_08'
			,CASE WHEN trips.tradate='1990-01-01' THEN '1990-01-01T00:00:00' ELSE CONCAT(trips.tradate,'T',trips.tratime) END AS 'E05_09'
			,CASE WHEN trips.atddate='1990-01-01' THEN '1990-01-01T00:00:00' ELSE CONCAT(trips.atddate,'T',trips.atdtime) END AS 'E05_10'
			,CONCAT(trips.avldate,'T',trips.avltime) AS 'E05_11'
			,CASE WHEN trips.cxldate='1900-01-01' THEN '1990-01-01T00:00:00' ELSE CONCAT(trips.cxldate,'T',trips.cxltime) END AS 'E05_12'
			,(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E05_13') AS 'E05_13'
		FOR XML PATH('E05'), TYPE)
		,(SELECT 
			(SELECT
				cust.lastname AS 'E06_01'
				,cust.firstname AS 'E06_02'
				,cust.middleInitial AS 'E06_03'
			FOR XML PATH('E06_01_0'), TYPE)
			,(SELECT
				ISNULL(LEFT(cust.haddr,29),'-5') AS 'E06_04'
				,(SELECT TOP 1 census_code FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=cust.hstate AND feature_name = cust.hcity AND FEATURE_CLASS IN ('Civil', 'Populated Place') ORDER BY FEATURE_CLASS ASC) AS 'E06_05'
				,(SELECT TOP 1 census_prefix FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=cust.hstate) AS 'E06_07'
				,ISNULL(cust.hzip, (SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E06_08')) AS 'E06_08'
			FOR XML PATH('E06_04_0'), TYPE)
			,cust.hcounty AS 'E06_06'
			,cust.home_country AS 'E06_09' --TODO: Translate to FIPS code -- Current Code base doesn't translate -- Leave for now
			,REPLACE(cust.ssn,'-','') AS 'E06_10'
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E06_11' AND sourcevalue=cust.sex),(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E06_11')) AS 'E06_11'
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E06_12' AND sourcevalue=cust.race),(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E06_12')) AS 'E06_12'
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E06_13' AND sourcevalue=cust.ethnicity),'-5') AS 'E06_13'
			,(SELECT
				ISNULL(cust.ageunits,DATEDIFF(year,cust.dob, GETDATE())) AS 'E06_14'
				,ISNULL(cust.ageUnitsOfMeasure,(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E06_15')) AS 'E06_15'
			FOR XML PATH('E06_14_0'), TYPE)
			,cust.dob AS 'E06_16'
			,CASE WHEN LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(cust.hphone,'(',''),')',''),'-',''),' ',''),'.',''),'[a-Z]','')) = 10 THEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(cust.hphone,'(',''),')',''),'-',''),' ',''),'.',''),'[a-Z]','')  ELSE '' END AS 'E06_17'
			,(SELECT
				(SELECT TOP 1 census_prefix FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=cust.driversstate) AS 'E06_18'
				,cust.driverslicense AS 'E06_19'
			FOR XML PATH('E06_19_0'), TYPE)
		FOR XML PATH('E06'), TYPE)
		,(SELECT 
			ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E07_01' AND sourcevalue=trips.method_payment),(SELECT nemsis_2_value FROM dbo.rmx_xref_defaults WHERE nemsis_2_element='E07_01')) AS 'E07_01'
			,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E07_02' AND sourcevalue=cust.cert_of_med_necessity) AS 'E07_02'
			,(SELECT	LEFT(payors.payorName,29) AS 'E07_03'
	,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E07_04' AND sourcevalue=payors.billing_priority) AS 'E07_04'
	,(SELECT CASE WHEN payors.ins_address2 IS NULL THEN payors.ins_address ELSE CONCAT(payors.ins_address,', ',payors.ins_address2) END AS 'E07_05'
					,(SELECT TOP 1 census_code FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=cust.hstate AND feature_name = payors.ins_city) AS 'E07_06'
					,(SELECT TOP 1 census_prefix FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=payors.ins_state) AS 'E07_07' 
					,payors.ins_zip AS 'E07_08'
				 FOR XML PATH('E07_05_0'), 	TYPE)
	,payors.groupCode AS 'E07_09'
	,payors.policy AS 'E07_10'
	,(SELECT
		cust.lastname AS 'E07_11'
		,cust.firstname AS 'E07_12'
		,cust.middleInitial AS 'E07_13'
		 FOR XML PATH('E07_11_0'), TYPE)
	,(SELECT nemsis_2_value 
		FROM dbo.rmx_xref_zolltonemsis 
		WHERE sourcedb=trips.sourcedb 
		AND nemsis_2_element='E07_14' 
		AND sourcevalue=payors.relationship) AS 'E07_14'
		FROM dbo.FDC_Customer_Payors payors
		WHERE payors.custno=trips.custno 
		AND payors.sourcedb=trips.sourcedb
			 FOR XML PATH('E07_03_0'), TYPE)
			 ,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E07_15' AND sourcevalue=trips.isWorkRelated) AS 'E07_15'
			 ,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E07_16' AND sourcevalue=trips.occupational_industry) AS 'E07_16'
			 ,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E07_17' AND sourcevalue=trips.occupational_industry) AS 'E07_17'
			 ,(SELECT
				(SELECT
					LEFT(cust.NOK_lastname,20) AS 'E07_18'
					,LEFT(cust.NOK_firstname,20) AS 'E07_19'
					,cust.nok_middle_initial AS 'E07_20'
				 FOR XML PATH('E07_18_01'), TYPE)
				 ,(SELECT cust.NOK_addr AS 'E07_21'
					,(SELECT TOP 1 census_code FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=cust.hstate AND feature_name = cust.NOK_city AND FEATURE_CLASS IN ('Civil', 'Populated Place') ORDER BY FEATURE_CLASS ASC) AS 'E07_22'
					,(SELECT TOP 1 census_prefix FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=cust.NOK_state) AS 'E07_23'
					,cust.NOK_zip AS 'E07_24'
				 FOR XML PATH('E07_21_0'), TYPE)
				 ,CASE WHEN LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(cust.hphone,'(',''),')',''),'-',''),' ',''),'.',''),'[a-Z]','')) = 10 THEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(cust.NOK_phone,'(',''),')',''),'-',''),' ',''),'.',''),'[a-Z]','')  ELSE '' END AS 'E07_25'
				 --,cust.NOK_phone AS 'E07_25'
				 ,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E07_26' AND sourcevalue=cust.NOK_relation) AS 'E07_26'
			  FOR XML PATH('E07_18_0'), TYPE)
			  ,(SELECT
				trips.employerName AS 'E07_27'
				,(SELECT
					LEFT(NULLIF(CONCAT(trips.employeraddr,', ',trips.employeraddr2),', '),29) AS 'E07_28'
					,(SELECT TOP 1 census_code FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=cust.hstate AND feature_name = trips.employercity AND FEATURE_CLASS IN ('Civil', 'Populated Place') ORDER BY FEATURE_CLASS ASC) AS 'E07_29'	
					,(SELECT TOP 1 census_prefix FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=trips.employerstate) AS 'E07_30'	
					,trips.employerzip AS 'E07_31'	
				FOR XML PATH('E07_28_0'), TYPE)
			  FOR XML PATH('E07_27_0'), TYPE)
			  ,CASE WHEN LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(trips.employerphone,'(',''),')',''),'-',''),' ',''),'.',''),'[a-Z]','')) = 10 THEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(trips.employerphone,'(',''),')',''),'-',''),' ',''),'.',''),'[a-Z]','') END AS 'E07_32'
			  ,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E07_33' AND sourcevalue=trips.response_urgency) AS 'E07_33'
			  ,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E07_34' AND sourcevalue=trips.CMSServiceLevel),'-5') AS 'E07_34'
			  ,(SELECT
				ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E07_35' AND sourcevalue=trips.Scene_Condition),'-5') AS 'E07_35'	--TODO: Verify correct condition code used here
			  FOR XML PATH('E07_35_0'), TYPE)
		FOR XML PATH('E07'), TYPE)
		,(SELECT 
			ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E08_05' AND sourcevalue=trips.NumPatientsAtScene),'1125') AS 'E08_05'
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E08_06' AND sourcevalue=trips.MassCasualty),'-5') AS 'E08_06'
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE nemsis_2_element='E08_07' AND sourcevalue=trips.locationtype),'-5') AS 'E08_07'
			,trips.pufac AS 'E08_08'
			,CASE WHEN @SourceDB IN ('TUCSON','YUMAAZ','KNOX') THEN @ProfitCenter ELSE (CASE WHEN pickupCallType.descr LIKE 'ATS%' THEN CAST(trips.puzone AS VARCHAR(6)) + 'A' ELSE CAST(trips.puzone AS VARCHAR(6)) END) END AS 'E08_09'
				,(SELECT trips.puaddr AS 'E08_11'
					,(SELECT TOP 1 census_code FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=trips.pust AND feature_name = trips.pucity AND FEATURE_CLASS IN ('Civil', 'Populated Place') ORDER BY FEATURE_CLASS ASC) AS 'E08_12'
					,(SELECT TOP 1 census_prefix FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=trips.pust AND feature_name = trips.pucity) AS 'E08_14'
					, trips.puzip AS 'E08_15'
				FOR XML PATH('E08_11_0'), TYPE)
		FOR XML PATH('E08'), TYPE)
		,(SELECT ISNULL((SELECT TOP 1 NULLIF(pta,'0') FROM dbo.Trip_Interventions intervention WHERE intervention.SourceDB = Trips.SourceDB AND intervention.tdate = trips.tdate AND intervention.job = trips.job  ),'-5') AS 'E09_01' 
			,'-5' AS 'E09_02'
			,'-5' AS 'E09_03'
			,'-5' AS 'E09_04'
			,ISNULL((SELECT TOP 1 eTree.descr FROM dbo.Trip_Tree eTree WHERE eTree.SourceDB = Trips.SourceDB AND eTree.tdate = Trips.tdate AND eTree.job = Trips.job AND eTree.type = 32),'-5') AS 'E09_05'
			,'-5' AS 'E09_11'
			,'-5' AS 'E09_12'
			,'-5' AS 'E09_13'
			,'-5' AS 'E09_14'
			,'-5' AS 'E09_15'
			,'-5' AS 'E09_16'
		FOR XML PATH('E09'), TYPE)
		,(SELECT '-5' AS 'E10_01'--(SELECT ISNULL((SELECT TOP 1 CASE WHEN eTree.descr = 'Not Applicable' THEN '-5' ELSE eTree.descr END FROM dbo.Trip_Tree eTree WHERE eTree.SourceDB = Trips.SourceDB AND eTree.tdate = Trips.tdate AND eTree.job = Trips.job AND eTree.type = 103),'-5') AS 'E10_01'
		FOR XML PATH('E10'), TYPE)
		,(SELECT '-5' AS 'E11_01'
			,'-5' AS 'E11_02'
			,'-5' AS 'E11_03' 
		FOR XML PATH('E11'), TYPE)
		,(SELECT '-5' AS 'E12_01'
			, (select
				ISNULL(cust.personaldoctorname,'-5') AS 'E12_04'
			   FOR XML PATH('E12_4_0'), TYPE)
	/* Mantis #308 - Removed non-mandatory NEMSIS fields that tend to cause AC import issues
			, ISNULL((SELECT TOP 1 eTree.code FROM dbo.Trip_Tree eTree WHERE eTree.SourceDB = Trips.SourceDB AND eTree.tdate = Trips.tdate AND eTree.job = Trips.job AND eTree.type = 9),'-5')  AS 'E12_08'
			, ISNULL((SELECT TOP 1 eTree.code FROM dbo.Trip_Tree eTree WHERE eTree.SourceDB = Trips.SourceDB AND eTree.tdate = Trips.tdate AND eTree.job = Trips.job AND eTree.type = 11),'-5') AS 'E12_10'
			,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E12_11' AND sourcevalue=Trips.med_hist_obtained_from )AS 'E12_11'
			,(SELECT TOP 1 code  AS 'E12_14'
					FROM dbo.Trip_Medications meds WHERE trips.SourceDB = meds.SourceDB AND trips.tdate = meds.tdate AND trips.job = trips.job
				FOR XML PATH('E12_14_0'), TYPE)
	*/
		FOR XML PATH('E12'), TYPE)		

		,(SELECT (SELECT 'TempPlaceHolder') AS 'E13_01' -- In IIB the narrative will be added from the @Narrative output parameter and inserted as <E13_01></E13_01>  TempPlaceHolder gets replaced for the IIB developers so the string will look like <E13><\E13> Instead of the default in the conversion of <E13_01/>
			FOR XML PATH('E13'), TYPE)
/*  E14 through E20 are done through Parameters */
		,(SELECT trips.dfac AS 'E20_02'
			,( SELECT 
				LEFT(cad_trips.daddr,29) AS 'E20_03', 
				(SELECT TOP 1 census_code FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha=cad_trips.dst AND feature_name = cad_trips.dcity AND FEATURE_CLASS IN ('Civil', 'Populated Place') ORDER BY FEATURE_CLASS ASC) AS 'E20_04',
				(SELECT TOP 1 census_prefix FROM dbo.rmx_xref_gniscitycodes WHERE state_alpha = cad_trips.dst) AS 'E20_05',
				cad_trips.dzip AS 'E20_07'
				FOR XML PATH('E20_03_0'), TYPE)
			, trips.dzone AS 'E20_09'
			,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E20_10' AND sourcevalue=trips.outcome) AS 'E20_10'
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E20_14' AND sourcevalue=trips.transpriority),'4965') AS 'E20_14'
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E20_16' AND sourcevalue=trips.facilityreason),'-25') AS 'E20_16'
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E20_17' AND sourcevalue=trips.dest_type),'-5') AS 'E20_17'

		FOR XML PATH('E20'), TYPE)
		,(SELECT  
			ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E22_01' AND sourcevalue=trips.er_dept_disposition),'-5') AS 'E22_01'
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E22_02' AND sourcevalue=trips.hosp_disposition),'-5') AS 'E22_02'
		FOR XML PATH('E22'), TYPE)
	FOR XML PATH('Record'), TYPE)
FROM dbo.FDC_Trips trips (NOLOCK)
	LEFT OUTER JOIN dbo.Trips cad_trips (NOLOCK)
		ON cad_trips.tdate = trips.tdate
		AND cad_trips.job=trips.job
		AND cad_trips.sourcedb=trips.sourcedb
	LEFT OUTER JOIN dbo.fdc_customers cust (NOLOCK)
		ON cust.custno=trips.custno
		AND cust.sourcedb=trips.sourcedb
	LEFT OUTER JOIN dbo.Companies cmpy (NOLOCK)
		ON cmpy.SourceDB = trips.SourceDB
		AND cmpy.code = trips.cmpy
	LEFT OUTER JOIN dbo.FDC_Shifts shifts (NOLOCK)
		ON shifts.sourcedb=trips.sourcedb
		AND shifts.shiftno=trips.shiftno
	LEFT OUTER JOIN dbo.FDC_Demographics_Header dem (NOLOCK)
		ON dem.SourceDB=trips.SourceDB
		AND dem.COMPANY_ID = trips.cmpy
	--LEFT OUTER JOIN dbo.FDC_Customer_Payors payors (NOLOCK)
	--	ON payors.custno=trips.custno 
	--	AND payors.sourcedb=trips.sourcedb
	LEFT OUTER JOIN Call_Types AS dispatchCallType ON dispatchCallType.code = cad_trips.calltype AND dispatchCallType.sourceDB = cad_trips.sourceDB
	LEFT OUTER JOIN Call_Types AS pickupCallType ON pickupCallType.code = Trips.calltype AND pickupCallType.sourceDB = Trips.sourceDB

WHERE
	trips.SourceDB = @SourceDB
	AND trips.tdate = @Tdate
	AND trips.job = @Job
FOR XML PATH ('Header'), ROOT ('EMSDataSet')
);
 /*  Add in Nemeis namspace and Prep the E13 section (Narrative) as needed in the IIB */
SELECT  @XMLOutput = CAST('<?xml version="1.0" ?>' + REPLACE(REPLACE(REPLACE(CAST(@Xml AS VARCHAR(MAX)),'xmlns="http://www.nemsis.org"',''), '<EMSDataSet >','<EMSDataSet xmlns="http://www.nemsis.org">'),'<E13_01>TempPlaceHolder</E13_01>', '<E13_01></E13_01>') AS VARCHAR(8000))

SELECT  @Narrative = ( SELECT TOP 1
                                CAST(Trips.narrative AS VARCHAR(8000))
                        FROM     dbo.FDC_Trips Trips
                        WHERE    trips.SourceDB = @SourceDB
                                AND trips.tdate = @Tdate
                                AND trips.job = @Job
                        )

/*	
	Tags E14 through E19 allow Multiple valued sections which causes the VARCHAR to be more than 8000 characters.  The 
	IIB doesn't work will with varchar(max) so these sections are broken out into individual variable and are strung together
	in the IIB
*/	

SELECT  @E14 = CAST(( SELECT    NULLIF(CONCAT(vitals.vdate, 'T',
                                        vitals.vtime),'T') AS 'E14_01',
                                vitals.pta AS 'E14_02',
                                ( SELECT TOP 1
                                            nemsis_2_value
                                    FROM      dbo.rmx_xref_zolltonemsis
                                    WHERE     sourcedb = trips.sourcedb
                                            AND nemsis_2_element = 'E14_03'
                                            AND sourcevalue = vitals.ecg
                                ) AS 'E14_03',
                                ( SELECT    vitals.bps AS 'E14_04',
                                            vitals.bpd AS 'E14_05',
                                            ( SELECT TOP 1
                                                    nemsis_2_value
                                                FROM
                                                    dbo.rmx_xref_zolltonemsis
                                                WHERE
                                                    sourcedb = trips.sourcedb
                                                    AND nemsis_2_element = 'E14_06'
                                                    AND sourcevalue = vitals.Blood_Pressure_Method
                                            ) AS 'E14_06'
                                FOR
                                    XML PATH('E14_04_0'),
                                        TYPE
                                ),
                                vitals.pulse AS 'E14_07',
                                vitals.monitor_rate AS 'E14_08',
                                vitals.spo2 AS 'E14_09',
                                ( SELECT TOP 1
                                            nemsis_2_value
                                    FROM      dbo.rmx_xref_zolltonemsis
                                    WHERE     sourcedb = trips.sourcedb
                                            AND nemsis_2_element = 'E14_10'
                                            AND sourcevalue = vitals.pulsereg
                                ) AS 'E14_10',
                                vitals.resprate AS 'E14_11',
                                ( SELECT TOP 1
                                            nemsis_2_value
                                    FROM      dbo.rmx_xref_zolltonemsis
                                    WHERE     sourcedb = trips.sourcedb
                                            AND nemsis_2_element = 'E14_12'
                                            AND sourcevalue = vitals.respeffort
                                ) AS 'E14_12',
                                vitals.etco2 AS 'E14_13',
                                vitals.glucose AS 'E14_14',
                                ( SELECT    vitals.gcseyes AS 'E14_15',
                                            vitals.gcsverbal AS 'E14_16',
                                            vitals.gcsmotor AS 'E14_17'
                                FOR
                                    XML PATH('E14_15_0'),
                                        TYPE
                                ),
                                vitals.temperature AS 'E14_20',
                                vitals.TemperatureType AS 'E14_21',
                                ( SELECT TOP 1
                                            nemsis_2_value
                                    FROM      dbo.rmx_xref_zolltonemsis
                                    WHERE     sourcedb = trips.sourcedb
                                            AND nemsis_2_element = 'E14_22'
                                            AND sourcevalue = vitals.loc
                                ) AS 'E14_22',
                                vitals.painscale AS 'E14_23',
                                vitals.stroke_scale AS 'E14_24',
                                vitals.thrombolytic_screen AS 'E14_25',
                                ( SELECT TOP 1
                                            total_score
                                    FROM      dbo.FDC_TRIP_SCORES scores
                                    WHERE     trips.SourceDB = scores.SourceDB
                                            AND trips.tdate = scores.tdate
                                            AND trips.job = scores.JOB
                                            AND Score_Type_Descr = 'APGAR Score'
                                ) AS 'E14_26',
                                ( SELECT TOP 1
                                            total_score
                                    FROM      dbo.FDC_TRIP_SCORES scores
                                    WHERE     trips.SourceDB = scores.SourceDB
                                            AND trips.tdate = scores.tdate
                                            AND trips.job = scores.JOB
                                            AND Score_Type_Descr = 'Revised Trauma Score'
                                ) AS 'E14_27',
                                ( SELECT TOP 1
                                            total_score
                                    FROM      dbo.FDC_TRIP_SCORES scores
                                    WHERE     trips.SourceDB = scores.SourceDB
                                            AND trips.tdate = scores.tdate
                                            AND trips.job = scores.JOB
                                            AND Score_Type_Descr = 'Pediatric Trauma Score'
                                ) AS 'E14_28'
                        FROM      fdc_trips trips
                                INNER JOIN dbo.Trip_Vitals vitals ON trips.SourceDB = vitals.SourceDB
                                                    AND trips.tdate = vitals.tdate
                                                    AND trips.job = vitals.job
                        WHERE     trips.SourceDB = @SourceDB
                                AND trips.tdate = @tdate
                                AND trips.job = @job
								AND ISNULL(vitals.vdate,'') <> ''
                    FOR
                        XML PATH('E14')
                    ) AS VARCHAR(8000))

SELECT @E16 = ''
		--CAST((SELECT 
		--				CAST(cust.weight AS INT) AS 'E16_01'
		--				,(SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E16_02' AND sourcevalue=cust.bros_luten_color) AS 'E16_02'
		--				,(select
  --                           NULLIF(concat(vdate,'T',vtime),'T') as 'E16_03'
		--							 --TODO: BUILD MAPPING FOR OTHER E16 ELEMENTS USING TRIP_ASSESSMENT_DETAILS_TREE
		--					  from dbo.trip_vitals 
		--					  where sourcedb=trips.sourcedb and tdate=trips.tdate and job=trips.job
		--					  AND ISNULL(vdate,'') <> ''
		--					  for xml path('E16_00_0'), TYPE)
		--				--,(SELECT
		--				--		CONCAT(vdate,'T',vtime) AS 'E16_03'
		--				FROM  fdc_trips trips
		--				LEFT OUTER JOIN dbo.FDC_Customers cust
		--				ON trips.SourceDB = cust.SourceDB
		--				AND trips.custno = cust.custno
		--				WHERE     trips.SourceDB = @SourceDB
		--				AND trips.tdate = @tdate
		--				AND trips.job = @job
						
		--		FOR XML PATH('E16')
		--		) AS VARCHAR(8000))

SELECT  @E18 = CAST(ISNULL( (SELECT NULLIF(CONCAT(dateperformed, 'T', timeperformed),'T') AS 'E18_01'
                                ,ti.pta AS 'E18_02'
                                ,ISNULL(ti.intervention_code,
                                        ( SELECT nemsis_2_value
                                            FROM   dbo.rmx_xref_defaults
                                            WHERE  nemsis_2_element = 'E18_03'
                                        )) AS 'E18_03'
    /*  Mantis #308 - Removed E18_04as not mandatory and causing import issues in AC
	                            ,( SELECT TOP 1
                                            tiq.value_code
                                    FROM      trip_intervention_qualifiers tiq
                                    WHERE     ti.sourcedb = tiq.sourcedb
                                            AND ti.job = tiq.job
                                            AND ti.tdate = tiq.tdate
                                            AND ti.seq = tiq.seq
                                ) AS 'E18_04' --TODO Brings back more than 1 value
	*/
								,'-5' AS 'E18_08'
                        FROM      fdc_trips trips
                                INNER JOIN trip_interventions ti ( NOLOCK ) ON ti.sourcedb = trips.sourcedb
                                                    AND ti.job = trips.job
                                                    AND ti.tdate = trips.tdate
                        WHERE     trips.SourceDB = @SourceDB
                                AND trips.tdate = @tdate
                                AND trips.job = @job
                    FOR
                        XML PATH('E18')
                    ), (select NULL for XML PATH('E18'))) AS VARCHAR(8000))

SELECT  @E19 = CAST(ISNULL(( SELECT    ( SELECT    COALESCE(NULLIF(dateperformed,
                                                    ''),
                                                    '1990-01-01')
                                            + 'T'
                                            + COALESCE(NULLIF(timeperformed,
                                                    ''), '00:00:00') AS 'E19_01',
                                            ( eInt.pta ) AS 'E19_02',
                                            eInt.intervention_code AS 'E19_03',
                                            '-5' AS 'E19_05',
                                            '-5' AS 'E19_06',
                                            '-5' AS 'E19_07'
                                    FROM      fdc_trips trips
                                            INNER JOIN Trip_Interventions
                                            AS eInt ON trips.SourceDB = eInt.SourceDB
                                                    AND trips.tdate = eInt.tdate
                                                    AND trips.job = eInt.job
                                    WHERE     trips.SourceDB = @SourceDB
                                            AND trips.tdate = @tdate
                                            AND trips.job = @job
                                            AND EXISTS ( SELECT
                                                    1
                                                    FROM
                                                    Interface_Mapping_Values
                                                    AS intMapVal ( NOLOCK )
                                                    WHERE
                                                    intMapVal.MappingID = '3000'
                                                    AND intMapVal.RCSQLValue1 = eInt.intervention_code
                                                    AND intMapVal.SourceDB = eInt.SourceDB )
                                FOR
                                    XML PATH('E19_01_0'),TYPE)
                    FOR
                        XML PATH('E19')
                    ), (select NULL FOR XML PATH('E19'))) AS VARCHAR(8000))

        END


/*****************************************************  CAD *****************************************************************************************************************/
IF @FileType = 'CAD'
BEGIN
with xmlnamespaces (DEFAULT 'http://www.nemsis.org')
SELECT @XML = (

/***************************************************
********* NEMSIS 2 CAD Shell************************
****************************************************/

select top 1
		trips.SourceDB as 'D01_01',
		'AZ' AS 'D01_03',
		'CNTYS' AS 'D01_04',
		'6090' AS 'D01_07',
		'5840' AS 'D01_08',
		'5870' AS 'D01_09',
		'-10' AS 'D01_21',
		'12345'	AS 'D02_07'
	,(SELECT 	
		(select 
			CONCAT(trips.sourcedb, '.', trips.tdate, '.', trips.job) as 'E01_01'
			,'Zoll' as 'E01_02'
			,'RescueNet' as 'E01_03' --TODO: Where to get software name?
			,'CAD' as 'E01_04' --TODO: Where to get software version?
		for xml path('E01'), type)
		,(select
			isnull(NULL,'000') as 'E02_01'
			,trips.RunNumber as 'E02_02'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E02_04') as 'E02_04'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E02_05') as 'E02_05'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E02_06') as 'E02_06'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E02_07') as 'E02_07'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E02_08') as 'E02_08'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E02_09') as 'E02_09'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E02_10') as 'E02_10'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E02_12') as 'E02_12'
			,trips.dispzone as 'E02_14'
			,CASE WHEN trips.odomenr is null or LEN(trips.odomenr)=0 or trips.odomenr=0 THEN '0.0' ELSE CONCAT(LEFT(trips.odomenr,LEN(trips.odomenr)-1),'.',RIGHT(trips.odomenr,1)) END as 'E02_16'
			,CASE WHEN trips.odomats is null or LEN(trips.odomats)=0 or trips.odomats=0 THEN '0.0' ELSE CONCAT(LEFT(trips.odomats,LEN(trips.odomats)-1),'.',RIGHT(trips.odomats,1)) END as 'E02_17' 
			,CASE WHEN trips.odomatd is null or LEN(trips.odomatd)=0 or trips.odomatd=0 THEN '0.0' ELSE CONCAT(LEFT(trips.odomatd,LEN(trips.odomatd)-1),'.',RIGHT(trips.odomatd,1)) END as 'E02_18'
			,ISNULL((select top 1 nemsis_2_value from dbo.rmx_xref_zolltonemsis where sourcedb=trips.sourcedb and nemsis_2_element='E02_20' and sourcevalue=trips.priority),(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E02_20')) as 'E02_20'
		for xml path('E02'), type)
		,(select
			ISNULL((select top 1 nemsis_2_value from dbo.rmx_xref_zolltonemsis where sourcedb=trips.sourcedb and nemsis_2_element='E03_01' and sourcevalue=trips.natureID),(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E03_01')) as 'E03_01'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E03_02') as 'E03_02'
		for xml path('E03'), type)
/* Creating potentially multiple E04 sections since driver and attendant are on same row in Vehicle_Schedule table */
		,case when sched.driver is not null then (SELECT
			employeenumber as 'E04_01'
			,name as 'comment()'
			,'580' AS 'E04_02'
			,CASE
				WHEN drivers.license = 'EMT-P' then '6110'
				WHEN drivers.license = 'EMT-I' then '6100'
				WHEN drivers.license = 'Paramedic' then '6110'
				WHEN drivers.license = 'Physician' then '6112'
				WHEN drivers.license = 'EMT' then '6090'
				ELSE '-10'
			 END AS 'E04_03'
			FROM dbo.Drivers drivers (NOLOCK)
			WHERE sched.sourcedb=drivers.sourcedb and sched.driver=drivers.code
		FOR XML PATH('E04'), TYPE) end
		,case when sched.attendant is not null then (SELECT
			employeenumber as 'E04_01'
			,name as 'comment()'
			,'590' AS 'E04_02'
			,CASE
				WHEN drivers.license = 'EMT-P' then '6110'
				WHEN drivers.license = 'EMT-I' then '6100'
				WHEN drivers.license = 'Paramedic' then '6110'
				WHEN drivers.license = 'Physician' then '6112'
				WHEN drivers.license = 'EMT' then '6090'
				ELSE '-10'
			 END AS 'E04_03'
			FROM dbo.Drivers drivers (NOLOCK)
			WHERE sched.sourcedb=drivers.sourcedb and sched.attendant=drivers.code
		FOR XML PATH('E04'), TYPE) end
		,(select 
			--TODO: Figure Out Which table this comes from in Current Config,(select concat(PSAP_date,'T',PSAP_time) from dbo.FDC_Trip_Times times (NOLOCK) where times.tdate=trips.tdate and times.job=trips.job and times.sourcedb=trips.sourcedb) as 'E05_02'
			--(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E05_02') as 'E05_02'
			'1990-01-01T00:00:00' AS 'E05_02'
			,case when trips.calldate='1990-01-01' then '1990-01-01T00:00:00' ELSE concat(trips.calldate, 'T', trips.calltime) END as 'E05_03'
			,concat(trips.dispdate, 'T', trips.disptime) as 'E05_04'
			,case when trips.enrdate='1990-01-01' then '1990-01-01T00:00:00' ELSE concat(trips.enrdate,'T',trips.enrtime) END as 'E05_05'
			,case when trips.atsdate='1990-01-01' then '1990-01-01T00:00:00' ELSE concat(trips.atsdate,'T',trips.atstime) END as 'E05_06'
			--TODO: figure out if exist or if a default needs to be placed --,case when trips.atpsdate='1990-01-01' then '' ELSE concat(trips.atpsdate,'T',trips.atpstime) END as 'E05_07'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E05_07') as 'E05_07'
			,ISNULL((select case when txcare_date='1990-01-01' then '1990-01-01T00:00:00' ELSE NULLIF(concat(txcare_date,'T',txcare_time),'T') END from dbo.FDC_Trip_Times times (NOLOCK) where times.tdate=trips.tdate and times.job=trips.job and times.sourcedb=trips.sourcedb), '1990-01-01T00:00:00') as 'E05_08'
			,case when trips.tradate='1990-01-01' then '1990-01-01T00:00:00' ELSE concat(trips.tradate,'T',trips.tratime) END as 'E05_09'
			,case when trips.atddate='1990-01-01' then '1990-01-01T00:00:00' ELSE concat(trips.atddate,'T',trips.atdtime) END as 'E05_10'
			,concat(trips.avldate,'T',trips.avltime) as 'E05_11'
			,case when trips.cxldate='1900-01-01' then '1990-01-01T00:00:00' ELSE concat(trips.cxldate,'T',trips.cxltime) END as 'E05_12'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E05_13') as 'E05_13'
		for xml path('E05'), type)
		,(select 
			(select
				LEFT(cust.Name, CHARINDEX(',', cust.Name) - 1) as 'E06_01'
				,RIGHT(cust.Name, CHARINDEX(',', REVERSE(cust.Name)) - 2) as 'E06_02'
				,cust.middleInitial as 'E06_03'
			for xml path('E06_01_0'), type)
			,(select
				--concat(cust.haddr, ', ',cust.haddr2) as 'E06_04'
				--,cust.hcity as 'E06_05'
				--,(select top 1 census_prefix from dbo.rmx_xref_gniscitycodes where state_alpha=cust.hstate) as 'E06_07'
				'-5' AS 'E06_08'
				--isnull(cust.hzip, (select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E06_08')) as 'E06_08'
			for xml path('E06_04_0'), type)
			,REPLACE(cust.ssn,'-','') as 'E06_10'
			,ISNULL((select top 1 nemsis_2_value from dbo.rmx_xref_zolltonemsis where sourcedb=trips.sourcedb and nemsis_2_element='E06_11' and sourcevalue=cust.sex),(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E06_11')) as 'E06_11'
			,ISNULL((select top 1 nemsis_2_value from dbo.rmx_xref_zolltonemsis where sourcedb=trips.sourcedb and nemsis_2_element='E06_12' and sourcevalue=cust.race),(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E06_12')) as 'E06_12'
			,'-5' AS 'E06_13'
			,(SELECT
				(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E06_14') as 'E06_14'
				,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E06_15') as 'E06_15'
			FOR XML PATH('E06_14_0'), TYPE)

			--,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E06_14') as 'E06_14'
			--,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E06_15') as 'E06_15'
			--for xml path('E06_14_0'), type)
			,cust.dob as 'E06_16'
			--,REPLACE(REPLACE(REPLACE(REPLACE(cust.hphone,'(',''),')',''),'-',''),' ','') as 'E06_17'
			--,CASE WHEN LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(cust.hphone,'(',''),')',''),'-',''),' ',''),'[a-Z]','')) = 10 THEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(cust.hphone,'(',''),')',''),'-',''),' ',''),'[a-Z]','')  ELSE '' END AS 'E06_17'
			,CASE WHEN LEN(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(cust.hphone,'(',''),')',''),'-',''),' ',''),'.',''),'[a-Z]','')) = 10 THEN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(cust.hphone,'(',''),')',''),'-',''),' ',''),'.',''),'[a-Z]','')  ELSE NULL END AS 'E06_17'
			,(select
				(select top 1 census_prefix from dbo.rmx_xref_gniscitycodes where state_alpha=cust.driversstate) as 'E06_18'
			for xml path('E06_19_0'), type)
		for xml path('E06'), type)
		,(select 
			(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E07_01') as 'E07_01'
			 ,(select
				(select
					LEFT(LEFT(cust.Name, CHARINDEX(',', cust.Name) - 1),20) as 'E07_18'
					,LEFT(RIGHT(cust.Name, CHARINDEX(',', REVERSE(cust.Name)) - 2),20) as 'E07_19'
					,cust.middleInitial as 'E07_20'
				 for xml path('E07_18_01'), type)
				 ,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E07_26') as 'E07_26'
			  for xml path('E07_18_0'), type)
			 , (select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E07_34') as 'E07_34'
			  ,(select
				(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E07_35') as 'E07_35'
			  for xml path('E07_35_0'), type)
		for xml path('E07'), type)
		,(select 
			(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E08_05') as 'E08_05'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E08_06') as 'E08_06'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E08_07') as 'E08_07'
			,trips.pufac as 'E08_08'
			,trips.puzone as 'E08_09'
				,(select trips.puaddr as 'E08_11'
					,(select top 1 census_code from dbo.rmx_xref_gniscitycodes where state_alpha=trips.pust AND feature_name = trips.pucity AND FEATURE_CLASS IN ('Civil', 'Populated Place') ORDER BY FEATURE_CLASS ASC) as 'E08_12'			
					,(select top 1 census_prefix from dbo.rmx_xref_gniscitycodes where state_alpha=trips.pust AND feature_name = trips.pucity) as 'E08_14'
					, ISNULL(trips.puzip,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E08_15')) as 'E08_15'
 				for xml path('E08_11_0'), type)
		for xml path('E08'), type)
		,(SELECT 
		    '-5' AS 'E09_01'
		    ,'-5' AS 'E09_02'
		    ,'-5' AS 'E09_03'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E09_04') as 'E09_04'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E09_11') as 'E09_11'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E09_12') as 'E09_12'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E09_13') as 'E09_13'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E09_14') as 'E09_14'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E09_15') as 'E09_15'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E09_16') as 'E09_16'
		for xml path('E09'), type)
		,(SELECT '-5' AS 'E10_01'--(SELECT (select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E10_01') as 'E10_01'
		for xml path('E10'), type)
		,(select 
			(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E11_01') as 'E11_01'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E11_02') as 'E11_02'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E11_03') as 'E11_03'
		for xml path('E11'), type)
		 ,(
			SELECT (select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E12_01') as 'E12_01'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E12_19') as 'E12_19'
		for xml path('E12'), type)		
		
		/*,(select CAST(Trips.narrative AS VARCHAR(1000)) AS 'E13_01'
		for xml path('E13'), type)
		
		,(select concat(vitals.vdate, 'T', vitals.vtime) AS 'E14_01'
			,vitals.pta AS 'E14_02'
			,(select top 1 nemsis_2_value from dbo.rmx_xref_zolltonemsis where sourcedb=trips.sourcedb and nemsis_2_element='E14_03' and sourcevalue=vitals.ecg) AS 'E14_03'
			,(SELECT vitals.bps AS 'E14_04'
				,vitals.bpd AS 'E14_05'
				,(select top 1 nemsis_2_value from dbo.rmx_xref_zolltonemsis where sourcedb=trips.sourcedb and nemsis_2_element='E14_06' and sourcevalue=vitals.Blood_Pressure_Method) AS 'E14_06'
				for xml path('E14_04_0'), type)
			,vitals.pulse AS 'E14_07'
			,vitals.monitor_rate AS 'E14_08'
			,vitals.spo2 AS 'E14_09'
			, (select top 1 nemsis_2_value from dbo.rmx_xref_zolltonemsis where sourcedb=trips.sourcedb and nemsis_2_element='E14_10' and sourcevalue=vitals.pulsereg) AS 'E14_10'
			,vitals.resprate AS 'E14_11'
			,(select top 1 nemsis_2_value from dbo.rmx_xref_zolltonemsis where sourcedb=trips.sourcedb and nemsis_2_element='E14_12' and sourcevalue=vitals.respeffort) AS 'E14_12'
			,vitals.etco2 AS 'E14_13'
			,vitals.glucose AS 'E14_14'
			,(SELECT vitals.gcseyes AS 'E14_15'
				,vitals.gcsverbal AS 'E14_16'
				,vitals.gcsmotor AS 'E14_17'
				for xml path('E14_15_0'), type)
			,vitals.temperature AS 'E14_20'
			,vitals.TemperatureType  AS 'E14_21'
			,(select top 1 nemsis_2_value from dbo.rmx_xref_zolltonemsis where sourcedb=trips.sourcedb and nemsis_2_element='E14_22' and sourcevalue=vitals.loc) AS 'E14_22'
			,vitals.painscale as 'E14_23'
			,vitals.stroke_scale as 'E14_24'
			,vitals.thrombolytic_screen as 'E14_25'
			,(SELECT TOP 1 total_score FROM dbo.FDC_TRIP_SCORES scores WHERE trips.SourceDB = scores.SourceDB AND trips.tdate = scores.tdate AND trips.job = scores.JOB AND Score_Type_Descr='APGAR Score') as 'E14_26'
			,(SELECT TOP 1 total_score FROM dbo.FDC_TRIP_SCORES scores WHERE trips.SourceDB = scores.SourceDB AND trips.tdate = scores.tdate AND trips.job = scores.JOB AND Score_Type_Descr='Revised Trauma Score') as 'E14_27'
			,(SELECT TOP 1 total_score FROM dbo.FDC_TRIP_SCORES scores WHERE trips.SourceDB = scores.SourceDB AND trips.tdate = scores.tdate AND trips.job = scores.JOB AND Score_Type_Descr='Pediatric Trauma Score') as 'E14_28'

			--E14_22
			From dbo.Trip_Vitals vitals
			WHERE vitals.SourceDB = trips.SourceDB
			AND vitals.tdate = trips.tdate
			AND vitals.job = trips.job
		for xml path('E14'), type) 
/*		,(select ''
		for xml path('E15'), type)
		,(select ''
		for xml path('E16'), type)
		,(select ''
		for xml path('E17'), type) */
		,(select ''
		for xml path('E18'), type)*/

		/*
			,(select 
                      cast(cust.weight as int) as 'E16_01'
                      ,(select top 1 nemsis_2_value from dbo.rmx_xref_zolltonemsis where sourcedb=trips.sourcedb and nemsis_2_element='E16_02' and sourcevalue=cust.bros_luten_color) as 'E16_02'
                      ,(select
                             concat(vdate,'T',vtime) as 'E16_03'
                             --TODO: BUILD MAPPING FOR OTHER E16 ELEMENTS USING TRIP_ASSESSMENT_DETAILS_TREE
                      from dbo.trip_vitals 
                      where sourcedb=trips.sourcedb and tdate=trips.tdate and job=trips.job
                      for xml path('E16_00_0'), type)
              for xml path('E16'), type)
			  */
              --E17 not used currently in Zoll integration
			  /* Todo:  nothing showing up but minimally need single row for default null values */
 
		,(select trips.dfac AS 'E20_02'
			,( SELECT 
				LEFT(trips.daddr,29) AS 'E20_03', 
				(select top 1 census_code from dbo.rmx_xref_gniscitycodes where state_alpha=trips.dst AND feature_name = trips.dcity AND FEATURE_CLASS IN ('Civil', 'Populated Place') ORDER BY FEATURE_CLASS ASC) AS 'E20_04',
				(select top 1 census_prefix from dbo.rmx_xref_gniscitycodes where state_alpha=trips.dst) AS 'E20_05',
				ISNULL(trips.dzip,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E20_07')) AS 'E20_07'
				for XML PATH('E20_03_0'), TYPE)
			, trips.dzone as 'E20_09'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E20_10')  as 'E20_10'
			--,ISNULL((select top 1 nemsis_2_value from dbo.rmx_xref_zolltonemsis where sourcedbtrips.sourcedb and nemsis_2_element='E20_14' and sourcevalue=trips.transpriority),(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E20_14') as 'E20_14'
			,ISNULL((SELECT TOP 1 nemsis_2_value FROM dbo.rmx_xref_zolltonemsis WHERE sourcedb=trips.sourcedb AND nemsis_2_element='E20_14' AND sourcevalue=trips.transpriority),'-5') AS 'E20_14'
			,(select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E20_16') as 'E20_16'
			,ISNULL((select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E20_17'),'-5') as 'E20_17'
		for xml path('E20'), type)

/*		,(select '' -- Not defined in current code base
		for xml path('E21'), type)*/
		,(select 
			ISNULL((select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E22_01'),'-5') as 'E22_01'
			,ISNULL((select nemsis_2_value from dbo.rmx_xref_defaults where nemsis_2_element='E22_02'),'-5') as 'E22_02'
		for xml path('E22'), type) 
		/*,(select --Not defined in the Current code base
			'' as 'E23_01'
			,'' as 'E23_02'
			,'' as 'E23_03'
			,'' as 'E23_04'
			,'' as 'E23_05'
			,'' as 'E23_06'
			,'' as 'E23_07'
			,'' as 'E23_08'
			,'' as 'E23_09'
			,'' as 'E23_10'
			,'' as 'E23_11'

		for xml path('E23'), type)*/
	for xml path('Record'), type)
from dbo.Trip_History triph (NOLOCK)
	inner join dbo.Trips trips (NOLOCK)
		ON trips.tdate = triph.tdate 
		and trips.job=triph.job 
		and trips.SourceDB=triph.SourceDB
	inner join dbo.customers cust (NOLOCK)
		on cust.custno=trips.custno
		and cust.sourcedb=trips.sourcedb
	inner join dbo.Companies cmpy (NOLOCK)
		ON cmpy.SourceDB = trips.SourceDB
		and cmpy.code = trips.cmpy
	--inner join dbo.FDC_Shifts shifts (NOLOCK)
	--	ON shifts.sourcedb=trips.sourcedb
	--	and shifts.shiftno=trips.shiftno
	--left outer join dbo.FDC_Demographics_Header dem (NOLOCK)
	--	ON dem.SourceDB=trips.SourceDB
	--	and dem.COMPANY_ID = trips.cmpy
	left outer join dbo.Customer_Payors payors (NOLOCK)
		ON payors.custno=trips.custno 
	--	and payors.sourcedb=trips.sourcedb
	left outer join dbo.Vehicle_schedule sched (NOLOCK)
		ON sched.sourcedb=trips.sourcedb 
		and sched.schrecno=trips.schrecno
where
	triph.SourceDB = @SourceDB--'MEMPHIS'
	and triph.tdate = @Tdate--'2015-11-10'
	and triph.job = @Job--'0041-A'
	--'0063-A'
	and triph.description like 'COMPLETE%'
FOR XML PATH ('Header'), root ('EMSDataSet')
);

 /*  Add in Nemsis namspace and Prep the E13 section (Narrative) as needed in the IIB */
SELECT  @XMLOutput = CAST('<?xml version="1.0" ?>' + REPLACE(REPLACE(REPLACE(CAST(@Xml AS VARCHAR(max)),'xmlns="http://www.nemsis.org"',''),'<EMSDataSet >', '<EMSDataSet xmlns="http://www.nemsis.org">'),'<E13 />','<E13></E13>')
 AS VARCHAR(8000))

SELECT  @E18 = CAST(ISNULL(( SELECT    CONCAT(dateperformed, 'T',
                                        timeperformed) AS 'E18_01',
                                ti.pta AS 'E18_02',
                                ISNULL(ti.intervention_code,
                                        ( SELECT nemsis_2_value
                                            FROM   dbo.rmx_xref_defaults
                                            WHERE  nemsis_2_element = 'E18_03'
                                        )) AS 'E18_03',
                                ( SELECT TOP 1
                                            tiq.value_code
                                    FROM      trip_intervention_qualifiers tiq
                                    WHERE     ti.sourcedb = tiq.sourcedb
                                            AND ti.job = tiq.job
                                            AND ti.tdate = tiq.tdate
                                            AND ti.seq = tiq.seq
                                ) AS 'E18_04' --TODO Brings back more than 1 value
								,'-5' AS 'E18_08'
                        FROM      fdc_trips trips
                                INNER JOIN trip_interventions ti ( NOLOCK ) ON ti.sourcedb = trips.sourcedb
                                                    AND ti.job = trips.job
                                                    AND ti.tdate = trips.tdate
                        WHERE     trips.SourceDB = @SourceDB
                                AND trips.tdate = @tdate
                                AND trips.job = @job
                    FOR
                        XML PATH('E18')
                    ), (SELECT NULL FOR XML PATH('E18'))) AS VARCHAR(8000))

SELECT  @E19 = CAST(ISNULL(( SELECT    ( SELECT    COALESCE(NULLIF(dateperformed,
                                                    ''),
                                                    '1990-01-01')
                                            + 'T'
                                            + COALESCE(NULLIF(timeperformed,
                                                    ''), '00:00:00') AS 'E19_01',
                                            ( eInt.pta ) AS 'E19_02',
                                            eInt.intervention_code AS 'E19_03',
                                            '-5' AS 'E19_05',
                                            '-5' AS 'E19_06',
                                            '-5' AS 'E19_07'
                                    FROM      fdc_trips trips
                                            INNER JOIN Trip_Interventions
                                            AS eInt ON trips.SourceDB = eInt.SourceDB
                                                    AND trips.tdate = eInt.tdate
                                                    AND trips.job = eInt.job
                                    WHERE     trips.SourceDB = @SourceDB
                                            AND trips.tdate = @tdate
                                            AND trips.job = @job
                                            AND EXISTS ( SELECT
                                                    1
                                                    FROM
                                                    Interface_Mapping_Values
                                                    AS intMapVal ( NOLOCK )
                                                    WHERE
                                                    intMapVal.MappingID = '3000'
                                                    AND intMapVal.RCSQLValue1 = eInt.intervention_code
                                                    AND intMapVal.SourceDB = eInt.SourceDB )
                                FOR
                                    XML PATH('E19_01_1'),
                                        TYPE
                                )
                    FOR
                        XML PATH('E19')
                    ), (SELECT NULL FOR XML PATH('E19'))) AS VARCHAR(8000))


END
/* Characters like the & need to be converted in the text of the Narrative to xml compliant values.  & in the text needs to be &amp; 
	Casting the  @Narrative to xml then back to a varchar fixes this isses
 */
 SELECT @Narrative
--SELECT @Narrative = REPLACE(REPLACE(@Narrative,'<?xml version="1.0"?>','<?xml version="1.0" encoding="iso8859-1"?>'),'&','&amp;')
--SELECT @Narrative = --CAST(('<?xml version="1.0" encoding="iso8859-1"?>' + ' 
--' +@Narrative) AS VARCHAR(8000));
--SELECT CONVERT(XML,@Narrative,0)
SELECT  @Narrative = CAST(CAST(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@Narrative,'<?xml version="1.0"?>','<?xml version="1.0" encoding="iso8859-1"?>'),'&','&amp;'),'"','&quot;'),'''','&apos;'),'<','&lt;'),'>','&gt;') AS XML) AS VARCHAR(8000));
--SELECT @Narrative = '';

 --   SELECT  @XMLOutput AS XMLOutput
 --   SELECT  @Narrative AS Narrative
    RETURN


GO


