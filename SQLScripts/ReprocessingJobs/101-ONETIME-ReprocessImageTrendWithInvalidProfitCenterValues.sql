/*
SELECT 
  outSummary.LocalTransactionID,
  outSummary.Global_TransactionID,
  ctxpiv.SourceAgency,
  ctxpiv.UniqueTripId,
  outsummary.event_create_timestamp,
  outSummary.Event_Type,
  outSummary.Event_Details,
  outSummary.Event_Sequence,
  eventStream.Event_BitStreamData 
FROM
  rmx_ods.dbo.RMx_EventSummary outSummary (nolock)
INNER JOIN rmx_ods.dbo.RMx_EventBitStreamData eventStream (nolock)
ON
  outSummary.LocalTransactionID = eventStream.LocalTransactionID
  and eventStream.event_sequence = 1
inner join rmx_ods.dbo.vw_context_values_pivoted ctxpiv (NOLOCK)
      on outSummary.localtransactionid = ctxpiv.localtransactionid
      and outSummary.event_sequence = ctxpiv.event_sequence
WHERE
  outSummary.Event_Type               ='BUSINESS_FAULT'
AND outSummary.Event_Summary          ='Trip has Invalid Profit Center value'
and event_create_timestamp > '2015-01-30'
and sourcesystem = 'ImageTrend Inc.'
AND NOT EXISTS (
  select 1
  from rmx_ods.dbo.rmx_eventsummary summ2 (NOLOCK) inner join rmx_ods.dbo.vw_context_values_pivoted ctxpiv2 (NOLOCK)
      on summ2.localtransactionid = ctxpiv2.localtransactionid
      and summ2.event_sequence = ctxpiv2.event_sequence
  where
    ctxpiv2.uniquetripid = ctxpiv.uniquetripid
    and summ2.event_create_timestamp > outsummary.event_create_timestamp
)
order by ctxpiv.uniquetripid;
  
*/
INSERT INTO [rmx_ods].[dbo].[RMX_REPROCESSING_JOB_LOOKUP]
           ([JobID]
           ,[Name]
           ,[Description]
           ,[SQLQuery]
           ,[Schedule]
           ,[Enable]
           ,[Recurring]
           ,[MaxRetries]
           ,[AlertEmailAddrs])
     VALUES
           (101
           ,'ImageTrend sites failing profit center lookup from 1/29 release'
           ,'San Diego and Santa Clara trips started failing from 1/29 release due to profit center lookup still using LGB from Global Deployment spreadsheet instead of new SANDIEGO and SANTACLARA values'
           ,'SELECT 
  outSummary.LocalTransactionID,
  outSummary.Global_TransactionID,
  ctxpiv.SourceAgency,
  ctxpiv.UniqueTripId,
  outsummary.event_create_timestamp,
  outSummary.Event_Type,
  outSummary.Event_Details,
  outSummary.Event_Sequence,
  eventStream.Event_BitStreamData 
FROM
  rmx_ods.dbo.RMx_EventSummary outSummary (nolock)
INNER JOIN rmx_ods.dbo.RMx_EventBitStreamData eventStream (nolock)
ON
  outSummary.LocalTransactionID = eventStream.LocalTransactionID
  and eventStream.event_sequence = 1
inner join rmx_ods.dbo.vw_context_values_pivoted ctxpiv (NOLOCK)
      on outSummary.localtransactionid = ctxpiv.localtransactionid
      and outSummary.event_sequence = ctxpiv.event_sequence
WHERE
  outSummary.Event_Type               =''BUSINESS_FAULT''
AND outSummary.Event_Summary          =''Trip has Invalid Profit Center value''
and event_create_timestamp > ''2015-01-30''
and sourcesystem = ''ImageTrend Inc.''
AND NOT EXISTS (
  select 1
  from rmx_ods.dbo.rmx_eventsummary summ2 (NOLOCK) inner join rmx_ods.dbo.vw_context_values_pivoted ctxpiv2 (NOLOCK)
      on summ2.localtransactionid = ctxpiv2.localtransactionid
      and summ2.event_sequence = ctxpiv2.event_sequence
  where
    ctxpiv2.uniquetripid = ctxpiv.uniquetripid
    and summ2.event_create_timestamp > outsummary.event_create_timestamp
)
;'
           ,'0 30 17 12 2 ? *' --run every 12th of February at or after 5:30pm
           ,1
           ,0
           ,1
           ,'dg_rmx@rmetro.com');
GO
