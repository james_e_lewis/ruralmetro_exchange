INSERT INTO [dbo].[RMX_REPROCESSING_JOB_LOOKUP]
           ([JobID]
           ,[Name]
           ,[Description]
           ,[SQLQuery]
           ,[Schedule]
           ,[Enable]
           ,[Recurring]
           ,[MaxRetries]
           ,[AlertEmailAddrs])
     VALUES
           (4
           ,'Siren Delivery Technical Errors'
           ,'Errors while uploading trips to Siren'
           ,'SELECT outSummary.LocalTransactionID
       ,outSummary.Global_TransactionID
       ,outSummary.Event_Type
       ,outSummary.Event_Details
       ,outSummary.Event_Sequence
       ,eventStream.Event_BitStreamData
       ,outSummary.Event_Create_Timestamp
       ,ctx.Event_ContextData_Element_Value as UniqueTripID
FROM dbo.RMx_EventSummary outSummary(NOLOCK)
INNER JOIN dbo.RMx_EventBitStreamData eventStream(NOLOCK) ON outSummary.LocalTransactionID = eventStream.LocalTransactionID
       AND eventStream.event_sequence = 1
INNER JOIN dbo.RMx_EventContextData ctx(NOLOCK) ON ctx.LocalTransactionID = outSummary.localtransactionid
       AND ctx.event_sequence = outSummary.event_sequence
       AND ctx.Event_ContextData_Element_Name = ''UniqueTripID''
WHERE outSummary.SubComponent_Name = ''Siren_Delivery_MF''
       AND outSummary.Event_Summary in (''InvalidSirenCADMsgFormat'',''SirenInternalError-Database'',''SirenInternalError-General'',''SirenInternalError-ServerRequest'',''SirenInvalidCredentials'')
       AND NOT EXISTS (
              --a newer successful delivery of this trip to Siren from the Siren Delivery flow
              SELECT 1
              FROM dbo.rmx_eventsummary insummary(NOLOCK)
              INNER JOIN dbo.RMx_EventContextData innerCtx(NOLOCK) ON insummary.LocalTransactionID = innerCtx.LocalTransactionID
                      AND insummary.Event_Sequence = innerCtx.Event_Sequence
                      AND innerCtx.Event_ContextData_Element_Name = ''UniqueTripID''
              WHERE outsummary.subcomponent_name = insummary.subcomponent_name
                      AND innerCtx.Event_ContextData_Element_Value = ctx.Event_ContextData_Element_Value
                      AND insummary.Event_Type = ''TRANSACTION_DELIVERED''
                      and insummary.Event_Create_Timestamp > outSummary.Event_Create_Timestamp
              )
       AND (
              --this clause ensures we filter those records which
              -- have not yet exceeded the configured max retries for this job
              SELECT COUNT(*)
              FROM dbo.rmx_eventsummary insummary(NOLOCK)
              WHERE outsummary.subcomponent_name = insummary.subcomponent_name
                      AND insummary.Global_TransactionID = outSummary.Global_TransactionID
                      AND insummary.Event_Sequence > 1
              ) < (
              SELECT isnull(MAX(maxretries), 5000)
              FROM dbo.rmx_reprocessing_job_lookup(NOLOCK)
              WHERE NAME = ''Siren Delivery Technical Errors''
              )'
           ,'0 0 7,17 1/1 * ? *' --run twice daily at 7am & 5pm
           ,1
           ,1
           ,3
           ,'ashukla@rmetro.com');
GO
