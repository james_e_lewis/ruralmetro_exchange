INSERT INTO [dbo].[RMX_REPROCESSING_JOB_LOOKUP]
           ([JobID]
           ,[Name]
           ,[Description]
           ,[SQLQuery]
           ,[Schedule]
           ,[Enable]
           ,[Recurring]
           ,[MaxRetries]
           ,[AlertEmailAddrs])
     VALUES
           (2
           ,'Advance Claim Submit Technical Errors'
           ,'Errors while submitting trips to Advance Claim (HTTP 500 errors)'
           ,'SELECT outSummary.LocalTransactionID
    ,outSummary.Global_TransactionID
    ,outSummary.Event_Type
    ,outSummary.Event_Details
    ,outSummary.Event_Sequence
    ,eventStream.Event_BitStreamData
    ,outSummary.Event_Create_Timestamp
    ,ctx.Event_ContextData_Element_Value as UniqueTripID
FROM dbo.RMx_EventSummary outSummary(NOLOCK)
INNER JOIN dbo.RMx_EventBitStreamData eventStream(NOLOCK) ON outSummary.LocalTransactionID = eventStream.LocalTransactionID
    AND eventStream.event_sequence = 1
INNER JOIN dbo.RMx_EventContextData ctx(NOLOCK) ON ctx.LocalTransactionID = outSummary.localtransactionid
    AND ctx.event_sequence = outSummary.event_sequence
    AND ctx.Event_ContextData_Element_Name = ''UniqueTripID''
WHERE outSummary.Event_Type = ''TECHNICAL_FAULT''
    AND outSummary.Event_Summary = ''Error occured during Advance Claim call''
    AND NOT EXISTS (
        --a successful delivery of this trip to Advance Claim from the same component that faulted
        SELECT 1
        FROM dbo.rmx_eventsummary insummary(NOLOCK)
        INNER JOIN dbo.RMx_EventContextData innerCtx(NOLOCK) ON insummary.LocalTransactionID = innerCtx.LocalTransactionID
            AND insummary.Event_Sequence = innerCtx.Event_Sequence
            AND innerCtx.Event_ContextData_Element_Name = ''UniqueTripID''
        WHERE outsummary.subcomponent_name = insummary.subcomponent_name
            AND innerCtx.Event_ContextData_Element_Value = ctx.Event_ContextData_Element_Value
            AND insummary.Event_Type = ''TRANSACTION_DELIVERED''
        )
    AND (
        --this clause ensures we filter those records which
        -- have not yet exceeded the configured max retries for this job
        SELECT COUNT(*)
        FROM dbo.rmx_eventsummary insummary(NOLOCK)
        INNER JOIN dbo.RMx_EventContextData innerCtx(NOLOCK) ON insummary.LocalTransactionID = innerCtx.LocalTransactionID
            AND insummary.Event_Sequence = innerCtx.Event_Sequence
            AND innerCtx.Event_ContextData_Element_Name = ''UniqueTripID''
        WHERE outsummary.subcomponent_name = insummary.subcomponent_name
            AND innerCtx.Event_ContextData_Element_Value = ctx.Event_ContextData_Element_Value
            AND insummary.Event_Sequence > 1
        ) < (
        SELECT isnull(MAX(maxretries), 5000)
        FROM dbo.rmx_reprocessing_job_lookup(NOLOCK)
        WHERE NAME = ''Advance Claim Submit Technical Errors''
        )'
           ,'0 0 4 1/1 * ? *' --run daily at 4am
           ,1
           ,1
           ,3
           ,'ashukla@rmetro.com');
GO
