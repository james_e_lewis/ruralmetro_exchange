INSERT INTO [dbo].[RMX_REPROCESSING_JOB_LOOKUP]
           ([JobID]
           ,[Name]
           ,[Description]
           ,[SQLQuery]
           ,[Schedule]
           ,[Enable]
           ,[Recurring]
           ,[MaxRetries]
           ,[AlertEmailAddrs])
     VALUES
           (1
           ,'Invalid Profit Center Business Errors'
           ,'Maricopa/PMT Profit Center Determination Errors'
           ,'select * from (
SELECT 
  outSummary.LocalTransactionID,
  outSummary.Global_TransactionID,
  outSummary.Event_Type,
  outSummary.Event_Details,
  outSummary.Event_Sequence,
  contextdata.UniqueTripID,
  eventStream.Event_BitStreamData,
  (SELECT
      COUNT(*)
    FROM
      dbo.rmx_eventsummary insummary (NOLOCK)
    INNER JOIN dbo.vw_Context_Values_Pivoted innercontext
    ON
      insummary.localtransactionid = innercontext.localtransactionid
    AND insummary.event_sequence = innercontext.event_sequence
    WHERE
      outsummary.subcomponent_name = insummary.subcomponent_name
      and outSummary.event_summary = insummary.event_summary
    AND innercontext.UniqueTripId = contextdata.UniqueTripId) as NumRetries,
  (select min(insummary.event_create_timestamp)
   from dbo.rmx_eventsummary insummary (NOLOCK)
    INNER JOIN dbo.vw_Context_Values_Pivoted innercontext
    ON
      insummary.localtransactionid = innercontext.localtransactionid
    AND insummary.event_sequence = innercontext.event_sequence
  where
      outsummary.subcomponent_name = insummary.subcomponent_name
      and outSummary.event_summary = insummary.event_summary
    AND innercontext.UniqueTripId = contextdata.UniqueTripId) as OrigRecvDate
FROM
  dbo.RMx_EventSummary outSummary (NOLOCK)
INNER JOIN dbo.RMx_EventBitStreamData eventStream
ON
  outSummary.LocalTransactionID = eventStream.LocalTransactionID
  and eventStream.event_sequence = 1
INNER JOIN dbo.vw_Context_Values_Pivoted contextdata
ON
  contextdata.LocalTransactionID = outSummary.localtransactionid
AND contextdata.event_sequence = outSummary.event_sequence
WHERE
  outSummary.Event_Type =''BUSINESS_FAULT''
AND outSummary.Event_Summary =''Trip has Invalid Profit Center value''
AND contextdata.UniqueTripId is not null and contextdata.UniqueTripId != ''No Matching Records''
AND outSummary.event_create_timestamp > DATEADD(MM, -3, GETDATE()) --TODAY - 3 MONTHS
AND NOT EXISTS (
    select 1
    from dbo.rmx_eventsummary insummary (NOLOCK)
    INNER JOIN dbo.vw_Context_Values_Pivoted innercontext
    ON
      insummary.localtransactionid = innercontext.localtransactionid
    AND insummary.event_sequence = innercontext.event_sequence
    where
        outsummary.subcomponent_name = insummary.subcomponent_name
        and outSummary.event_summary = insummary.event_summary
        AND innercontext.UniqueTripId = contextdata.UniqueTripId
        and insummary.Event_Create_Timestamp > outsummary.Event_Create_Timestamp
)
) PossibleRetries
WHERE
    NumRetries < (
        SELECT
          isnull(MAX(maxretries), 5000)
        FROM
          dbo.rmx_reprocessing_job_lookup (NOLOCK)
        WHERE
          name = ''Invalid Profit Center Business Errors''
    )
    AND
    OrigRecvDate > DATEADD(D, -10, GETDATE())'
           ,'0 0 5 1/1 * ? *' --run daily at 5am
           ,1
           ,1
           ,7
           ,'ashukla@rmetro.com');
GO
