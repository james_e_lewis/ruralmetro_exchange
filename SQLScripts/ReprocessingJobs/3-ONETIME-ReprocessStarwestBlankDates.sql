/*
use RMx_ODS;
SELECT 
  outSummary.LocalTransactionID,
  outSummary.Global_TransactionID,
  ctxpiv.SourceAgency,
  ctxpiv.UniqueTripId,
  outsummary.event_create_timestamp,
  outSummary.Event_Type,
  outSummary.Event_Details,
  outSummary.Event_Sequence,
  eventStream.Event_BitStreamData
FROM
  dbo.RMx_EventSummary outSummary (nolock)
INNER JOIN dbo.RMx_EventBitStreamData eventStream (nolock)
ON
  outSummary.LocalTransactionID = eventStream.LocalTransactionID
  and eventStream.event_sequence = 1
inner join dbo.vw_context_values_pivoted ctxpiv (NOLOCK)
      on outSummary.localtransactionid = ctxpiv.localtransactionid
      and outSummary.event_sequence = ctxpiv.event_sequence
WHERE
  outSummary.Event_Type               ='BUSINESS_FAULT'
AND outSummary.Event_Summary          ='Both PSAP Call date/time and Unit notified by dispatch date/time are empty'
AND NOT EXISTS (
  select 1
  from dbo.rmx_eventsummary summ2 (NOLOCK) inner join dbo.vw_context_values_pivoted ctxpiv2 (NOLOCK)
      on summ2.localtransactionid = ctxpiv2.localtransactionid
      and summ2.event_sequence = ctxpiv2.event_sequence
  where
    ctxpiv2.uniquetripid = ctxpiv.uniquetripid
    and summ2.event_type = outsummary.event_type
    and summ2.event_summary = outsummary.event_summary
    and summ2.event_create_timestamp > outsummary.event_create_timestamp
)
order by ctxpiv.uniquetripid;
  
*/
INSERT INTO [RMx_ODS].[dbo].[RMX_REPROCESSING_JOB_LOOKUP]
           ([JobID]
           ,[Name]
           ,[Description]
           ,[SQLQuery]
           ,[Schedule]
           ,[Enable]
           ,[Recurring]
           ,[MaxRetries]
           ,[AlertEmailAddrs])
     VALUES
           (3
           ,'Invalid missing date times business faults'
           ,'Trips incorrectly identifed with empty E05_02 and E05_04 date/times'
           ,'SELECT 
  outSummary.LocalTransactionID,
  outSummary.Global_TransactionID,
  ctxpiv.SourceAgency,
  ctxpiv.UniqueTripId,
  outsummary.event_create_timestamp,
  outSummary.Event_Type,
  outSummary.Event_Details,
  outSummary.Event_Sequence,
  eventStream.Event_BitStreamData
FROM
  rmx_ods.dbo.RMx_EventSummary outSummary (nolock)
INNER JOIN rmx_ods.dbo.RMx_EventBitStreamData eventStream (nolock)
ON
  outSummary.LocalTransactionID = eventStream.LocalTransactionID
  and eventStream.event_sequence = 1
inner join rmx_ods.dbo.vw_context_values_pivoted ctxpiv (NOLOCK)
      on outSummary.localtransactionid = ctxpiv.localtransactionid
      and outSummary.event_sequence = ctxpiv.event_sequence
WHERE
  outSummary.Event_Type               =''BUSINESS_FAULT''
AND outSummary.Event_Summary          =''Both PSAP Call date/time and Unit notified by dispatch date/time are empty''
AND NOT EXISTS (
  select 1
  from rmx_ods.dbo.rmx_eventsummary summ2 (NOLOCK) inner join rmx_ods.dbo.vw_context_values_pivoted ctxpiv2 (NOLOCK)
      on summ2.localtransactionid = ctxpiv2.localtransactionid
      and summ2.event_sequence = ctxpiv2.event_sequence
  where
    ctxpiv2.uniquetripid = ctxpiv.uniquetripid
    and summ2.event_type = outsummary.event_type
    and summ2.event_summary = outsummary.event_summary
    and summ2.event_create_timestamp > outsummary.event_create_timestamp
);'
           ,'0 0 10 30/1 1 ? *' --run every 30/31st of January at 10am
           ,1
           ,0
           ,3
           ,'ashukla@rmetro.com');
GO
